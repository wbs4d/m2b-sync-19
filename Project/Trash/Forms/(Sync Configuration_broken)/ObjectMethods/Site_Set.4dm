C_POINTER:C301($Clients_ptr;$identifiers_ptr)
C_LONGINT:C283($RowClicked_i)
C_TEXT:C284($old_t;$IdentifierSelected_t)

$Clients_ptr:=OBJECT Get pointer:C1124(Object named:K67:5;"sync_Clients")
$identifiers_ptr:=OBJECT Get pointer:C1124(Object named:K67:5;"IdentifiersList")

$RowClicked_i:=$identifiers_ptr->

If ($RowClicked_i>0) & ($RowClicked_i<=Size of array:C274($identifiers_ptr->))  // If a line is highlighted
	$IdentifierSelected_t:=$identifiers_ptr->{$RowClicked_i}
	$old_t:=Form:C1466.SiteIdentifier
	Form:C1466.SiteIdentifier:=$IdentifierSelected_t
	DELETE FROM ARRAY:C228($identifiers_ptr->;$RowClicked_i)
	
	If ($old_t#Storage:C1525.Defaults.SiteIdentifier) & (Length:C16($old_t)>0)
		APPEND TO ARRAY:C911($identifiers_ptr->;$old_t)
		SORT ARRAY:C229($identifiers_ptr->)
	End if 
	
End if 

