C_LONGINT:C283($0)

C_LONGINT:C283($arrayrow_i;$processnum_i)
C_POINTER:C301($source_ptr)
C_TEXT:C284($IdentifierDropped_t)

_O_DRAG AND DROP PROPERTIES:C607($source_ptr;$arrayrow_i;$processnum_i)

Case of 
	: (Form event code:C388=On Drag Over:K2:13)
		If ($source_ptr=OBJECT Get pointer:C1124(Object named:K67:5;"IdentifiersList Box"))  //If the drop does come from the scrollable area
			$0:=0
		Else 
			$0:=-1  //The drop is refused
		End if 
		
	: (Form event code:C388=On Drop:K2:12)
		$source_ptr:=OBJECT Get pointer:C1124(Object named:K67:5;"IdentifiersList")
		
		$IdentifierDropped_t:=$source_ptr->{$arrayrow_i}
		Self:C308->:=$IdentifierDropped_t
		
		//  If it is present in the other possible locations remove it
		Syn_Pref_RemoveIdentifier($IdentifierDropped_t)
		
		
	: (Form event code:C388=On Load:K2:1)
		
End case 