C_BOOLEAN:C305($CloseNow_b;$QuitNow_b)
C_LONGINT:C283($CharCode_i;<>Sync_LocalDictionary_i;$i)
C_POINTER:C301($Object_ptr)

If (<>sync_FoundationPresent_b)
	EXECUTE METHOD:C1007("Fnd_Wnd_CloseNow";$CloseNow_b)
	EXECUTE METHOD:C1007("Fnd_Gen_QuitNow";$QuitNow_b)
	EXECUTE METHOD:C1007("Fnd_Gen_FormMethod";$QuitNow_b)
	
End if 

Case of 
	: (syn_CloseDialog) | ($CloseNow_b) | ($QuitNow_b) | (syn_Quit>1)  //  Syn_Quit = 1, stop "sync" process but not close dialogs
		CANCEL:C270
		syn_CloseDialog(False:C215)  //  Reset it for next time!
		
	: (Form event code:C388=On Unload:K2:2)
		
	: (Form event code:C388=On Load:K2:1)  //  Does this seem strange, it will only be run once!
		
		If (Application type:C494=4D Remote mode:K5:5)
			//  We need to retrieve the prefs from the server!
			//    Request them from the server as text & rebuild them locally
			
			//    Just in case something has changed, get rid of the one we have
			If (Dict_IsValid(<>Sync_LocalDictionary_i))
				Dict_Release(<>Sync_LocalDictionary_i)
			End if 
			
			<>Sync_LocalDictionary_i:=Dict_LoadFromText(syn_GetPrefsFromServer)
			If (Dict_Name(<>Sync_LocalDictionary_i)#"SyncLocal")
				Dict_Name(<>Sync_LocalDictionary_i;"SyncLocal")
			End if 
			
			//Else 
			
			//<>Sync_LocalDictionary_i:=Dict_Named ("SyncLocal")
			
			
		End if 
		
		
		Dict_APD_Cleanup(True:C214)  // Just in case it has been closed
		//  On Load will only be called once, so why have it at the top of the case statement?
		
		$Object_ptr:=OBJECT Get pointer:C1124(Object named:K67:5;"SiteIdentifier")
		$Object_ptr->:=KVP_Text("SyncLocal.SiteIdentifier")
		If (Length:C16($Object_ptr->)=0)
			$Object_ptr->:=KVP_Text("SyncLocal.SiteIdentifier";KVP_Text("SyncDefaults.SiteIdentifier"))
		End if 
		
		$Object_ptr:=OBJECT Get pointer:C1124(Object named:K67:5;"ServerIdentifier")
		$Object_ptr->:=KVP_Text("SyncLocal.ServerIdentifier")
		
		$Object_ptr:=OBJECT Get pointer:C1124(Object named:K67:5;"ServerAddress")
		$Object_ptr->:=KVP_Text("SyncLocal.ServerAddress")
		
		$Object_ptr:=OBJECT Get pointer:C1124(Object named:K67:5;"Password")
		$Object_ptr->:=KVP_Text("SyncLocal.Password")
		If (Length:C16($Object_ptr->)=0)
			$Object_ptr->:=KVP_Text("SyncLocal.Password";KVP_Text("SyncDefaults.Password"))
		End if 
		
		$Object_ptr:=OBJECT Get pointer:C1124(Object named:K67:5;"CommsPort")
		$Object_ptr->:=KVP_Longint("SyncLocal.Port")
		If ($Object_ptr->=0)
			$Object_ptr->:=KVP_Longint("SyncLocal.Port";KVP_Longint("SyncDefaults.Port"))
		End if 
		
		$Object_ptr:=OBJECT Get pointer:C1124(Object named:K67:5;"sync_Clients")
		Dict_GetArray(<>Sync_LocalDictionary_i;"Sync Clients";$Object_ptr)
		
		If (True:C214)  //  This sets up the list of allowable identifiers
			$Object_ptr:=OBJECT Get pointer:C1124(Object named:K67:5;"IdentifiersList")
			ARRAY TEXT:C222($Object_ptr->;32)  // RESIZE TO MAXIMUM
			
			$CharCode_i:=Character code:C91("A")-1
			
			For ($i;1;26)
				$Object_ptr->{$i}:=Char:C90($CharCode_i+$i)+String:C10($i;"00")
			End for 
			
			$CharCode_i:=945-27  //Alpha
			
			For ($i;27;32)
				$Object_ptr->{$i}:=Char:C90($CharCode_i+$i)+String:C10($i;"00")
			End for 
		End if 
		
		$Object_ptr:=OBJECT Get pointer:C1124(Object named:K67:5;"Log Everything")
		$Object_ptr->:=KVP_Longint(Syn Log Everything)
		
		$Object_ptr:=OBJECT Get pointer:C1124(Object named:K67:5;"Error Logging")
		$Object_ptr->:=KVP_Longint(Syn Log Errors)
		
		$Object_ptr:=OBJECT Get pointer:C1124(Object named:K67:5;"Log Connection Errors")
		$Object_ptr->:=KVP_Longint(Syn Log Connection Errors)
		
		$Object_ptr:=OBJECT Get pointer:C1124(Object named:K67:5;"Transmission Log")
		$Object_ptr->:=KVP_Longint(Syn Transmission Log)
		
		
End case 