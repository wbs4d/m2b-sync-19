//%attributes = {"invisible":true,"shared":true}
// Method: PositionR
C_LONGINT:C283($0)
C_TEXT:C284($1)
C_TEXT:C284($2)

C_LONGINT:C283($posLast_i;$posNext_i)
C_TEXT:C284($CharacterToFind_t;$marker_t;$StringToSearch_t)

If (False:C215)
	C_LONGINT:C283(PositionR;$0)
	C_TEXT:C284(PositionR;$1)
	C_TEXT:C284(PositionR;$2)
End if 

$CharacterToFind_t:=$1
$StringToSearch_t:=$2

$marker_t:="~"*Length:C16($CharacterToFind_t)
If ($CharacterToFind_t=$marker_t)
	$marker_t:="`"*Length:C16($CharacterToFind_t)
	If ($CharacterToFind_t=$marker_t)
		$marker_t:=Char:C90(0x03B1)*Length:C16($CharacterToFind_t)  //  Greek Unicode for lower case alpha
		If ($CharacterToFind_t=$marker_t)
			$marker_t:=Char:C90(0x03B2)*Length:C16($CharacterToFind_t)  //  Greek Unicode for lower case beta
			If ($CharacterToFind_t=$marker_t)
				$marker_t:=Char:C90(0x03B3)*Length:C16($CharacterToFind_t)  //  Greek Unicode for lower case gamma
				If ($CharacterToFind_t=$marker_t)
					$marker_t:=Char:C90(0x03B4)*Length:C16($CharacterToFind_t)  //  Greek Unicode for lower case delta
					If ($CharacterToFind_t=$marker_t)
						$marker_t:=Char:C90(0x03B5)*Length:C16($CharacterToFind_t)  //  Greek Unicode for lower case epsilon
						If ($CharacterToFind_t=$marker_t)
							$marker_t:=Char:C90(0x03B6)*Length:C16($CharacterToFind_t)  //  Greek Unicode for lower case zeta
							If ($CharacterToFind_t=$marker_t)
								$marker_t:=Char:C90(0x03B7)*Length:C16($CharacterToFind_t)  //  Greek Unicode for lower case eta
								If ($CharacterToFind_t=$marker_t)
									$marker_t:=Char:C90(0x03B8)*Length:C16($CharacterToFind_t)  //  Greek Unicode for lower case theta
									If ($CharacterToFind_t=$marker_t)
										$marker_t:=Char:C90(0x03B9)*Length:C16($CharacterToFind_t)  //  Greek Unicode for lower case iota
										If ($CharacterToFind_t=$marker_t)
											$marker_t:=Char:C90(0x03BA)*Length:C16($CharacterToFind_t)  //  Greek Unicode for lower case kappa
											If ($CharacterToFind_t=$marker_t)
												$marker_t:=Char:C90(0x03BB)*Length:C16($CharacterToFind_t)  //  Greek Unicode for lower case lambda
												If ($CharacterToFind_t=$marker_t)
													$marker_t:=Char:C90(0x03BC)*Length:C16($CharacterToFind_t)  //  Greek Unicode for lower case mu
													If ($CharacterToFind_t=$marker_t)
														$marker_t:=Char:C90(0x03BD)*Length:C16($CharacterToFind_t)  //  Greek Unicode for lower case nu
														If ($CharacterToFind_t=$marker_t)
															$marker_t:=Char:C90(0x03BE)*Length:C16($CharacterToFind_t)  //  Greek Unicode for lower case xi
														End if 
													End if 
												End if 
											End if 
										End if 
									End if 
								End if 
							End if 
						End if 
					End if 
				End if 
			End if 
		End if 
	End if 
End if 

$posLast_i:=0
Repeat 
	$posNext_i:=Position:C15($CharacterToFind_t;$StringToSearch_t)
	If ($posNext_i>0)
		$posLast_i:=$posNext_i
		$StringToSearch_t:=Replace string:C233($StringToSearch_t;$CharacterToFind_t;$marker_t;1)
	End if 
Until ($posNext_i=0)

$0:=$posLast_i