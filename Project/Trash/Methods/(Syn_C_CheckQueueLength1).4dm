//%attributes = {"invisible":true,"shared":true}
//
// Syn_C_CheckQueueLength
// http://127.0.0.1:3456/4DWSDL/
//
// Method source code automatically generated by the 4D SOAP wizard.
// ----------------------------------------------------------------
C_LONGINT:C283($0)
C_TEXT:C284($1)

If (False:C215)
	C_LONGINT:C283(Syn_C_CheckQueueLength;$0)
	C_TEXT:C284(Syn_C_CheckQueueLength;$1;$2)
	
	//Calls:
	Syn_S_CheckQueueLength
	
End if 

WEB SERVICE SET PARAMETER:C777("target";$1)

WEB SERVICE CALL:C778($2;"A_WebService#Syn_S_CheckQueueLength";"Syn_S_CheckQueueLength";"http://www.4d.com/namespace/default";Web Service dynamic:K48:1;*)

If (OK=1)
	WEB SERVICE GET RESULT:C779($0;"RecordsInQueue";*)  // Memory clean-up on the final return value.
End if 