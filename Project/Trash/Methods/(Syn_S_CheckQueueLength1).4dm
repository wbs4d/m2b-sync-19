//%attributes = {"invisible":true,"shared":true,"publishedSoap":true,"publishedWsdl":true}
//C_LONGINT($0)
C_TEXT:C284($1)

C_LONGINT:C283(syn_RecordsInQueue_i)
syn_RecordsInQueue_i:=0

If (False:C215)
	//C_LONGINT(Syn_S_CheckQueueLength ;$0)
	C_TEXT:C284(Syn_S_CheckQueueLength;$1)
	
	//called by 
	Syn_C_CheckQueueLength
	
End if 

SOAP DECLARATION:C782($1;Is text:K8:3;SOAP input:K46:1;"target")
SOAP DECLARATION:C782(syn_RecordsInQueue_i;Is longint:K8:6;SOAP output:K46:2;"RecordsInQueue")

READ ONLY:C145(<>sync_Table_ptr->)

//SET QUERY DESTINATION(Into variable;syn_RecordsInQueue_i)

QUERY:C277(<>sync_Table_ptr->;<>sync_ForIdentifier_ptr->=$1)

syn_RecordsInQueue_i:=Records in selection:C76(<>sync_Table_ptr->)

Syn_UnloadRecordReduceSelection(<>sync_Table_ptr)