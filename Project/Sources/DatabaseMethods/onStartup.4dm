
Syn_TRACE

000_DisplayDictionaries

Syn_Startup

//Run 4DPop
//This code is available even if the component is not present like in the final application.
If (Not:C34(Is compiled mode:C492))
	ARRAY TEXT:C222($tTxt_Components;0)
	COMPONENT LIST:C1001($tTxt_Components)
	If (Find in array:C230($tTxt_Components;"4DPop")>0)
		EXECUTE METHOD:C1007("4DPop_Palette")
	End if 
	
	If (Find in array:C230($tTxt_Components;"Code Analysis")>0)
		EXECUTE METHOD:C1007("CA_ShowQuickLauncher")
		
		C_TEXT:C284($CA_Path_t;$LastSave_t;$Now_t)
		C_LONGINT:C283($Dict_i)
		
		If (Application type:C494=4D Local mode:K5:1)
			
			$CA_Path_t:=Structure file:C489
			$CA_Path_t:=File_LongNameToPathName($CA_Path_t)
			$CA_Path_t:=$CA_Path_t+"Code Analysis Folder"+Folder separator:K24:12
			
			If (Test path name:C476($CA_Path_t)=Is a folder:K24:2)
			Else 
				CREATE FOLDER:C475($CA_Path_t;*)
				
			End if 
			$CA_Path_t:=$CA_Path_t+"Last Export.xml"
			
			$LastSave_t:=""
			If (Test path name:C476($CA_Path_t)=Is a document:K24:1)
				$Dict_i:=Dict_LoadFromFile($CA_Path_t)
				$LastSave_t:=Dict_GetText($Dict_i;"Last Save")
			Else 
				$Dict_i:=Dict_New("Code Analysis")
				$LastSave_t:="00000000"
			End if 
			
			$Now_t:=String:C10(Year of:C25(Current date:C33);"0000")+String:C10(Month of:C24(Current date:C33);"00")+String:C10(Day of:C23(Current date:C33);"00")
			
			If ($LastSave_t<$Now_t)
				Dict_SetText($Dict_i;"Last Save";$Now_t)
				EXECUTE METHOD:C1007("CA_SaveMethodsToTextFiles")
				Dict_SaveToFile($Dict_i;$CA_Path_t)
			End if 
			
			Dict_Release($Dict_i)
		End if 
		
		
		
	End if 
	
End if 

