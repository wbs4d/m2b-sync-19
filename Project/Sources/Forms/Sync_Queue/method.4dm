C_BOOLEAN:C305(<>sync_FoundationPresent_b;$CloseNow_b;$QuitNow_b)
C_POINTER:C301($FixedQueue_ptr)

If (<>sync_FoundationPresent_b)
	EXECUTE METHOD:C1007("Fnd_Wnd_CloseNow";$CloseNow_b)
	EXECUTE METHOD:C1007("Fnd_Gen_QuitNow";$QuitNow_b)
	EXECUTE METHOD:C1007("Fnd_Gen_FormMethod";$QuitNow_b)
End if 

Case of 
	: (syn_CloseDialog) | ($CloseNow_b) | ($QuitNow_b) | (syn_Quit>1)  //  Syn_Quit = 1, stop "sync" process but not close dialogs
		CANCEL:C270
		syn_CloseDialog(False:C215)  //  Reset it for next time!
		
		
	: (Form event code:C388=On Timer:K2:25) | (Form event code:C388=On Activate:K2:9)
		syn_UpdateQueueDisplay
		
	: (Form event code:C388=On Load:K2:1)
		READ ONLY:C145(*)
		
		LISTBOX SET TABLE SOURCE:C1013(*;"Sync_Queue";Table:C252(sync_Table_ptr))  // {; highlightName} ) 
		
		LISTBOX SET COLUMN FORMULA:C1203(*;"For Column";"[Sync]ForIdentifier";Is text:K8:3)
		LISTBOX SET COLUMN FORMULA:C1203(*;"PK Column";"[Sync]PK";Is text:K8:3)
		LISTBOX SET COLUMN FORMULA:C1203(*;"SyncID Column";"[Sync]Sync_ID";Is text:K8:3)
		
		ALL RECORDS:C47(sync_Table_ptr->)
		
		//SET TIMER(180)  //  Refresh every 3 seconds
		
		$FixedQueue_ptr:=OBJECT Get pointer:C1124(Object named:K67:5;"Fixed Queue")
		
		Syn_SendUpdatesTo($FixedQueue_ptr;Current method name:C684)
		If (Find in array:C230($FixedQueue_ptr->;<>sync_SiteIdentifier_c3)=-1)
			APPEND TO ARRAY:C911($FixedQueue_ptr->;<>sync_SiteIdentifier_c3)  //  Add the local queue as well
		End if 
		
		SORT ARRAY:C229($FixedQueue_ptr->)
		
		INSERT IN ARRAY:C227($FixedQueue_ptr->;1;2)
		$FixedQueue_ptr->{1}:="Choose Queue"
		$FixedQueue_ptr->{2}:="-"
		$FixedQueue_ptr->:=1
		
		
		syn_UpdateQueueDisplay
		
		
		
	: (Form event code:C388=On Close Box:K2:21)
		CANCEL:C270
		
End case 