C_POINTER:C301($Object_ptr;$availableClients_ptr;$registeredClients_ptr)
C_TEXT:C284($currentIdentifier_t;$defaultSiteIdentifier_t)
C_LONGINT:C283($i;$numberOfClients_i)

CONFIRM:C162("Are you sure you want to reset the Sync System?";"Cancel";"Reset")

If (OK=0)
	
	$availableClients_ptr:=OBJECT Get pointer:C1124(Object named:K67:5;"availableIdentifiersList")
	$registeredClients_ptr:=OBJECT Get pointer:C1124(Object named:K67:5;"registeredClientsList")
	$defaultSiteIdentifier_t:=KVP_Text("SyncDefaults.SiteIdentifier")
	
	// SiteIdentifier
	$Object_ptr:=OBJECT Get pointer:C1124(Object named:K67:5;"SiteIdentifier")
	$currentIdentifier_t:=$Object_ptr->
	If (Length:C16($currentIdentifier_t)>0)
		If ($currentIdentifier_t#$defaultSiteIdentifier_t)
			APPEND TO ARRAY:C911($availableClients_ptr->;$currentIdentifier_t)
		End if 
	End if 
	$Object_ptr->:=$defaultSiteIdentifier_t
	
	// ServerIdentifier
	$Object_ptr:=OBJECT Get pointer:C1124(Object named:K67:5;"ServerIdentifier")
	$currentIdentifier_t:=$Object_ptr->
	If (Length:C16($currentIdentifier_t)>0)
		APPEND TO ARRAY:C911($availableClients_ptr->;$currentIdentifier_t)
	End if 
	$Object_ptr->:=""
	
	// ServerAddress
	$Object_ptr:=OBJECT Get pointer:C1124(Object named:K67:5;"ServerAddress")
	$Object_ptr->:=""
	
	// Communications Port
	$Object_ptr:=OBJECT Get pointer:C1124(Object named:K67:5;"CommsPort")
	$Object_ptr->:=KVP_Longint("SyncDefaults.Port")
	
	// Password
	$Object_ptr:=OBJECT Get pointer:C1124(Object named:K67:5;"Password")
	$Object_ptr->:=KVP_Text("SyncDefaults.Password")
	
	// RegisteredClientsList
	$numberOfClients_i:=Size of array:C274($registeredClients_ptr->)
	For ($i;$numberOfClients_i;1;-1)
		APPEND TO ARRAY:C911($availableClients_ptr->;$registeredClients_ptr->{$i})
		DELETE FROM ARRAY:C228($registeredClients_ptr->;$i)
	End for 
	
	SORT ARRAY:C229($availableClients_ptr->)
	
End if 