C_POINTER:C301($Clients_ptr;$siteIdentifier_ptr)
C_LONGINT:C283($RowClicked_i)
C_TEXT:C284($old_t;$IdentifierSelected_t;$SyncDefault_t)

$SyncDefault_t:=KVP_Text("SyncDefaults.SiteIdentifier")
$siteIdentifier_ptr:=OBJECT Get pointer:C1124(Object named:K67:5;"SiteIdentifier")
$Clients_ptr:=OBJECT Get pointer:C1124(Object named:K67:5;"availableIdentifiersList")


$RowClicked_i:=$Clients_ptr->

If ($RowClicked_i>0) & ($RowClicked_i<=Size of array:C274($Clients_ptr->))  // If a line is highlighted
	$IdentifierSelected_t:=$Clients_ptr->{$RowClicked_i}
	$old_t:=$siteIdentifier_ptr->
	$siteIdentifier_ptr->:=$IdentifierSelected_t
	DELETE FROM ARRAY:C228($Clients_ptr->;$RowClicked_i)
	
	If ($old_t#$IdentifierSelected_t) & (Length:C16($old_t)>0) & ($old_t#"X33")
		APPEND TO ARRAY:C911($Clients_ptr->;$old_t)
		SORT ARRAY:C229($Clients_ptr->)
	End if 
	
End if 

