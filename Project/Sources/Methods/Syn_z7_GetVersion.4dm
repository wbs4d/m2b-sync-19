//%attributes = {"invisible":true}
C_TEXT:C284($0)

C_BOOLEAN:C305($isWin64_b)
C_LONGINT:C283($platform_i)
C_TEXT:C284($path_t)

If (False:C215)
	C_TEXT:C284(Syn_z7_GetVersion;$0)
End if 

$path_t:=Get 4D folder:C485(Database folder:K5:14)+"Plugins"+Folder separator:K24:12+"7z.bundle"+Folder separator:K24:12+"Contents"+Folder separator:K24:12

_O_PLATFORM PROPERTIES:C365($platform_i)

Case of 
	: ($platform_i=Mac OS:K25:2)
		
		If (Application type:C494=4D Remote mode:K5:5)
			SET ENVIRONMENT VARIABLE:C812("_4D_OPTION_CURRENT_DIRECTORY";$path_t+"MacOS")
			SET ENVIRONMENT VARIABLE:C812("_4D_OPTION_BLOCKING_EXTERNAL_PROCESS";"true")
			LAUNCH EXTERNAL PROCESS:C811("chmod 555 7z")
		End if 
		
		$path_t:=$path_t+"MacOS"+Folder separator:K24:12+"7z"
		
	: ($platform_i=Windows:K25:3)
		
		$isWin64_b:=(System folder:C487(Applications or program files:K41:17)="@(x86)\\")
		
		If ($isWin64_b)
			$path_t:=$path_t+"Windows64"+Folder separator:K24:12+"7z.exe"
		Else 
			$path_t:=$path_t+"Windows"+Folder separator:K24:12+"7z.exe"
		End if 
		
		SET ENVIRONMENT VARIABLE:C812("_4D_OPTION_HIDE_CONSOLE";"true")
		
End case 

$0:=SYN_LEP_EscapePath($path_t)