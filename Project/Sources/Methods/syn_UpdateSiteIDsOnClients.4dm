//%attributes = {"invisible":true,"executedOnServer":true}
C_TEXT:C284($1)

C_LONGINT:C283($Client_i; $NumberOfClients_i)

ARRAY LONGINT:C221($MethodsList_ai; 0)
ARRAY TEXT:C222($ClientList_at; 0)

If (False:C215)
	C_TEXT:C284(syn_UpdateSiteIDsOnClients; $1)
End if 
//trace
//Retrieve the registered clients in $ClientList
GET REGISTERED CLIENTS:C650($ClientList_at; $MethodsList_ai)
$NumberOfClients_i:=Size of array:C274($ClientList_at)

For ($Client_i; 1; $NumberOfClients_i)
	EXECUTE ON CLIENT:C651($ClientList_at{$Client_i}; "Syn_LocalSiteIdentifier"; $1)
	
End for 