//%attributes = {"invisible":true,"shared":true}
// Long name to path name Project Name
// Long name to path name ( String ) -> String
// Long name to path name ( Long file name ) -> Path name
C_TEXT:C284($0)
C_TEXT:C284($1)

C_LONGINT:C283($CharacterCode_i; $DirectorySymbolCharCode_i; $Length_i; $Position_i)

If (False:C215)
	C_TEXT:C284(Syn Path of Containing Folder; $0)
	C_TEXT:C284(Syn Path of Containing Folder; $1)
End if 

$DirectorySymbolCharCode_i:=Character code:C91(Folder separator:K24:12)
$Length_i:=Length:C16($1)
$Position_i:=0

For ($CharacterCode_i; $Length_i; 1; -1)
	If (Character code:C91($1[[$CharacterCode_i]])=$DirectorySymbolCharCode_i)
		$Position_i:=$CharacterCode_i
		$CharacterCode_i:=0
	End if 
End for 
If ($Position_i>0)
	$0:=Substring:C12($1; 1; $Position_i)
Else 
	$0:=$1
End if 