//%attributes = {"invisible":true}
// ----------------------------------------------------
// Method: Syn_ReinsertHiddenUpdates
// Description:
If (False:C215)
	Syn_ReinsertHiddenUpdates
End if   //
//
// Parameters:
// ----------------------------------------------------
// Created by Wayne Stewart (2015-08-09T14:00:00Z)
//     waynestewart@mac.com
// ----------------------------------------------------
C_LONGINT:C283($Bracket_i)
C_LONGINT:C283($NumberOfRecords_i)
C_LONGINT:C283($Position_i)
C_TEXT:C284($Prefix_t)
C_TEXT:C284($SyncID_t)

$SyncID_t:=Syn.SOE.ReinsertInQueue

$Bracket_i:=Num:C11(Storage:C1525.Local.SiteIdentifier)
$Bracket_i:=$Bracket_i+Syn Bump From Queue  //  These are the bumped records

$Prefix_t:=Storage:C1525.Local.SiteIdentifier[[1]]
$Prefix_t:=$Prefix_t+String:C10($Bracket_i)

READ WRITE:C146(sync_Table_ptr->)

QUERY:C277(sync_Table_ptr->; sync_ForIdentifier_ptr->=$Prefix_t; *)
QUERY:C277(sync_Table_ptr->;  & ; sync_Sync_ID_ptr->=$SyncID_t)

$NumberOfRecords_i:=Records in selection:C76(sync_Table_ptr->)

If ($NumberOfRecords_i>0)
	ARRAY TEXT:C222($ForIdentifiers_at; $NumberOfRecords_i)
	For ($Position_i; 1; $NumberOfRecords_i)
		$ForIdentifiers_at{$Position_i}:=Storage:C1525.Local.SiteIdentifier  //  Reset to the original value
	End for 
	ARRAY TO SELECTION:C261($ForIdentifiers_at; sync_ForIdentifier_ptr->)
	Syn_UnloadRecordReduceSelection(sync_Table_ptr)
End if 

$Position_i:=Find in array:C230(Syn_UpdatesOnHold_at; $SyncID_t)  //  There really should only be one?
While ($Position_i>-1)
	DELETE FROM ARRAY:C228(Syn_UpdatesOnHold_at; $Position_i)
	$Position_i:=Find in array:C230(Syn_UpdatesOnHold_at; $SyncID_t)
End while 


Syn.SOE.ReinsertInQueue:=""  //  Reset to blank