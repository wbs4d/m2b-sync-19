//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Syn_Log_Reveal

// Displays the Config folder
// only works on server or stand alone

// Created by Wayne Stewart (2021-04-21T14:00:00Z)

//     wayne@4dsupport.guru
// ----------------------------------------------------


var $message_t : Text

Case of 
	: (Application type:C494=4D Remote mode:K5:5)
		$message_t:="The files are stored on the server"
		
	: (Test path name:C476(Storage:C1525.Defaults.SyncFolder)=Is a folder:K24:2)
		SHOW ON DISK:C922(Storage:C1525.Defaults.SyncFolder; *)
		$message_t:=""
		
	Else 
		$message_t:="The Sync folder can't be located"
		
End case 

If (Length:C16($message_t)>0)
	If (Storage:C1525.Syn.foundationPresent)
		EXECUTE METHOD:C1007("Fnd_Dlg_Alert"; *; $message_t)
	Else 
		ALERT:C41($message_t)
	End if 
End if 



