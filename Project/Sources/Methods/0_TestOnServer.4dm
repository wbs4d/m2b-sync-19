//%attributes = {"invisible":true,"executedOnServer":true}
//000_DisplayDictionaries 

// 0_Testing
// Created by Wayne Stewart (Sep 10, 2013)
//  Method is an autostart type
//     waynestewart@mac.com



C_LONGINT:C283($1;$ProcessID_i)

If (False:C215)  //  Copy this to your Compiler Method!
	C_LONGINT:C283(0_TestOnServer;$1)
End if 

If (Count parameters:C259=1)
	
	ALERT:C41(String:C10(Syn Timing Delay))
	//
	//
	//For ($ProcessID_i;1;200)
	//CREATE RECORD([Client])
	//[Client]First_Name:="F "+<>sync_SiteIdentifier_c3+" "+String(Random)
	//[Client]Last_Name:="L "+String(Milliseconds)
	//[Client]Date_Contacted:=Current date
	//SAVE RECORD([Client])
	//Syn_UnloadRecordReduceSelection (->[Client])
	//End for 
	
Else 
	// This version allows for any number of processes
	// $ProcessID_i:=New Process(Current method name;512*1024;Current method name;0)
	// This version allows for one unique process
	$ProcessID_i:=New process:C317(Current method name:C684;512*1024;Current method name:C684;0;*)
	RESUME PROCESS:C320($ProcessID_i)
	SHOW PROCESS:C325($ProcessID_i)
	BRING TO FRONT:C326($ProcessID_i)
End if 

