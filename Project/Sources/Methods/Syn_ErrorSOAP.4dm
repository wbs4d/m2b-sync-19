//%attributes = {"invisible":true}

// ----------------------------------------------------
// Project Method: Syn_ErrorSOAP
If (False:C215)
	Syn_ErrorSOAP
End if 
// Method Type:    Private

// Parameters:

// Local Variables:

// Created by Wayne Stewart (Jul 6, 2013)
//     waynestewart@mac.com

//

// ----------------------------------------------------
C_BOOLEAN:C305($logging_b)
$logging_b:=LOG ENABLE
LOG ENABLE(True:C214)
Case of 
	: (Error=9910)
		If (Storage:C1525.Local.logErrors=1) | (Storage:C1525.Local.logEverything=1)
			LOG ADD ENTRY("Target: "+Storage:C1525.Local.ServerIdentifier;\
				Error Method;\
				"line: "+String:C10(Error Line);\
				"Err: "+String:C10(Error);\
				WEB SERVICE Get info:C780(Web Service detailed message:K48:6);\
				WEB SERVICE Get info:C780(Web Service fault actor:K48:8))
		End if 
		
	: (Error=9913)  //  This one there is a second test - only log if they want to see connection errors
		If ((Storage:C1525.Local.logConnectionErrors=1) | (Storage:C1525.Local.logEverything=1) | (Storage:C1525.Local.logErrors=1))
			LOG ADD ENTRY("Target: "+Storage:C1525.Local.ServerIdentifier;Error Method;"line: "+String:C10(Error Line);"Err: "+String:C10(Error);WEB SERVICE Get info:C780(Web Service detailed message:K48:6))
		End if 
		
	Else 
		If (Storage:C1525.Local.logErrors=1) | (Storage:C1525.Local.logEverything=1)
			LOG ADD ENTRY("Target: "+Storage:C1525.Local.ServerIdentifier;Error Method;"line: "+String:C10(Error Line);"Err: "+String:C10(Error);WEB SERVICE Get info:C780(Web Service detailed message:K48:6))
		End if 
		
End case 

OK:=0  //  Need to set this back to 0 as Syn_Log_AddEntry will set it to 1
LOG ENABLE($logging_b)