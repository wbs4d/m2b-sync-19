//%attributes = {"invisible":true}
// ----------------------------------------------------
// Project Method: Compiler_Sync

// Compiler variables related to the Sync routines.

// Method Type: Private

// Parameters: None

// Returns: Nothing

// Created by Wayne Stewart (Jul 22, 2012)
//     waynestewart@mac.com
// ----------------------------------------------------



// Process Variables
If (Syn=Null:C1517)  // So we only do this once.
	C_OBJECT:C1216(Syn)
	
	// Why are we still using Process Variables?
	// 4D Docs: You must use process variables only
	// https://doc.4d.com/4Dv18R6/4D/18-R6/WEB-SERVICE-GET-RESULT.301-5198973.en.html
	
	C_TEXT:C284(syn_SmallFieldData_t; syn_PK_t; syn_ID_t; syn_JournalID_t)
	C_BLOB:C604(syn_LargeData_x)
	C_LONGINT:C283(syn_RecordsInQueue_i)
	
	// These used to be IP variables but moved to process variable so we can run preemptive
	// I considered putting them into the Syn onject but you can't de-reference an object property
	ARRAY POINTER:C280(sync_CachedStructure_aptr; 0)
	C_POINTER:C301(sync_Table_ptr; sync_PK_ptr; sync_Sync_ID_ptr; sync_ForIdentifier_ptr; sync_SmallFieldData_ptr; sync_LargeFieldData_ptr; sync_NumberOfRetries_ptr; sync_DTS_Received_ptr; sync_Update_Type_ptr; sync_DTS_Created_ptr; sync_JournalID_ptr)
	
	ARRAY TEXT:C222(Syn_UpdatesOnHold_at; 0)
	
	C_TEXT:C284(Syn_Target_t; Syn_Origin_t; Syn_ServerAddress_t)
	
End if 

// Parameters
If (False:C215)
	
	
	
	
	C_DATE:C307(Syn_DateToText; $1)
	C_TEXT:C284(Syn_DateToText; $0)
	
	C_DATE:C307(Syn_DateFromText; $0)
	C_TEXT:C284(Syn_DateFromText; $1)
	
	C_TIME:C306(Syn_TimeToText; $1)
	C_TEXT:C284(Syn_TimeToText; $0)
	
	C_TIME:C306(Syn_TimeFromText; $0)
	C_TEXT:C284(Syn_TimeFromText; $1)
	
	
	
	
	
	C_TEXT:C284(Syn_ApplAlert; $1)
	C_POINTER:C301(Syn_SendUpdatesTo; $1)
	C_TEXT:C284(Syn_SendUpdatesTo; $2)
	
	
	C_BOOLEAN:C305(Syn_ThisTable; $0)
	C_LONGINT:C283(Syn_ThisTable; $1)
	C_POINTER:C301(Syn_ThisTable; $2)
	
	C_OBJECT:C1216(Syn_ObjectSaveToFile; $1)
	C_TEXT:C284(Syn_ObjectSaveToFile; $2)
	C_BOOLEAN:C305(Syn_ObjectSaveToFile; $3)
	
	C_OBJECT:C1216(Syn_ObjectLoadFromFile; $0)
	C_TEXT:C284(Syn_ObjectLoadFromFile; $1)
	
	C_VARIANT:C1683(Syn_Time; $1; $0)
	C_VARIANT:C1683(Syn_Date; $1; $0)
	
	C_POINTER:C301(Syn_DetectDuplicates; $1; $2)
	
	C_TEXT:C284(Syn_Info; $1; $0)
	
	C_LONGINT:C283(Syn_ApplyUpdates; $1)
	
	C_TEXT:C284(Syn_CreateRecord; $0)
	C_OBJECT:C1216(Syn_CreateRecord; $1)
	
	C_TEXT:C284(Syn_UpdateRecord; $0)
	C_OBJECT:C1216(Syn_UpdateRecord; $1)
	
	C_TEXT:C284(Syn_DeleteRecord; $0)
	C_OBJECT:C1216(Syn_DeleteRecord; $1)
	
	C_TEXT:C284(Syn_BugAlert; $1; $2)
	
	C_OBJECT:C1216(Syn_ExtractDictionary; $0)
	
	C_TEXT:C284(Syn_LongNameToFileName; $1; $0)
	
	C_LONGINT:C283(syn_Quit; $1; $0)
	
	C_BOOLEAN:C305(syn_CloseDialog; $1; $0)
	
	C_LONGINT:C283(syn_DisplayPreferences; $1)
	
	C_TEXT:C284(Syn_GetServerAddress; $0)
	
	C_LONGINT:C283(syn_RecordsInQueue; $0)
	C_TEXT:C284(syn_RecordsInQueue; $1)
	
	C_BOOLEAN:C305(Syn_ThisTable; $0)
	C_LONGINT:C283(Syn_ThisTable; $1)
	C_POINTER:C301(Syn_ThisTable; $2)  //;$3)
	
	C_BOOLEAN:C305(Syn_PerformingUpdate; $0)
	
	C_POINTER:C301(Syn_SendUpdatesTo; $1)
	
	C_BOOLEAN:C305(syn_EqualStrings; $0)
	C_TEXT:C284(syn_EqualStrings; $1)
	C_TEXT:C284(syn_EqualStrings; $2)
	
	C_BOOLEAN:C305(Syn_RecordLoad; $0)
	C_POINTER:C301(Syn_RecordLoad; $1)
	
	C_LONGINT:C283(Syn_GreaterOf; $0)
	C_LONGINT:C283(Syn_GreaterOf; $1)
	C_LONGINT:C283(Syn_GreaterOf; $2)
	
	C_LONGINT:C283(Syn_LesserOf; $0)
	C_LONGINT:C283(Syn_LesserOf; $1)
	C_LONGINT:C283(Syn_LesserOf; $2)
	
	C_VARIANT:C1683(Syn_UnloadRecordReduceSelection; $1)
	
	C_TEXT:C284(syn_S_SendUpdate; $1; $2)
	C_TEXT:C284(syn_C_RequestUpdate; $1; $2)
	
	C_TEXT:C284(syn_S_DeleteSyncRecord; $1; $2; $3)
	C_TEXT:C284(syn_C_DeleteSyncRecord; $1; $2; $3)
	
	C_TEXT:C284(syn_S_ReceiveUpdate; $0; $1; $2; $3; $4; $5)
	C_BLOB:C604(syn_S_ReceiveUpdate; $6)
	
	C_TEXT:C284(syn_C_SendUpdate; $0; $1; $2; $3; $4; $5)
	C_BLOB:C604(syn_C_SendUpdate; $6)
	
	
	C_LONGINT:C283(syn_C_RequestUpdate; $0)
	C_TEXT:C284(syn_C_RequestUpdate; $1)
	C_TEXT:C284(syn_C_RequestUpdate; $2)
	
	C_TEXT:C284(Syn_PushToServer; $1)
	C_LONGINT:C283(Syn_PushToServer; $2)
	
	C_TEXT:C284(Syn_Pref_RemoveIdentifier; $1)
	
	C_TEXT:C284(Syn_PullFromServer; $1)
	C_LONGINT:C283(Syn_PullFromServer; $2)
	
	
	
	
	
	C_TEXT:C284(Syn_LocalSiteIdentifier; $1; $0)
	C_TEXT:C284(Syn_SiteBracketForTSIdentifier; $0)
	
	C_LONGINT:C283(syn_Monitor; $1)
	
	C_TEXT:C284(Syn_GetSiteIdentifierFromServer; $0)
	
	C_OBJECT:C1216(syn_GetPrefsFromServer; $0)
	
	C_TEXT:C284(syn_UpdateSiteIDsOnClients; $1)
	
	C_OBJECT:C1216(syn_SavePrefsOnServer; $1)
	
	C_LONGINT:C283(Syn_OnQuit; $1)
	
	//C_TEXT(File_LongNameToPathName; $0; $1)
	
	C_TEXT:C284(Syn_S_CheckQueueLength; $1; $2)
	
	C_LONGINT:C283(Syn_C_CheckQueueLength; $0)
	C_TEXT:C284(Syn_C_CheckQueueLength; $1; $2)
	
	C_TEXT:C284(syn_LoadFirstRecordInQueue; $1)
	
	C_LONGINT:C283(Syn_CheckDuplicateKeys; $0)
	C_POINTER:C301(Syn_CheckDuplicateKeys; $1)
	C_BOOLEAN:C305(Syn_CheckDuplicateKeys; $2)
	
	C_TEXT:C284(Syn_DeleteSpecifiedQueueItem; $1)
	C_TEXT:C284(syn_LoadFirstRecordInQueue; $0)
	C_TEXT:C284(syn_LoadFirstRecordInQueue; $1)
	
	
	
	C_LONGINT:C283(Syn_ConvertPasswordIntoKey; $0)
	C_TEXT:C284(Syn_ConvertPasswordIntoKey; $1)
	
	C_TEXT:C284(Syn_HideUpdate; $1; $2; $3)
	
	C_TEXT:C284(Syn_CheckForOnHold; $1)
	
	C_TEXT:C284(TS_GetTimeStamp; $0)
	//C_DATE(TS_GetTimeStamp; $2)
	//C_TIME(TS_GetTimeStamp; $3)
	C_TEXT:C284(TS_GetTimeStamp; $1)
	
	
	
	C_TEXT:C284(Syn Path of Containing Folder; $0)
	C_TEXT:C284(Syn Path of Containing Folder; $1)
	
	C_TEXT:C284(Syn_NewSyncRecords; $1)
	C_TEXT:C284(Syn_NewSyncRecords; $2)
	C_OBJECT:C1216(Syn_NewSyncRecords; $3)
	C_BLOB:C604(Syn_NewSyncRecords; $4)
	C_TEXT:C284(Syn_NewSyncRecords; $5; $6)
	
	C_OBJECT:C1216(Syn_OBJ_IsEqual; $1; $2)
	C_BOOLEAN:C305(Syn_OBJ_IsEqual; $0)
	
End if 


