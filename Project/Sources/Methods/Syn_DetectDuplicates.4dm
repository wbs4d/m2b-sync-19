//%attributes = {}

// ----------------------------------------------------
// Project Method: Syn_DetectDuplicates
C_POINTER:C301($1; $2)

C_LONGINT:C283($CurrentValue_i; $NumberOfDistinctValues_i; $NumberOfRecords_i)
C_POINTER:C301($IDField_ptr; $Table_ptr)

ARRAY TEXT:C222($DuplicateIDs_at; 0)
ARRAY TEXT:C222($IDValues_at; 0)

If (False:C215)
	C_POINTER:C301(Syn_DetectDuplicates; $1; $2)
	
End if 

// Looks for duplicate Record ID

// Access: Private

// Created by Wayne Stewart (2016-11-15)
//     wayne@4dsupport.guru
// ----------------------------------------------------

If (Count parameters:C259=1)
	$Table_ptr:=$1
Else 
	$Table_ptr:=->[BulkTest:2]
	
End if 

If (Count parameters:C259=2)
	$IDField_ptr:=$2
Else 
	$IDField_ptr:=->[BulkTest:2]ID:1
End if 

ALL RECORDS:C47($Table_ptr->)
DISTINCT VALUES:C339($IDField_ptr->; $IDValues_at)

$NumberOfDistinctValues_i:=Size of array:C274($IDValues_at)
$NumberOfRecords_i:=Records in table:C83($Table_ptr->)

ALERT:C41(String:C10($NumberOfDistinctValues_i)+" distinct values and "+String:C10($NumberOfRecords_i)+" records.")
SET QUERY DESTINATION:C396(Into variable:K19:4; $NumberOfRecords_i)  //  reuse variable
For ($CurrentValue_i; 1; $NumberOfDistinctValues_i)
	QUERY:C277($Table_ptr->; $IDField_ptr->=$IDValues_at{$CurrentValue_i})
	If ($NumberOfRecords_i>1)
		APPEND TO ARRAY:C911($DuplicateIDs_at; $IDValues_at{$CurrentValue_i})
	End if 
End for 

SET QUERY DESTINATION:C396(Into current selection:K19:1)

QUERY WITH ARRAY:C644($IDField_ptr->; $DuplicateIDs_at)

TRACE:C157
