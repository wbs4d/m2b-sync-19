//%attributes = {"invisible":true}
// syn_C_DeleteSyncRecord
//
//PRIVATE METHOD
//
//This method calls the web server and requests that it
//delete the [Sync] record on the server with the PK
//field as specified.
//
//PARAMETERS
//
//Address : Text : Web address of the server
//PK      : Text : Primary key of record to delete.
// ----------------------------------------------------------------
C_TEXT:C284($1;$SOAPServerAddress_t)
C_TEXT:C284($2;$JournalID_t)
C_TEXT:C284($3;$Target_t)
C_TEXT:C284($Origin_t)
C_BOOLEAN:C305($logging_b)
If (False:C215)
	C_TEXT:C284(syn_C_DeleteSyncRecord;$1;$2;$3)
	
	//  Calls via SOAP
	syn_S_DeleteSyncRecord($2;Storage:C1525.Local.SiteIdentifier;$3)
	
End if 

Syn_Init

$SOAPServerAddress_t:=$1
$JournalID_t:=$2
$Target_t:=$3
$Origin_t:=Storage:C1525.Local.SiteIdentifier
WEB SERVICE SET PARAMETER:C777("Journal_ID";$JournalID_t)
WEB SERVICE SET PARAMETER:C777("origin";$Origin_t)
WEB SERVICE SET PARAMETER:C777("target";$Target_t)

WEB SERVICE CALL:C778($SOAPServerAddress_t;"A_WebService#syn_S_DeleteSyncRecord";"syn_S_DeleteSyncRecord";"http://www.4d.com/namespace/default";Web Service dynamic:K48:1)

If (Storage:C1525.Local.logTransmissions=1)
	$logging_b:=LOG ENABLE
	LOG ENABLE(True:C214)
	LOG USE LOG("TransmissionLog")
	LOG ADD ENTRY(\
		"DelSync";\
		"T "+$Target_t+\
		" O "+$Origin_t+\
		" L "+$Origin_t;\
		$JournalID_t)
	LOG USE LOG("ErrorLog")
	LOG ENABLE($logging_b)
End if 
