//%attributes = {"invisible":true}
C_TEXT:C284($1;$2;$3;$CallingMethod_t;$ResultCode_t;$RecordID_t;$JournalID_t)

If (False:C215)
	C_TEXT:C284(Syn_DeleteSpecifiedQueueItem;$1;$2;$3)
End if 
C_BOOLEAN:C305($logging_b)
$logging_b:=LOG ENABLE
LOG ENABLE(True:C214)
$JournalID_t:=$1
$CallingMethod_t:=$2
$ResultCode_t:=$3

QUERY:C277(sync_Table_ptr->;sync_JournalID_ptr->=$JournalID_t)  //  Shouldn't need to do this query
If (Syn_RecordLoad(sync_Table_ptr))  //  This will attempt to load the record for 18 minutes and then give up
	$RecordID_t:=sync_Sync_ID_ptr->
	DELETE RECORD:C58(sync_Table_ptr->)
	
	If (Storage:C1525.Local.logEverything=1)
		LOG ADD ENTRY(Current method name:C684;Syn OK;$CallingMethod_t;$ResultCode_t;$JournalID_t)
	End if 
Else 
	
	Case of 
		: (Storage:C1525.Local.logEverything=1) | (Storage:C1525.Local.logErrors=1)
			LOG ADD ENTRY(Current method name:C684;Syn Err Locked Record;$CallingMethod_t;$ResultCode_t;$JournalID_t)
	End case 
	
End if 
LOG ENABLE($logging_b)