//%attributes = {"invisible":true}
C_TEXT:C284($0)
C_TEXT:C284($1)

If (False:C215)
	C_TEXT:C284(syn_LoadFirstRecordInQueue; $0)
	C_TEXT:C284(syn_LoadFirstRecordInQueue; $1)
End if 

SET QUERY DESTINATION:C396(Into current selection:K19:1)
QUERY:C277(sync_Table_ptr->; sync_ForIdentifier_ptr->=$1)
ORDER BY:C49(sync_Table_ptr->; sync_PK_ptr->; >)  //  Make certain the "lowest value" is at front
FIRST RECORD:C50(sync_Table_ptr->)  //  I think this is unnecessary, it should be loaded

$0:=sync_JournalID_ptr->

