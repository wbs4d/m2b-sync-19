//%attributes = {"invisible":true,"shared":true}
C_BOOLEAN:C305(<>sync_FoundationPresent_b)

If (Test path name:C476(<>Syn_Log_Path_t)=Is a document:K24:1)
	SHOW ON DISK:C922(<>Syn_Log_Path_t;*)
Else 
	If (<>sync_FoundationPresent_b)
		EXECUTE METHOD:C1007("Fnd_Dlg_Alert";*;"No Error Log Available")
	Else 
		ALERT:C41("No Error Log Available")
	End if 
End if 