//%attributes = {"invisible":true}

// ----------------------------------------------------
// Project Method: Syn_GetServerAddress

// Method Type:    Private

// Parameters:
C_TEXT:C284($0)

// Local Variables:
C_LONGINT:C283($Port_i)
C_TEXT:C284($ipAddress_t; $ServerAddress_t)

// Created by Wayne Stewart (Sep 30, 2013)
//     waynestewart@mac.com

//   

// ----------------------------------------------------

Syn_Init

If (False:C215)
	C_TEXT:C284(Syn_GetServerAddress; $0)
End if 

$ipAddress_t:=Storage:C1525.Local.ServerAddress
$Port_i:=Storage:C1525.Local.Port
$ServerAddress_t:=$ipAddress_t+":"+String:C10($Port_i)

$0:="http://"+$ServerAddress_t+"/4DSOAP/"