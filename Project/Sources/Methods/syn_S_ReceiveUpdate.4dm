//%attributes = {"invisible":true,"shared":true,"publishedSoap":true,"publishedWsdl":true}
C_TEXT:C284($0)
C_TEXT:C284($1)
C_TEXT:C284($2)
C_TEXT:C284($3)
C_TEXT:C284($4)
C_TEXT:C284($5)
C_BLOB:C604($6)

C_BLOB:C604($LargeFieldData_x)
C_OBJECT:C1216($Packet_o)
C_TEXT:C284($Action_t;$Origin_t;$PK_t;$SmallFieldData_t;$Sync_ID_t;$Target_t)

If (False:C215)
	C_TEXT:C284(syn_S_ReceiveUpdate;$0)
	C_TEXT:C284(syn_S_ReceiveUpdate;$1)
	C_TEXT:C284(syn_S_ReceiveUpdate;$2)
	C_TEXT:C284(syn_S_ReceiveUpdate;$3)
	C_TEXT:C284(syn_S_ReceiveUpdate;$4)
	C_TEXT:C284(syn_S_ReceiveUpdate;$5)
	C_BLOB:C604(syn_S_ReceiveUpdate;$6)
End if 

Syn_Init

// ----------------------------------------------------
// User name (OS): Wayne Stewart
// Date and time: Apr 18, 2013
If (False:C215)
	syn_S_ReceiveUpdate
	
	// Called by:
	syn_C_SendUpdate
	
End if 
// ----------------------------------------------------
// Method: syn_S_ReceiveUpdate
// Description
//   This method allows a sync client to push updates to
//   a server
// Parameters
// ----------------------------------------------------

SOAP DECLARATION:C782($1;Is text:K8:3;SOAP input:K46:1;"target")
SOAP DECLARATION:C782($2;Is text:K8:3;SOAP input:K46:1;"origin")

SOAP DECLARATION:C782($3;Is text:K8:3;SOAP input:K46:1;"PK")
SOAP DECLARATION:C782($4;Is text:K8:3;SOAP input:K46:1;"Sync_ID")

SOAP DECLARATION:C782($5;Is text:K8:3;SOAP input:K46:1;"SmallFieldData")
SOAP DECLARATION:C782($6;Is BLOB:K8:12;SOAP input:K46:1;"LargeData")

SOAP DECLARATION:C782($0;Is text:K8:3;SOAP output:K46:2;"Result")

$Target_t:=$1
$Origin_t:=$2
$PK_t:=$3
$Sync_ID_t:=$4
$SmallFieldData_t:=$5
$LargeFieldData_x:=$6

Syn_Init

Syn_TRACE

If (Storage:C1525.Local.logTransmissions=1)
	C_BOOLEAN:C305($logging_b)
	$logging_b:=LOG ENABLE
	LOG ENABLE(True:C214)
	LOG USE LOG("TransmissionLog")
	LOG ADD ENTRY($2;Current method name:C684;$1;$3)
	LOG USE LOG("ErrorLog")
	LOG ENABLE($logging_b)
End if 

If ($Target_t=Storage:C1525.Local.SiteIdentifier)
	QUERY:C277(sync_Table_ptr->;sync_PK_ptr->=$PK_t;*)  //  You can have multiples with the same PK
	QUERY:C277(sync_Table_ptr->; & ;sync_ForIdentifier_ptr->=$Target_t)  // but not with the same For
	
	If (Records in selection:C76(sync_Table_ptr->)>0)
		$0:=Syn Push Duplicate Record  //  This shouldn't happen!
		//  If it does, let the sender know
	Else 
		
		$Packet_o:=JSON Parse:C1218($SmallFieldData_t)
		If ($Packet_o#Null:C1517)
			$Action_t:=$Packet_o.Act
			
			// The server has just received an update from a client
			
			//  Create multiple copies
			//  One for this machine
			//  One for its server
			//  One for each of its clients, except the one it came from
			Syn_NewSyncRecords($PK_t;$Sync_ID_t;$Packet_o;$LargeFieldData_x;$Action_t;Current method name:C684)
			
			
		Else 
			
			$0:=Syn Err Invalid JSON
		End if 
		
		//$DictionaryID_i:=JSON Parse Text($SmallFieldData_t)
		//If (JSON Is Valid Object($DictionaryID_i)=1)
		//$Action_t:=JSON Get Text($DictionaryID_i; Syn Action)
		
		//// The server has just received an update from a client
		
		////  Create multiple copies
		////  One for this machine
		////  One for its server
		////  One for each of its clients, except the one it came from
		//Syn_NewSyncRecords($PK_t; $Sync_ID_t; String($DictionaryID_i); $LargeFieldData_x; $Action_t; Current method name)
		
		//JSON Release($DictionaryID_i)
		
		
		
		//$0:=Syn OK
		
		//Else 
		//$0:=Syn Err Invalid JSON
		//End if 
		
	End if 
Else 
	$0:=Syn Push Wrong Target  // Discard everything but send response saying so…
End if 