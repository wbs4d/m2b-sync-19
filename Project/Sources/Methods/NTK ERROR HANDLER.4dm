//%attributes = {"invisible":true,"shared":true}
C_LONGINT:C283($1)
C_LONGINT:C283($2)
C_TEXT:C284($3)

C_BOOLEAN:C305($Logging_b)
C_LONGINT:C283($ErrorClass_i;$Socket_i)
C_TEXT:C284($ErrorClass_t;$ErrorMessage_t;$Socket_t)

If (False:C215)
	C_LONGINT:C283(NTK ERROR HANDLER;$1)
	C_LONGINT:C283(NTK ERROR HANDLER;$2)
	C_TEXT:C284(NTK ERROR HANDLER;$3)
End if 

// ----------------------------------------------------
// User name (OS): Wayne Stewart
// Date and time: Aug 8, 2014
If (False:C215)
	NTK ERROR HANDLER
End if 
// ----------------------------------------------------
// Method: NTK ERROR HANDLER

// Description
//
// The 4D method should handle and declare the following
// parameters:
//
// Parameter    Description     Type
// $1           Socket          longint
// $2           Error class     longint
// $3           Error message   text
//
// The error class parameter defines which subsystem
// triggered the error. You can use the following
// constants:
// • NTK Parameter Error
//     An invalid parameter was passed to the plugin.
// • NTK OS Error
//     An error occurred at the OS level.
// • NTK OS Warning
//     An error occurred at the OS level, but the error should
//         be considered more or less a warning.
// • NTK SSL Error
//     An error occurred at the SSL level (OpenSSL).
// • NTK SSL Warning
//     An error occurred at the SSL level (OpenSSL), but the
//     error should be considered more or less a warning.
// • NTK JSON Error
//     An error occurred in the JSON code.

// Parameters

// ----------------------------------------------------

$Socket_i:=$1
$ErrorClass_i:=$2
$ErrorMessage_t:=$3

$Socket_t:=String:C10($Socket_i)

Case of 
	: ($ErrorClass_i=NTK Parameter Error)
		$ErrorClass_t:="NTK Parameter Error"
		
	: ($ErrorClass_i=NTK OS Error)
		$ErrorClass_t:="NTK OS Error"
		
	: ($ErrorClass_i=NTK OS Warning)
		$ErrorClass_t:="NTK OS Warning"
		
	: ($ErrorClass_i=NTK SSL Error)
		$ErrorClass_t:="NTK SSL Error"
		
	: ($ErrorClass_i=NTK SSL Warning)
		$ErrorClass_t:="NTK SSL Warning"
		
	: ($ErrorClass_i=NTK JSON Error)
		$ErrorClass_t:="NTK JSON Error"
		
End case 

//SET TEXT TO PASTEBOARD($ErrorMessage_t)

//$ErrorMessage_t:=Replace string($ErrorMessage_t;<>Syn_Log_RecordDelimiter_t;"\t")
//$ErrorMessage_t:=Replace string($ErrorMessage_t;Char(Line feed);"\t")
$ErrorMessage_t:=Replace string:C233($ErrorMessage_t;Char:C90(Carriage return:K15:38);"\t")

If (KVP_Longint(Syn Log Everything)=1) | (KVP_Longint(Syn Log Connection Errors)=1)
	Syn_Log_AddEntry(Syn_GetTimeStamp(Syn TS DateTime Padded);Current method name:C684;$ErrorClass_t;$ErrorMessage_t;$Socket_t)
End if 



