//%attributes = {"invisible":true,"shared":true}
//  This method will kill any running sync processes (hopefully)
C_LONGINT:C283($1)

C_LONGINT:C283($CurrentProcess_i;$NumberOfTasks_i;$procState_i;$procTime_i;$QuitLevel_i)
C_TEXT:C284($procName_t)

If (False:C215)
	C_LONGINT:C283(Syn_OnQuit;$1)
End if 

$NumberOfTasks_i:=Count tasks:C335

If (Count parameters:C259=1)
	$QuitLevel_i:=$1
Else 
	$QuitLevel_i:=2
End if 



syn_Quit($QuitLevel_i)  //  Tell  the component that we are quitting

For ($CurrentProcess_i;1;$NumberOfTasks_i)  //  Loop through the tasks one by one & kill them
	
	PROCESS PROPERTIES:C336($CurrentProcess_i;$procName_t;$procState_i;$procTime_i)
	If ($procName_t="syn@")
		RESUME PROCESS:C320($CurrentProcess_i)
		DELAY PROCESS:C323(Current process:C322;60)
		RESUME PROCESS:C320($CurrentProcess_i)
	End if 
End for 

