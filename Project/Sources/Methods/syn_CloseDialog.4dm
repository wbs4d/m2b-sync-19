//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: syn_CloseDialog (Boolean) --> Boolean

// Call this method from the Host to tell
// any Sync component windowed processes to close.
// True = CLOSE WINDOW

// Access: Shared

// Parameters: 
//   $1 : Type : Description
//   $x : Type : Description (optional)

// Returns: 
//   $0 : Type : Description

// Created by Wayne Stewart (2021-04-21T14:00:00Z)
//     wayne@4dsupport.guru
// ----------------------------------------------------

Syn_Init  // Just so we know everything is declared

If (Count parameters:C259=1)
	Use (Storage:C1525.Syn)
		Storage:C1525.Syn.closeDialog:=$1
	End use 
End if 

$0:=Storage:C1525.Syn.closeDialog
