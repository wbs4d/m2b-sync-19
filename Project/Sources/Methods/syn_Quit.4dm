//%attributes = {"invisible":true}
// ----------------------------------------------------
// Project Method: syn_Quit ({Quit Level}) -> Number

// You should call the Syn_OnQuit method instead.
// This method sets a simple flag, the other method
// actually polls to kill the processes.

// Access: Private

// Parameters: 
//   $1 : Longint : Description

// Returns: 
//   $0 : Longint : Description

// Created by Wayne Stewart (2021-04-21T14:00:00Z)
//     wayne@4dsupport.guru
// ----------------------------------------------------


If (False:C215)
	C_LONGINT:C283(syn_Quit; $1; $0)
End if 

C_LONGINT:C283($1; $0)

Syn_Init

If (Count parameters:C259=1)
	Use (Storage:C1525.Syn)
		Storage:C1525.Syn.QuitLevel:=$1
	End use 
End if 

$0:=Storage:C1525.Syn.QuitLevel



