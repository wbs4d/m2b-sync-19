//%attributes = {"invisible":true}
// ----------------------------------------------------
// Project Method: Syn_ExtractDictionary
If (False:C215)
	C_OBJECT:C1216(Syn_ExtractDictionary;$0)
End if 
// Method Type:    Private

// Parameters:

// Returns: A Dictionary Reference
C_OBJECT:C1216($0)
// Local Variables:

// Created by Wayne Stewart (Jul 6, 2013)
//     waynestewart@mac.com

//   This method assumes a [sync] record is loaded

// ----------------------------------------------------
C_BOOLEAN:C305($logging_b)
$logging_b:=LOG ENABLE
LOG ENABLE(True:C214)

//  New Version
If (Length:C16(sync_SmallFieldData_ptr->)=0)
	Case of 
		: (Storage:C1525.Local.logEverything=1)\
			 | (Storage:C1525.Local.logErrors=1)
			LOG ADD ENTRY(sync_PK_ptr->;\
				Current method name:C684;\
				Syn Err JSON Empty;\
				sync_Sync_ID_ptr->)
	End case 
	
	$0:=New object:C1471  // Return an empty object
	
Else 
	$0:=JSON Parse:C1218(sync_SmallFieldData_ptr->)
End if 

LOG ENABLE($logging_b)