//%attributes = {"shared":true}
// ----------------------------------------------------
// Project Method: Syn_Date (Time or String) --> String or Time

// Automagically converts a  time to a string or vice versa
//  !25/4/1915! becomes "1915-04-25"
//  "1915-04-25" becomes !25/4/1915!

// Access: Shared

// Parameters: 
//   $1 : Variant : A time or a string

// Returns: 
//   $0 : Variant : The converted string or time

// Created by Wayne Stewart (2021-04-21T14:00:00Z)
//     wayne@4dsupport.guru
// ----------------------------------------------------


C_VARIANT:C1683($1; $0)


If (Value type:C1509($1)=Is time:K8:8)
	$0:=String:C10($1; HH MM SS:K7:1)
Else 
	$0:=Time:C179($1)
End if 