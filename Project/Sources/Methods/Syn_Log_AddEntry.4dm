//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Log_AddEntry(text1{;text2..textN})
C_TEXT:C284(${1})

C_LONGINT:C283($parameter_i)
C_TIME:C306($docRef_h)
C_TEXT:C284($logEntry_t;$logItem_t)

If (False:C215)
	C_TEXT:C284(Syn_Log_AddEntry;${1})
End if 

// Call this method to add a new entry to the log file.
// Safe to call even if we're not currently logging activity.

// Access: Shared

// Parameters:
// $1 : Text : Any text
// $2..N : Text : Any text(optional)

// Returns: None

// Created by Dave Batton on May 25, 2004
// ----------------------------------------------------

Syn_Init

If (<>Syn_Log_Enabled_b)
	
	// Syn_CheckLogSize 
	
	$logEntry_t:=$1
	$logEntry_t:=Replace string:C233($logEntry_t;Char:C90(Tab:K15:37);"\t")
	$logEntry_t:=Replace string:C233($logEntry_t;<>Syn_Log_RecordDelimiter_t;"\r")
	For ($parameter_i;2;Count parameters:C259)
		$logItem_t:=${$parameter_i}
		$logItem_t:=Replace string:C233($logItem_t;Char:C90(Tab:K15:37);"\t")
		$logItem_t:=Replace string:C233($logItem_t;<>Syn_Log_RecordDelimiter_t;"\r")
		$logEntry_t:=$logEntry_t+Char:C90(Tab:K15:37)+$logItem_t
	End for 
	
	// Add the current date and time to the log entry.
	$logEntry_t:=String:C10(Current date:C33;Internal date short:K1:7)+Char:C90(Tab:K15:37)+String:C10(Current time:C178;HH MM SS:K7:1)+Char:C90(Tab:K15:37)+$logEntry_t+<>Syn_Log_RecordDelimiter_t
	
	If (Not:C34(Semaphore:C143(<>Syn_Log_Semaphore_t;300)))  // Wait up to 5 seconds if the semaphore already exists.
		If (Test path name:C476(<>Syn_Log_Path_t)=Is a document:K24:1)
			$docRef_h:=Append document:C265(<>Syn_Log_Path_t)
		Else 
			$docRef_h:=Create document:C266(<>Syn_Log_Path_t;"TEXT")
			If (OK=1)
				If (<>Syn_Log_Platform_i#Windows:K25:3)
					_O_SET DOCUMENT CREATOR:C531(<>Syn_Log_Path_t;"R*ch")
				End if 
			End if 
		End if 
		
		If (OK=1)
			SEND PACKET:C103($docRef_h;$logEntry_t)
			CLOSE DOCUMENT:C267($docRef_h)
		End if 
		CLEAR SEMAPHORE:C144(<>Syn_Log_Semaphore_t)
	End if 
End if 