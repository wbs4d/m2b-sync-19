//%attributes = {"invisible":true}
// Syn_NewSyncRecords(PK; Sync_ID; Object; Blob; Update Type; Called From )
C_TEXT:C284($1)
C_TEXT:C284($2)
C_OBJECT:C1216($3)
C_BLOB:C604($4)
C_TEXT:C284($5; $6)

C_BLOB:C604($LargeFieldData_x)
C_BOOLEAN:C305($JSON_Release_b)
C_LONGINT:C283($CurrentTarget_i; $JSON_i; $NumberOfTargets_i; $UpdateOrigin_i)
C_TEXT:C284($Action_t; $PK_t; $SyncID_t; $Target_t; $CallingMethod_t; $DTS_Created_t; $DTS_Received_t; $Packet_t)
C_OBJECT:C1216($Packet_o)

ARRAY TEXT:C222($Targets_at; 0)

If (False:C215)
	C_TEXT:C284(Syn_NewSyncRecords; $1)
	C_TEXT:C284(Syn_NewSyncRecords; $2)
	C_OBJECT:C1216(Syn_NewSyncRecords; $3)
	C_BLOB:C604(Syn_NewSyncRecords; $4)
	C_TEXT:C284(Syn_NewSyncRecords; $5; $6)
	
End if 

//  This method creates as many sync records as are required

$PK_t:=$1
$SyncID_t:=$2
$Packet_o:=$3
$LargeFieldData_x:=$4
$Action_t:=$5
$CallingMethod_t:=$6

If ($Packet_o#Null:C1517)  // Check it's valid
	
	
	If (Length:C16($Action_t)=0)  //  Did we get this as a parameter?
		$Action_t:=$Packet_o.Act  //  If not, read it from the packet
	End if 
	
	Syn_SendUpdatesTo(->$Targets_at; $CallingMethod_t)  //  Retrieve the lists of sites to inform
	
	// Remove the 'bounce back' sync target
	//  Circumstances:
	//   1. Creation in a trigger - Syn Origin will be local identifier
	//   2. Creation when Server receives update - Syn Via should be filtered
	//   3. Creation when Client downloads update - Syn Via should be filtered
	// Probably won't need to filter Syn Origin
	
	If (Size of array:C274($Targets_at)>0)  // Any targets?
		If ($Packet_o.Origin#Null:C1517)  //  Does it have the origin set?
			$UpdateOrigin_i:=Find in array:C230($Targets_at; $Packet_o.Origin)  // Where?
			If ($UpdateOrigin_i>0)  //  If it is present in list of targets then remove it
				DELETE FROM ARRAY:C228($Targets_at; $UpdateOrigin_i)
			End if 
		End if 
	End if 
	
	
	If (Size of array:C274($Targets_at)>0)  // Any targets?
		If ($Packet_o.Via#Null:C1517)  //  Does it have a via (immediate origin) property?
			$UpdateOrigin_i:=Find in array:C230($Targets_at; $Packet_o.Via)  // Where?
			If ($UpdateOrigin_i>0)  //  If it is present in list of targets then remove it
				DELETE FROM ARRAY:C228($Targets_at; $UpdateOrigin_i)
			End if 
		End if 
	End if 
	
	//  Now we need to change the Syn Via within the instruction packet to the local site
	//  so at the next sync site it will be processed correctly
	$Packet_o.Via:=Storage:C1525.Local.SiteIdentifier
	
	//  Set the Date created timestamp (if it hasn't been done previously)
	//    Copy it into the local var which will be added to record later
	If ($Packet_o.Created#Null:C1517)
		// Set this to the time the packet was originally created
		//  (which might be several computers ago).
		$DTS_Created_t:=$Packet_o.Created
	Else 
		$DTS_Created_t:=Timestamp:C1445  // If not add the date created in
		$Packet_o.Created:=$DTS_Created_t
	End if 
	
	// Now set the received time stamp if this is a relayed sync instruction
	If ($CallingMethod_t="syn_C_RequestUpdate") | ($CallingMethod_t="syn_S_ReceiveUpdate")
		$DTS_Received_t:=Timestamp:C1445  // NB. this is not stored in the object
	End if 
	
	$NumberOfTargets_i:=Size of array:C274($Targets_at)
	
	For ($CurrentTarget_i; 1; $NumberOfTargets_i)
		
		$Target_t:=$Targets_at{$CurrentTarget_i}
		
		$Packet_o.Target:=$Target_t  //  Change this to the new target
		$Packet_t:=JSON Stringify:C1217($Packet_o; *)  // Let's make it easier to read
		
		
		CREATE RECORD:C68(sync_Table_ptr->)
		sync_ForIdentifier_ptr->:=$Target_t
		sync_PK_ptr->:=$PK_t
		sync_Sync_ID_ptr->:=$SyncID_t
		sync_SmallFieldData_ptr->:=$Packet_t  // TEA Decrypt ($4;<>Syn_EncryptionKey_i;26;7;1962)
		sync_Update_Type_ptr->:=$Action_t
		sync_LargeFieldData_ptr->:=$LargeFieldData_x
		sync_NumberOfRetries_ptr->:=0
		sync_DTS_Created_ptr->:=$DTS_Created_t
		sync_DTS_Received_ptr->:=$DTS_Received_t
		//  <>sync_JournalID_ptr  We don't set this as it is an auto UUID field
		
		SAVE RECORD:C53(sync_Table_ptr->)
		
	End for 
	
	Syn_UnloadRecordReduceSelection(sync_Table_ptr)
	
	
End if 