//%attributes = {"invisible":true}
C_LONGINT:C283($EncryptionKey_i)
C_POINTER:C301($Clients_ptr)
C_COLLECTION:C1488($Clients_c)
C_TEXT:C284($bracket_t)
$Clients_c:=New shared collection:C1527

$Clients_ptr:=OBJECT Get pointer:C1124(Object named:K67:5;"sync_Clients")
ARRAY TO COLLECTION:C1563($Clients_c;$Clients_ptr->)

If ($Clients_c=Null:C1517)
	$Clients_c:=New shared collection:C1527
End if 

Use (Storage:C1525.Local)
	Storage:C1525.Local.SiteIdentifier:=Form:C1466.SiteIdentifier
	Storage:C1525.Local.ServerIdentifier:=Form:C1466.ServerIdentifier
	Storage:C1525.Local.ServerAddress:=Form:C1466.ServerAddress
	Storage:C1525.Local.Port:=Form:C1466.CommsPort
	Storage:C1525.Local.Password:=Form:C1466.Password
	Storage:C1525.Local.Clients:=$Clients_c
	Storage:C1525.Local.logConnectionErrors:=Form:C1466.logConnectionErrors
	Storage:C1525.Local.logErrors:=Form:C1466.logErrors
	Storage:C1525.Local.logEverything:=Form:C1466.logEverything
	Storage:C1525.Local.logTransmissions:=Form:C1466.logTransmissions
	
End use 

$EncryptionKey_i:=Syn_ConvertPasswordIntoKey(Storage:C1525.Local.Password)
If ($EncryptionKey_i=0)
	$EncryptionKey_i:=1701
End if 

Use (Storage:C1525.Syn)
	Storage:C1525.Syn.encryptionKey:=$EncryptionKey_i
End use 



If (Application type:C494=4D Remote mode:K5:5)  //  Send it to server
	syn_SavePrefsOnServer(Storage:C1525.Local)
	
Else 
	Syn_ObjectSaveToFile(Storage:C1525.Local;Storage:C1525.Defaults.PrefsPath;True:C214)
End if 

$bracket_t:=Storage:C1525.Local.SiteIdentifier
$bracket_t:=String:C10(Num:C11($bracket_t))+"/32"


If (Syn#Null:C1517)  // Update this in case it has changed
	
	Syn.bracket:=$bracket_t
	
Else 
	Compiler_Sync
	
	PROCESS PROPERTIES:C336(Current process:C322;$procName_t;$procState_i;$procTime_i)
	
	Syn:=New object:C1471(\
		"processNumber";Current process:C322;\
		"processName";$procName_t;\
		"createSyncRecords";($procName_t#"Syn_ApplyUpdates");\
		"bracket";$bracket_t)
	
	syn_CacheStructure  // This now contains process variables
	
	//  Enable logging in every process it is controlled by Storage
	LOG ENABLE(True:C214)
	
	
End if 