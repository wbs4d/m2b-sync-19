//%attributes = {"invisible":true,"shared":true}
C_BLOB:C604($LargeFieldBlob_x; $oldBLOB_x)
C_BOOLEAN:C305($DeleteMode_b; $DoNotCreateSyncInstructions_b; $InsertMode_b; $UpdateMode_b)
C_DATE:C307($CurrentDate_d)
C_LONGINT:C283($CurrentField_i; $CurrentSyncTarget_i; $DatabaseEvent_i; $FieldType_i; $InstructionPacket_i; $NumberOfFields_i; $NumberOfLargeFields_i; $NumberOfSmallFields_i; $Offset_i; $OK_i)
C_LONGINT:C283($recordNumber_i; $SmallTextSizeLimit_i; $tableNumber_i)
C_TIME:C306($CurrentTime_h)
C_PICTURE:C286($mask_pic; $oldImage_pic)
C_POINTER:C301($CurrentField_ptr; $SYNC_ID_ptr)
C_TEXT:C284($Action_t; $DTS_t; $ErrMethod_t; $KeyPrefix_t; $TimeAsString_t; $PK_t; $SyncID_t)
C_OBJECT:C1216($oldObject_o; $Packet_o; $field_o)
C_COLLECTION:C1488($SF_c; $LF_c)

ARRAY LONGINT:C221($LargeFieldNumbers_ai; 0)
ARRAY LONGINT:C221($LargeFieldTypes_ai; 0)
ARRAY LONGINT:C221($SmallFieldNumbers_ai; 0)
ARRAY LONGINT:C221($SmallFieldTypes_ai; 0)
ARRAY POINTER:C280($LargeFieldsToUpdate_aptr; 0)
ARRAY POINTER:C280($SmallFieldsToUpdate_aptr; 0)
ARRAY TEXT:C222($Targets_at; 0)


Syn_TRACE

// ----------------------------------------------------
// User name (OS): Wayne Stewart
// Date and time: Jan 4, 2013
If (False:C215)
	Syn_Trigger
End if 
// ----------------------------------------------------
// Method: sync_Trigger
// Description
//
//
// Parameters
//
// ----------------------------------------------------

Syn_Init

C_LONGINT:C283(OK)
$OK_i:=OK

$SmallTextSizeLimit_i:=2048

$DoNotCreateSyncInstructions_b:=False:C215

$ErrMethod_t:=Method called on error:C704
ON ERR CALL:C155("Syn_ErrorTrigger")

TRIGGER PROPERTIES:C399(Trigger level:C398; $DatabaseEvent_i; $tableNumber_i; $recordNumber_i)

$InsertMode_b:=($DatabaseEvent_i=On Saving New Record Event:K3:1)
$UpdateMode_b:=($DatabaseEvent_i=On Saving Existing Record Event:K3:2)
$DeleteMode_b:=($DatabaseEvent_i=On Deleting Record Event:K3:3)



If (Syn.createSyncRecords)  //  Only run this code if we are not in the "Apply Updates process"
	
	If (Syn_ThisTable($tableNumber_i; ->$SYNC_ID_ptr))  //;->$SYNC_DATA_ptr))
		
		$PK_t:=TS_GetTimeStamp(Syn.bracket)
		
		//  Retrieve the lists of sites to inform, this might be server and clients
		Syn_SendUpdatesTo(->$Targets_at; Current method name:C684)
		
		$NumberOfFields_i:=Get last field number:C255($tableNumber_i)
		
		//  -----------  CONFIRM ID FIELD HAS A VALUE
		//  -----------    Set ID FIELD if it doesn't have a value
		
		If (Length:C16($SYNC_ID_ptr->)=0)  //  We always do this even if there is no-one to sync to
			//  Now UUID (auto generated)
			//  This code should never be called!
			//  BUT just in case
			$SYNC_ID_ptr->:=Generate UUID:C1066
			
			If (Storage:C1525.Local.logErrors=1) | (Storage:C1525.Local.logEverything=1)
				C_BOOLEAN:C305($logging_b)
				$logging_b:=Log Enable
				Log Enable(True:C214)
				LOG ADD ENTRY($PK_t; Current method name:C684; "Empty JournalID (with auto-UUID)"; $SYNC_ID_ptr->)
				Log Enable($logging_b)
			End if 
		End if 
		
		$SyncID_t:=$SYNC_ID_ptr->
		
		If (Size of array:C274($Targets_at)>0)  //  At least one Sync To
			
			//  -----------  DETERMINE CHANGED FIELDS
			Case of   //  Trigger Modes
				: ($InsertMode_b)  //  No tests for equality
					$Action_t:=Syn Insert Record
					
					For ($CurrentField_i; 1; $NumberOfFields_i)
						If (Is field number valid:C1000($tableNumber_i; $CurrentField_i))
							$CurrentField_ptr:=Field:C253($tableNumber_i; $CurrentField_i)
							If ($CurrentField_ptr=$SYNC_ID_ptr)  //  Always skip this field
							Else 
								
								$FieldType_i:=Type:C295($CurrentField_ptr->)
								
								Case of 
									: ($FieldType_i=Is BLOB:K8:12)
										If (BLOB size:C605($CurrentField_ptr->)>0)
											APPEND TO ARRAY:C911($LargeFieldsToUpdate_aptr; $CurrentField_ptr)
											APPEND TO ARRAY:C911($LargeFieldNumbers_ai; $CurrentField_i)
											APPEND TO ARRAY:C911($LargeFieldTypes_ai; $FieldType_i)
										End if 
										
									: ($FieldType_i=Is object:K8:27)
										If (OB Is defined:C1231($CurrentField_ptr->))
											APPEND TO ARRAY:C911($LargeFieldsToUpdate_aptr; $CurrentField_ptr)
											APPEND TO ARRAY:C911($LargeFieldNumbers_ai; $CurrentField_i)
											APPEND TO ARRAY:C911($LargeFieldTypes_ai; $FieldType_i)
										End if 
										
									: ($FieldType_i=Is text:K8:3) | ($FieldType_i=Is alpha field:K8:1)
										
										If (Length:C16($CurrentField_ptr->)=0)
										Else 
											If ((Length:C16($CurrentField_ptr->)<$SmallTextSizeLimit_i))  //  Only if a small amount of text
												// `& (Position("<";$CurrentField_ptr->;*)=0) `& (Position(">";$CurrentField_ptr->;*)=0))  //  Only if a small amount of text & no style tags
												APPEND TO ARRAY:C911($SmallFieldsToUpdate_aptr; $CurrentField_ptr)
												APPEND TO ARRAY:C911($SmallFieldNumbers_ai; $CurrentField_i)
												APPEND TO ARRAY:C911($SmallFieldTypes_ai; $FieldType_i)
												
											Else 
												APPEND TO ARRAY:C911($LargeFieldsToUpdate_aptr; $CurrentField_ptr)
												APPEND TO ARRAY:C911($LargeFieldNumbers_ai; $CurrentField_i)
												APPEND TO ARRAY:C911($LargeFieldTypes_ai; $FieldType_i)
												
											End if 
										End if 
										
									: ($FieldType_i=Is picture:K8:10)
										If (Picture size:C356($CurrentField_ptr->)>0)
											APPEND TO ARRAY:C911($LargeFieldsToUpdate_aptr; $CurrentField_ptr)
											APPEND TO ARRAY:C911($LargeFieldNumbers_ai; $CurrentField_i)
											APPEND TO ARRAY:C911($LargeFieldTypes_ai; $FieldType_i)
										End if 
										
									: ($FieldType_i=Is boolean:K8:9)
										If ($CurrentField_ptr->)  // Default value is false, so only send if True
											APPEND TO ARRAY:C911($SmallFieldsToUpdate_aptr; $CurrentField_ptr)
											APPEND TO ARRAY:C911($SmallFieldNumbers_ai; $CurrentField_i)
											APPEND TO ARRAY:C911($SmallFieldTypes_ai; $FieldType_i)
										End if 
										
									: ($FieldType_i=Is date:K8:7)
										If ($CurrentField_ptr->=!00-00-00!)
										Else 
											APPEND TO ARRAY:C911($SmallFieldsToUpdate_aptr; $CurrentField_ptr)
											APPEND TO ARRAY:C911($SmallFieldNumbers_ai; $CurrentField_i)
											APPEND TO ARRAY:C911($SmallFieldTypes_ai; $FieldType_i)
											
										End if 
									: ($FieldType_i=Is integer:K8:5) | ($FieldType_i=Is longint:K8:6) | ($FieldType_i=Is real:K8:4)
										If ($CurrentField_ptr->=0)
										Else 
											APPEND TO ARRAY:C911($SmallFieldsToUpdate_aptr; $CurrentField_ptr)
											APPEND TO ARRAY:C911($SmallFieldNumbers_ai; $CurrentField_i)
											APPEND TO ARRAY:C911($SmallFieldTypes_ai; $FieldType_i)
											
										End if 
										
									: ($FieldType_i=Is time:K8:8)
										
										If ($CurrentField_ptr->=?00:00:00?)
										Else 
											APPEND TO ARRAY:C911($SmallFieldsToUpdate_aptr; $CurrentField_ptr)
											APPEND TO ARRAY:C911($SmallFieldNumbers_ai; $CurrentField_i)
											APPEND TO ARRAY:C911($SmallFieldTypes_ai; $FieldType_i)
											
										End if 
										
								End case 
								
							End if 
						End if 
					End for 
					
				: ($UpdateMode_b)
					$Action_t:=Syn Update Record
					For ($CurrentField_i; 1; $NumberOfFields_i)
						If (Is field number valid:C1000($tableNumber_i; $CurrentField_i))
							$CurrentField_ptr:=Field:C253($tableNumber_i; $CurrentField_i)
							If ($CurrentField_ptr=$SYNC_ID_ptr)  // | ($CurrentField_ptr=$SYNC_DATA_ptr)  //  Always skip these two fields
							Else 
								$FieldType_i:=Type:C295($CurrentField_ptr->)
								Case of   //  Checking field types
									: ($FieldType_i=Is BLOB:K8:12)
										$oldBLOB_x:=Old:C35($CurrentField_ptr->)
										
										Case of 
											: (BLOB size:C605($oldBLOB_x)=0) & (BLOB size:C605($CurrentField_ptr->)=0)
											: (Generate digest:C1147($oldBLOB_x; MD5 digest:K66:1)=Generate digest:C1147($CurrentField_ptr->; MD5 digest:K66:1))
											Else 
												APPEND TO ARRAY:C911($LargeFieldsToUpdate_aptr; $CurrentField_ptr)
												APPEND TO ARRAY:C911($LargeFieldNumbers_ai; $CurrentField_i)
												APPEND TO ARRAY:C911($LargeFieldTypes_ai; $FieldType_i)
												
										End case 
										
										
									: ($FieldType_i=Is object:K8:27)
										$oldObject_o:=Old:C35($CurrentField_ptr->)
										
										If (Syn_OBJ_IsEqual($oldObject_o; $CurrentField_ptr->))
										Else 
											APPEND TO ARRAY:C911($LargeFieldsToUpdate_aptr; $CurrentField_ptr)
											APPEND TO ARRAY:C911($LargeFieldNumbers_ai; $CurrentField_i)
											APPEND TO ARRAY:C911($LargeFieldTypes_ai; $FieldType_i)
										End if 
										
									: ($FieldType_i=Is text:K8:3) | ($FieldType_i=Is alpha field:K8:1)
										
										If (syn_EqualStrings($CurrentField_ptr->; Old:C35($CurrentField_ptr->)))
										Else 
											If (Length:C16($CurrentField_ptr->)<$SmallTextSizeLimit_i)
												APPEND TO ARRAY:C911($SmallFieldsToUpdate_aptr; $CurrentField_ptr)
												APPEND TO ARRAY:C911($SmallFieldNumbers_ai; $CurrentField_i)
												APPEND TO ARRAY:C911($SmallFieldTypes_ai; $FieldType_i)
												
											Else 
												APPEND TO ARRAY:C911($LargeFieldsToUpdate_aptr; $CurrentField_ptr)
												APPEND TO ARRAY:C911($LargeFieldNumbers_ai; $CurrentField_i)
												APPEND TO ARRAY:C911($LargeFieldTypes_ai; $FieldType_i)
												
											End if 
										End if 
										
										
									: ($FieldType_i=Is picture:K8:10)
										$oldImage_pic:=Old:C35($CurrentField_ptr->)
										
										Case of 
											: (Picture size:C356($oldImage_pic)=0) & (Picture size:C356($CurrentField_ptr->)=0)
											: (Equal pictures:C1196($oldImage_pic; $CurrentField_ptr->; $mask_pic))
											Else 
												APPEND TO ARRAY:C911($LargeFieldsToUpdate_aptr; $CurrentField_ptr)
												APPEND TO ARRAY:C911($LargeFieldNumbers_ai; $CurrentField_i)
												APPEND TO ARRAY:C911($LargeFieldTypes_ai; $FieldType_i)
												
										End case 
										
									Else 
										If ($CurrentField_ptr->=Old:C35($CurrentField_ptr->))
										Else 
											APPEND TO ARRAY:C911($SmallFieldsToUpdate_aptr; $CurrentField_ptr)
											APPEND TO ARRAY:C911($SmallFieldNumbers_ai; $CurrentField_i)
											APPEND TO ARRAY:C911($SmallFieldTypes_ai; $FieldType_i)
											
										End if 
										
								End case   //  Checking field types
								
							End if   //  Always skip these two fields
						End if 
					End for 
					
					$DoNotCreateSyncInstructions_b:=(Size of array:C274($SmallFieldsToUpdate_aptr)=0) & (Size of array:C274($LargeFieldsToUpdate_aptr)=0)
					
				Else   //  $DeleteMode_b
					$Action_t:=Syn Delete Record
					
			End case   //  Trigger modes
			
			If ($DoNotCreateSyncInstructions_b)
			Else   //  -----------  CREATE SYNC INSTRUCTIONS
				
				$Packet_o:=New object:C1471(\
					"Target"; ""; \
					"Origin"; Storage:C1525.Local.SiteIdentifier; \
					"Act"; $Action_t; \
					"TblNo"; $TableNumber_i; \
					"RecID"; $SyncID_t; \
					"Created"; Timestamp:C1445)
				
				If ($DeleteMode_b)  //  If we are in delete mode there is no more to add to instruction packet
					
				Else 
					
					$NumberOfSmallFields_i:=Size of array:C274($SmallFieldsToUpdate_aptr)
					$NumberOfLargeFields_i:=Size of array:C274($LargeFieldsToUpdate_aptr)
					
					// No longer need these as we can use the collection.length
					//$Packet_o.NoSmlFld:=$NumberOfSmallFields_i
					//$Packet_o.NoLrgFld:=$NumberOfLargeFields_i
					
					
					If ($NumberOfSmallFields_i>0)
						
						$SF_c:=New collection:C1472
						
						For ($CurrentField_i; 1; $NumberOfSmallFields_i)  //  Process small fields
							$field_o:=New object:C1471("No"; $SmallFieldNumbers_ai{$CurrentField_i})  // Create an object for this SMALL field
							
							$FieldType_i:=$SmallFieldTypes_ai{$CurrentField_i}
							
							Case of   //  Add small field value to the object
								: ($FieldType_i=Is alpha field:K8:1) | ($FieldType_i=Is text:K8:3)
									If (Is field value Null:C964($SmallFieldsToUpdate_aptr{$CurrentField_i}->))
										Syn_TRACE
										//$Packet_o.NoSmlFld:=$Packet_o.NoSmlFld-1  //  Lower the count
									End if 
									
									If (Length:C16($SmallFieldsToUpdate_aptr{$CurrentField_i}->)=0)  //  Not null, just empty
										$field_o.Val:=Syn Empty Value  // This is recognisable text that we can filter at other end
									Else 
										$field_o.Val:=$SmallFieldsToUpdate_aptr{$CurrentField_i}->
									End if 
									
								: ($FieldType_i=Is integer:K8:5)\
									 | ($FieldType_i=Is longint:K8:6)\
									 | ($FieldType_i=Is real:K8:4)
									$field_o.Val:=$SmallFieldsToUpdate_aptr{$CurrentField_i}->
									
								: ($FieldType_i=Is boolean:K8:9)  // 1 or 0 is less data to transmit than True or False
									$field_o.Val:=Num:C11($SmallFieldsToUpdate_aptr{$CurrentField_i}->)
									
								: ($FieldType_i=Is date:K8:7)
									$field_o.Val:=$SmallFieldsToUpdate_aptr{$CurrentField_i}->  //Syn_DateToText($SmallFieldsToUpdate_aptr{$CurrentField_i}->)  // Convert to a string
									
								: ($FieldType_i=Is time:K8:8)
									$field_o.Val:=$SmallFieldsToUpdate_aptr{$CurrentField_i}->  //Syn_TimeToText($SmallFieldsToUpdate_aptr{$CurrentField_i}->)
									
							End case 
							
							If ($field_o.Val#Null:C1517)
								$SF_c.push($field_o)
							End if 
							
						End for 
						
						If ($SF_c.length>0)  // Only add it if we need to
							$Packet_o.SF:=$SF_c
						End if 
						
					End if 
					
					
					If ($NumberOfLargeFields_i>0)
						
						$LF_c:=New collection:C1472  // Start a collection
						$Offset_i:=0
						
						For ($CurrentField_i; 1; $NumberOfLargeFields_i)
							$field_o:=New object:C1471("No"; $LargeFieldNumbers_ai{$CurrentField_i})  // Create an object for this LARGE field
							VARIABLE TO BLOB:C532($LargeFieldsToUpdate_aptr{$CurrentField_i}->; $LargeFieldBlob_x; $Offset_i)
							$LF_c.push($field_o)
						End for 
						
						If ($LF_c.length>0)  // Only add it if we need to
							$Packet_o.LF:=$LF_c
							$Packet_o.BlobSize:=BLOB size:C605($LargeFieldBlob_x)
						End if 
						
					End if 
					
				End if   // ($DeleteMode_b)
				
			End if 
		End if 
		
		If ($DoNotCreateSyncInstructions_b)
		Else 
			
			Syn_NewSyncRecords($PK_t; $SyncID_t; $Packet_o; $LargeFieldBlob_x; $Action_t; Current method name:C684)
			
			
		End if 
		
		//  Now release the instruction packet
		$Packet_o:=New object:C1471
		
	End if   //  At least one Sync To
End if 

ON ERR CALL:C155($ErrMethod_t)

OK:=$OK_i