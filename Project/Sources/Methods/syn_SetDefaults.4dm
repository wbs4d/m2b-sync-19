//%attributes = {"invisible":true}
C_TEXT:C284($syncFolder_t)

// ----------------------------------------------------
// User name (OS): Wayne Stewart
// Date and time: Jan 5, 2013
If (False:C215)
	syn_SetDefaults
End if 
// ----------------------------------------------------
// Method: sync_SetDefaults
// Description
//
//
// Parameters
//
// ----------------------------------------------------
C_LONGINT:C283(<>Sync_DefaultDictionary_i)

Syn_TRACE

If (Dict_IsValid(<>Sync_DefaultDictionary_i))  //  Get rid of it first
	Dict_Release(<>Sync_DefaultDictionary_i)
End if 

<>Sync_DefaultDictionary_i:=Dict_New("SyncDefaults")

KVP_Longint("SyncDefaults.Port";1701)
KVP_Text("SyncDefaults.Password";"fred1234")
KVP_Longint("SyncDefaults.TimeOut";10)
KVP_Longint("SyncDefaults.Small Blob Size Offset";20)
KVP_Longint("SyncDefaults.Large Blob Size Offset";24)
KVP_Longint("SyncDefaults.Data Offset";28)
KVP_Longint("SyncDefaults.Blob Padding";80)
KVP_Text("SyncDefaults.SiteIdentifier";"x33")  //  This is larger than 32!


If (Application type:C494#4D Remote mode:K5:5)  //  Don't do any of this stuff on 4D Client
	$syncFolder_t:=Syn Path of Containing Folder(Data file:C490)+"Sync"+Folder separator:K24:12
	
	KVP_Text("SyncDefaults.Sync Folder";$syncFolder_t)
	KVP_Text("SyncDefaults.PrefsPath";$syncFolder_t+"Config.xml")
	
	If (Test path name:C476($syncFolder_t)=Is a folder:K24:2)
	Else 
		CREATE FOLDER:C475($syncFolder_t;*)
	End if 
	
End if 


