//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// User name (OS): Wayne Stewart
// Date and time: Aug 12, 2013
If (False:C215)
	Syn_Startup
End if 

// ----------------------------------------------------
// Method: Syn_Startup
// Description
//   This method is called to start the component
//     on 4D Remote there is not much to set up!
// Parameters
//
// ----------------------------------------------------

Syn_TRACE

Syn_Init


syn_Quit(0)

Syn_UnloadRecordReduceSelection(sync_Table_ptr)





If (Application type:C494=4D Remote mode:K5:5)
	UNREGISTER CLIENT:C649
	REGISTER CLIENT:C648(Current machine:C483+String:C10(Random:C100); *)
Else 
	
	// No need to call here
	//Syn_CheckForOnHold(Storage.Local.SiteIdentifier)
	
	Syn_PullFromServer
	Syn_PushToServer
	Syn_ApplyUpdates
End if 

If (Is compiled mode:C492)
	If (Caps lock down:C547)
		syn_DisplayPreferences
		syn_Monitor
	End if 
Else 
	syn_DisplayPreferences
	syn_Monitor
End if 

