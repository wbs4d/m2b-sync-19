//%attributes = {"invisible":true,"shared":true,"publishedSoap":true,"publishedWsdl":true}
// This method is called by a sync client and asks for the oldest sync record to be sent to it.
//  This is the client requesting an update from the server

C_TEXT:C284($1;$2)

If (False:C215)
	C_TEXT:C284(syn_S_SendUpdate;$1;$2)
	
	//  Called by:
	syn_C_RequestUpdate
End if 

Syn_Init

//C_LONGINT(syn_RecordsInQueue_i)
//C_TEXT(syn_SmallFieldData_t;syn_PK_t;syn_ID_t;syn_JournalID_t)
//C_BLOB(syn_LargeData_x)

C_BLOB:C604($Blank_x)
Syn_TRACE
syn_RecordsInQueue_i:=0
syn_LargeData_x:=$Blank_x
syn_SmallFieldData_t:=""
syn_PK_t:=""
syn_ID_t:=""
syn_JournalID_t:=""

SOAP DECLARATION:C782($1;Is text:K8:3;SOAP input:K46:1;"target")
SOAP DECLARATION:C782($2;Is text:K8:3;SOAP input:K46:1;"origin")

SOAP DECLARATION:C782(syn_PK_t;Is text:K8:3;SOAP output:K46:2;"PK")
SOAP DECLARATION:C782(syn_ID_t;Is text:K8:3;SOAP output:K46:2;"Sync_ID")
SOAP DECLARATION:C782(syn_JournalID_t;Is text:K8:3;SOAP output:K46:2;"Journal_ID")

SOAP DECLARATION:C782(syn_SmallFieldData_t;Is text:K8:3;SOAP output:K46:2;"SmallFieldData")
SOAP DECLARATION:C782(syn_LargeData_x;Is BLOB:K8:12;SOAP output:K46:2;"LargeData")

SOAP DECLARATION:C782(syn_RecordsInQueue_i;Is longint:K8:6;SOAP output:K46:2;"RecordsInQueue")


READ ONLY:C145(sync_Table_ptr->)
SET QUERY DESTINATION:C396(Into current selection:K19:1)
QUERY:C277(sync_Table_ptr->;sync_ForIdentifier_ptr->=$1)

syn_RecordsInQueue_i:=Records in selection:C76(sync_Table_ptr->)

If (syn_RecordsInQueue_i>0)
	ORDER BY:C49(sync_Table_ptr->;sync_PK_ptr->;>)  //  Get the oldest first, PK is unique (for each "For") & ordered w/n sync table
	FIRST RECORD:C50(sync_Table_ptr->)  //  I think this is unnecessary, it should already be loaded
	
	syn_SmallFieldData_t:=sync_SmallFieldData_ptr->
	syn_LargeData_x:=sync_LargeFieldData_ptr->
	syn_PK_t:=sync_PK_ptr->
	syn_ID_t:=sync_Sync_ID_ptr->
	syn_JournalID_t:=sync_JournalID_ptr->
	
	Syn_TRACE
	Syn_UnloadRecordReduceSelection(sync_Table_ptr)
	
	If (Storage:C1525.Local.logTransmissions=1)
		C_BOOLEAN:C305($logging_b)
		$logging_b:=LOG ENABLE
		LOG ENABLE(True:C214)
		LOG USE LOG("TransmissionLog")
		LOG ADD ENTRY("ReqSend";\
			"T: "+$1+\
			" O: "+$2+\
			" L: "+Storage:C1525.Local.SiteIdentifier;syn_PK_t;syn_ID_t;syn_JournalID_t;String:C10(syn_RecordsInQueue_i))
		LOG USE LOG("ErrorLog")
		LOG ENABLE($logging_b)
	End if 
	
End if 

