//%attributes = {"invisible":true}
C_TEXT:C284($0)
C_TEXT:C284($1)

C_LONGINT:C283($platform_i)
C_TEXT:C284($path_t)

If (False:C215)
	C_TEXT:C284(SYN_LEP_EscapePath;$0)
	C_TEXT:C284(SYN_LEP_EscapePath;$1)
End if 

If (Count parameters:C259#0)
	
	$path_t:=$1
	
	_O_PLATFORM PROPERTIES:C365($platform_i)
	
	Case of 
		: ($platform_i=Windows:K25:3)
			
			$path_t:=Syn_LEP_Escape($path_t)
			
		: ($platform_i=Mac OS:K25:2)
			
			$path_t:=Syn_LEP_Escape(Convert path system to POSIX:C1106($path_t))
			
	End case 
	
End if 

$0:=$path_t