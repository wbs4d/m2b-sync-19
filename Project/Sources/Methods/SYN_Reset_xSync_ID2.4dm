//%attributes = {"invisible":true,"executedOnServer":true}
// SYN_Reset_xSync_ID2
// Created by Wayne Stewart (Aug 9, 2014)
//  Method is an autostart type
//     waynestewart@mac.com
C_LONGINT:C283($1)
C_LONGINT:C283($2)
C_LONGINT:C283($3)

C_LONGINT:C283($RecordCount_i)
C_POINTER:C301($Field_ptr; $table_ptr)
C_TEXT:C284($sync_Bracket_t; $sync_SiteIdentifier_t)

If (False:C215)
	C_LONGINT:C283(SYN_Reset_xSync_ID2; $1)
	C_LONGINT:C283(SYN_Reset_xSync_ID2; $2)
	C_LONGINT:C283(SYN_Reset_xSync_ID2; $3)
End if 
C_BOOLEAN:C305($logging_b)
$logging_b:=Log Enable
Log Enable(True:C214)
$table_ptr:=Table:C252($1)
$Field_ptr:=Field:C253($1; $2)

READ WRITE:C146($table_ptr->)

//SET QUERY DESTINATION(Into variable;$RecordCount_i)
QUERY:C277($table_ptr->; $Field_ptr->="")
$RecordCount_i:=Records in selection:C76($table_ptr->)

If ($RecordCount_i>0)
	$sync_SiteIdentifier_t:=Syn_LocalSiteIdentifier
	$sync_Bracket_t:=String:C10(Num:C11($sync_SiteIdentifier_t))+"/32"
	LOG ADD ENTRY(Table name:C256($1); Field name:C257($1; $2); String:C10($RecordCount_i))
	//Syn_Log_AddEntry
	APPLY TO SELECTION:C70($table_ptr->; $Field_ptr->:=$sync_SiteIdentifier_t+"_"+TS_GetTimeStamp($sync_Bracket_t))
	
End if 

READ ONLY:C145($table_ptr->)
UNLOAD RECORD:C212($table_ptr->)
REDUCE SELECTION:C351($Table_ptr->; 0)

Log Enable($logging_b)