//%attributes = {"invisible":true}

If (False:C215)
	C_TEXT:C284($1)
	
	C_LONGINT:C283($ExistingLocation_i)
	C_POINTER:C301($RegClientsList_ptr; $ServerIdentifier_ptr; $SiteIdentifier_ptr)
	C_TEXT:C284($Identifier_t; $ReceivingObject_t)
	
	
	If (False:C215)
		C_TEXT:C284(Syn_Pref_RemoveIdentifier; $1)
	End if 
	
End if 

$Identifier_t:=$1

$ReceivingObject_t:=OBJECT Get name:C1087(Object current:K67:2)

$SiteIdentifier_ptr:=OBJECT Get pointer:C1124(Object named:K67:5; "Site Identifier")
$ServerIdentifier_ptr:=OBJECT Get pointer:C1124(Object named:K67:5; "Server Identifier")
$RegClientsList_ptr:=OBJECT Get pointer:C1124(Object named:K67:5; "Sync_IdentifierArray")


If ($ReceivingObject_t#"Site Identifier")  //  If we are setting either a registered client or the server
	If ($SiteIdentifier_ptr->=$Identifier_t)  //  And if we are setting it to a value that is currently used
		$SiteIdentifier_ptr->:=""  //                delete that value
	End if 
End if 

If ($ReceivingObject_t#"Server Identifier")
	If ($ServerIdentifier_ptr->=$Identifier_t)  //  And if we are setting it to a value that is currently used
		$ServerIdentifier_ptr->:=""  //                delete that value
	End if 
End if 

If ($ReceivingObject_t#"sync_ListboxObject_ab")  //  If you drag from list of clients back on to itself!
	If ($ReceivingObject_t#"Sync_IdentifierArray")
		$ExistingLocation_i:=Find in array:C230($RegClientsList_ptr->; $Identifier_t)
		If ($ExistingLocation_i>0)
			DELETE FROM ARRAY:C228($RegClientsList_ptr->; $ExistingLocation_i)
		End if 
	End if 
End if 