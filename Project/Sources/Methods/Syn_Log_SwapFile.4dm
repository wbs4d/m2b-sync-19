//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Syn_Log_SwapFile

// Allows the developer to swap from the error log 
// to the transmission log

// Access: Shared

// Parameters: 
//   $1 : TEXT : Pass one of the two constant values

// Created by Wayne Stewart (2016-05-11T14:00:00Z)
//     waynestewart@mac.com
// ----------------------------------------------------

C_TEXT:C284($1)

Case of 
	: (Count parameters:C259=0)
		<>Syn_Log_Path_t:=<>Syn_Log_ErrorPath_t
		
	: ($1=Syn Log Main)
		<>Syn_Log_Path_t:=<>Syn_Log_ErrorPath_t
		
	: ($1=Syn Transmission Log)
		<>Syn_Log_Path_t:=<>Syn_Log_TransmissionPath_t
		
End case 