//%attributes = {"invisible":true,"shared":true,"publishedSoap":true,"publishedWsdl":true}
C_TEXT:C284($1)
C_TEXT:C284($2)

C_TEXT:C284($Origin_t;$Target_t)

If (False:C215)
	C_TEXT:C284(Syn_S_CheckQueueLength;$1)
	C_TEXT:C284(Syn_S_CheckQueueLength;$2)
End if 

Syn_Init

syn_RecordsInQueue_i:=0

If (Caps lock down:C547)
	TRACE:C157
End if 

If (False:C215)
	//called by
	Syn_C_CheckQueueLength
	
End if 

SOAP DECLARATION:C782($1;Is text:K8:3;SOAP input:K46:1;"target")
SOAP DECLARATION:C782($2;Is text:K8:3;SOAP input:K46:1;"origin")

$Origin_t:=$1
$Target_t:=$2

SOAP DECLARATION:C782(syn_RecordsInQueue_i;Is longint:K8:6;SOAP output:K46:2;"RecordsInQueue")

READ ONLY:C145(sync_Table_ptr->)
SET QUERY DESTINATION:C396(Into variable:K19:4;syn_RecordsInQueue_i)
QUERY:C277(sync_Table_ptr->;sync_ForIdentifier_ptr->=$Target_t)
SET QUERY DESTINATION:C396(Into current selection:K19:1)

If (syn_EqualStrings($Target_t;Storage:C1525.Local.SiteIdentifier))
Else 
	$Target_t:=Lowercase:C14($Target_t)
End if 

If (Storage:C1525.Local.logTransmissions=1)
	C_BOOLEAN:C305($logging_b)
	$logging_b:=LOG ENABLE
	LOG ENABLE(True:C214)
	LOG USE LOG("TransmissionLog")
	LOG ADD ENTRY(\
		"LengthQ";\
		"T: "+$1+\
		" O: "+$2+\
		" L: "+Storage:C1525.Local.SiteIdentifier;\
		"Records: "+String:C10(syn_RecordsInQueue_i))
	LOG USE LOG("ErrorLog")
	LOG ENABLE($logging_b)
End if 