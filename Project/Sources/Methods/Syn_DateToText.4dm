//%attributes = {}
// ----------------------------------------------------
// Project Method: Syn_DateToText --> ReturnType

// Description

// Access: Shared

// Parameters: 
//   $1 : Type : Description
//   $x : Type : Description (optional)

// Returns: 
//   $0 : Type : Description

// Created by Wayne Stewart (2021-04-25T14:30:00Z)
//     wayne@4dsupport.guru
// ----------------------------------------------------
If (False:C215)
	Syn_DateToText
	
End if 

C_DATE:C307($1)
C_TEXT:C284($0)

$0:=Substring:C12(String:C10($1; ISO date:K1:8); 1; 10)