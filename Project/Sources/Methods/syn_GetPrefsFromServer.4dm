//%attributes = {"invisible":true,"shared":true,"executedOnServer":true}
// ----------------------------------------------------
// Project Method: syn_GetPrefsFromServer

// Method Type:    Private

// Parameters:
C_OBJECT:C1216($0)

If (False:C215)
	C_OBJECT:C1216(syn_GetPrefsFromServer; $0)
End if 

// Local Variables:

// Created by Wayne Stewart (Sep 30, 2013)
//     waynestewart@mac.com

//   
// ----------------------------------------------------

$0:=Storage:C1525.Local  // Return the Server Prefs
