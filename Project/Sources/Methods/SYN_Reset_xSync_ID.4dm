//%attributes = {"invisible":true,"executedOnServer":true}
// SYN_Reset_xSync_ID
// Created by Wayne Stewart (Aug 9, 2014)
//  Method is an autostart type
//     waynestewart@mac.com
C_LONGINT:C283($1)

C_LONGINT:C283($CurrentTable_i;$NumberOfTables_i;$ProcessID_i)


If (False:C215)
	C_LONGINT:C283(SYN_Reset_xSync_ID;$1)
End if 


If (Count parameters:C259=1)
	
	LOG ENABLE(True:C214)
	$NumberOfTables_i:=Get last table number:C254
	For ($CurrentTable_i;1;Get last table number:C254)
		If (Is table number valid:C999($CurrentTable_i))
			
			$NumberOfFields_i:=Get last field number:C255($CurrentTable_i)
			
			For ($CurrentField_i;1;$NumberOfFields_i)
				If (Is field number valid:C1000($CurrentTable_i;$CurrentField_i))
					If (Field name:C257($CurrentTable_i;$CurrentField_i)="xSync_ID")
						//beep
						SYN_Reset_xSync_ID2($CurrentTable_i;$CurrentField_i)
						$CurrentField_i:=$NumberOfFields_i+1  //  Exit loop
					End if 
				End if 
			End for 
			
		End if   // Valid Table
	End for   //  Next Table
	
	SHOW ON DISK:C922(Log Folder Path;*)
	LOG ENABLE(False:C215)
	
Else 
	// This version allows for any number of processes
	// $ProcessID_i:=New Process(Current method name;0;Current method name;0)
	// This version allows for one unique process
	$ProcessID_i:=New process:C317(Current method name:C684;0;"Generate Sync ID";0;*)
	RESUME PROCESS:C320($ProcessID_i)
	SHOW PROCESS:C325($ProcessID_i)
	BRING TO FRONT:C326($ProcessID_i)
End if 