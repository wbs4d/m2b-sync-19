//%attributes = {"invisible":true}
// Syn_Log_Compress
// Created by Wayne Stewart (Aug 12, 2015)
//  Method is an autostart type
//     me@us.com
// ----------------------------------------------------
C_TEXT:C284($1)
C_LONGINT:C283($2)

C_LONGINT:C283($ProcessID_i;$StackSize_i)
C_TEXT:C284($ArchivePath_t;$Path_t)



If (False:C215)
	C_TEXT:C284(Syn_Log_Compress;$1)
	C_LONGINT:C283(Syn_Log_Compress;$2)
End if 

// ----------------------------------------------------

$StackSize_i:=512*1024

If (Count parameters:C259=2)
	$Path_t:=$1
	$ArchivePath_t:=Replace string:C233($Path_t;".txt";".7z")
	
	If (Syn_z7_Archive($Path_t;$ArchivePath_t;""))
		DELETE DOCUMENT:C159($Path_t)  // Now get rid of it
		
	Else 
		If (Is compiled mode:C492)
		Else 
			ALERT:C41("Error Compressing File")
		End if 
		
	End if 
	
Else 
	// This version allows for any number of processes
	// $ProcessID_i:=New Process(Current method name;$StackSize_i;Current method name;0)
	
	// On the other hand, this version allows for one unique process
	$ProcessID_i:=New process:C317(Current method name:C684;$StackSize_i;Current method name:C684;$1;0;*)
	
	RESUME PROCESS:C320($ProcessID_i)
	SHOW PROCESS:C325($ProcessID_i)
	BRING TO FRONT:C326($ProcessID_i)
End if 