//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Syn_LocalSiteIdentifier

// Global and IP variables accessed:


If (False:C215)
	C_TEXT:C284(Syn_LocalSiteIdentifier; $1; $0)
End if 

// Method Type:    Protected

// Parameters:
C_TEXT:C284($1; $0)

// Created by Wayne Stewart Aug 10, 2013
//     waynestewart@mac.com
// ----------------------------------------------------

Syn_Init

If (Count parameters:C259=1)
	Use (Storage:C1525.Local)
		Storage:C1525.Local.SiteIdentifier:=$1
	End use 
End if 

// Alway return the value
$0:=Storage:C1525.Local.SiteIdentifier

