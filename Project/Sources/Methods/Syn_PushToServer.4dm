//%attributes = {"invisible":true,"executedOnServer":true}
// Created by Wayne Stewart (Dec 1, 2012)
//  Method is an autostart type
//     Waynestewart@mac.com
C_TEXT:C284($1)
C_LONGINT:C283($2)

C_BOOLEAN:C305($QuitNow_b)
C_LONGINT:C283($Counter_i;$CurrentDelay_i;$Max_i;$NumberOfFails_i;$ProcessID_i;$RecordsInQueue_i;$Start_i)
C_TIME:C306($finishTime_h)
C_TEXT:C284($JournalID_t;$PKOfCurrentInstruction_t;$ProcessName_t;$ServerAddress_t;Storage:C1525.Local.ServerIdentifier;$ServerResponse_t)

If (False:C215)
	C_TEXT:C284(Syn_PushToServer;$1)
	C_LONGINT:C283(Syn_PushToServer;$2)
End if 

//  Method starts the Sync Push client
//   The sync push client pushes changes to the server
C_BOOLEAN:C305($logging_b)
$logging_b:=LOG ENABLE
LOG ENABLE(True:C214)
Syn_Init



Case of 
	: (Count parameters:C259=0)
		If (Storage:C1525.Local.ServerIdentifier=Null:C1517)
		Else 
			If (Length:C16(Storage:C1525.Local.ServerIdentifier)>0)
				Syn_PushToServer(Storage:C1525.Local.ServerIdentifier)
			End if 
		End if 
		
	: (Count parameters:C259=2)
		$ProcessID_i:=Current process:C322
		$finishTime_h:=Current time:C178+?00:10:00?  // We will kill process in 10 minutes time
		
		READ ONLY:C145(*)
		READ WRITE:C146(sync_Table_ptr->)  //  Only the [sync] records should be in read write
		
		WEB SERVICE SET OPTION:C901(Web Service HTTP timeout:K48:9;30)  //  30 second time out (default is 180 seconds)
		ON ERR CALL:C155("Syn_ErrorSOAP")
		
		Repeat 
			
			//  How many records are in the queue?
			$RecordsInQueue_i:=syn_RecordsInQueue(Storage:C1525.Local.ServerIdentifier)  // This does not load but only counts them
			
			//  Set current Delay
			$CurrentDelay_i:=0  //  0 ticks
			
			Case of 
				: ($RecordsInQueue_i>0)  //  Are there any records queued up?
					
					//  Call this every time, in case the user has changed ip address
					$ServerAddress_t:=Syn_GetServerAddress
					
					// At most we want to send 10 records
					$Max_i:=1  //Syn_LesserOf ($RecordsInQueue_i;10)
					
					//  Or spend 60 seconds doing it
					$Start_i:=Tickcount:C458
					
					//  This is a general safety check
					$NumberOfFails_i:=0
					
					// If there is a record we should load it now, this is repeated at the bottom of the loop
					$JournalID_t:=syn_LoadFirstRecordInQueue(Storage:C1525.Local.ServerIdentifier)
					
					For ($Counter_i;1;$Max_i)  //  Run through maximum of 10 records / 60 seconds
						
						//  Now send the instruction
						$ServerResponse_t:=syn_C_SendUpdate($ServerAddress_t;\
							Storage:C1525.Local.ServerIdentifier;\
							sync_PK_ptr->;\
							sync_Sync_ID_ptr->;\
							sync_SmallFieldData_ptr->;\
							sync_LargeFieldData_ptr->)  // Send the fields from the current record
						
						$JournalID_t:=sync_JournalID_ptr->  //  This is unique in the table
						
						Case of 
							: ($ServerResponse_t=Syn OK)  //
								Syn_DeleteSpecifiedQueueItem($JournalID_t;Current method name:C684;$ServerResponse_t)  // Delete the record that we just sent
								$NumberOfFails_i:=0
								
							: ($ServerResponse_t=Syn No Response)  // Server is probably down, exit loop
								$Counter_i:=$Max_i+1
								
							: ($ServerResponse_t=Syn Push Duplicate Record)  // This shouldn't be possible
								If (Storage:C1525.Local.logErrors=1) | (Storage:C1525.Local.logEverything=1)
									LOG ADD ENTRY(sync_PK_ptr->;Current method name:C684;Syn Push Duplicate Record;sync_Sync_ID_ptr->)
								End if 
								
								$NumberOfFails_i:=$NumberOfFails_i+1  //  Increase if we get some other error
								Syn_DeleteSpecifiedQueueItem($JournalID_t;Current method name:C684;$ServerResponse_t)  // Delete the record that we just sent by mistake
								
							: ($ServerResponse_t=Syn Err Invalid JSON)  // This shouldn't be possible
								If (Storage:C1525.Local.logErrors=1) | (Storage:C1525.Local.logEverything=1)
									LOG ADD ENTRY(sync_PK_ptr->;Current method name:C684;Syn Err Invalid JSON;sync_Sync_ID_ptr->)
								End if 
								
								$NumberOfFails_i:=$NumberOfFails_i+1  //  Increase if we get some other error
								Syn_DeleteSpecifiedQueueItem($JournalID_t;Current method name:C684;$ServerResponse_t)  // Delete the record that is corrupt
								
							Else 
								$NumberOfFails_i:=$NumberOfFails_i+1  //  Increase if we get some other error
								
						End case 
						
						//  Note this may be the same or a new record depending on what happened in Case statement
						$JournalID_t:=syn_LoadFirstRecordInQueue(Storage:C1525.Local.ServerIdentifier)  // Load the next record
						
						If ($NumberOfFails_i>9)
							$Counter_i:=$Max_i+1
						End if 
						
						If (Tickcount:C458>($Start_i+3600))  //  maximum of one minute
							$Counter_i:=$Max_i+1
						End if 
						
					End for 
					
				Else 
					
					$CurrentDelay_i:=$CurrentDelay_i+Syn Timing Delay
					$CurrentDelay_i:=Syn_LesserOf($CurrentDelay_i;1800)  // Slow down to check every 30 seconds
					DELAY PROCESS:C323($ProcessID_i;Syn Timing Delay)
					
			End case 
			
			If (Storage:C1525.Syn.foundationPresent)
				EXECUTE METHOD:C1007("Fnd_Gen_QuitNow";$QuitNow_b)
			End if 
			
		Until ((syn_Quit>0) | ($QuitNow_b) | (Process aborted:C672) | (Current time:C178>$finishTime_h))
		
		//syn_C_CloseConnection ($ServerAddress_t)  // Close connection as we have been using keep alive
		
		If (Current time:C178>$finishTime_h) & (Not:C34((syn_Quit>0) | ($QuitNow_b) | (Process aborted:C672)))
			Syn_PushToServer(Storage:C1525.Local.ServerIdentifier)  //  relaunch in a new process
		End if 
		
		
	Else 
		
		$ProcessName_t:="Syn-Push to Server ("+Storage:C1525.Local.ServerIdentifier+"): "+Storage:C1525.Local.ServerAddress
		$ProcessID_i:=New process:C317(Current method name:C684;0;$ProcessName_t;Storage:C1525.Local.ServerIdentifier;0)
		RESUME PROCESS:C320($ProcessID_i)
		
End case 

LOG ENABLE($logging_b)