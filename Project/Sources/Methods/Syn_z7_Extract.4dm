//%attributes = {"invisible":true}
C_BOOLEAN:C305($0)
C_TEXT:C284($1)
C_TEXT:C284($2)
C_TEXT:C284($3)
C_POINTER:C301($4)

C_BLOB:C604($Response_x)
C_LONGINT:C283($NumberOfParameters_i)
C_TEXT:C284($command_t;$sourcePath_t;$targetPath_t)

If (False:C215)
	C_BOOLEAN:C305(Syn_z7_Extract;$0)
	C_TEXT:C284(Syn_z7_Extract;$1)
	C_TEXT:C284(Syn_z7_Extract;$2)
	C_TEXT:C284(Syn_z7_Extract;$3)
	C_POINTER:C301(Syn_z7_Extract;$4)
End if 

//flags set:
//-y Assume Yes on all queries

$NumberOfParameters_i:=Count parameters:C259

If ($NumberOfParameters_i>1)
	
	$sourcePath_t:=$1
	$targetPath_t:=$2
	
	$command_t:=" -y x "+SYN_LEP_EscapePath($sourcePath_t)+" -o"+SYN_LEP_EscapePath($targetPath_t)
	
	If ($NumberOfParameters_i>2)
		If (Length:C16($3)#0)
			$command_t:=$command_t+" -p"+Syn_LEP_Escape($3)
		End if 
	End if 
	
	If (Syn_z7_Execute($command_t;->$Response_x))
		
		$0:=True:C214
		
	Else 
		
		If ($NumberOfParameters_i>3)
			If (Not:C34(Is nil pointer:C315($4)))
				If (Type:C295($4->)=Is text:K8:3)
					$4->:=Convert to text:C1012($Response_x;"utf-8")
				End if 
			End if 
		End if 
		
	End if 
	
End if 