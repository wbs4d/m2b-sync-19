//%attributes = {"invisible":true,"shared":true}
// Method: Syn_CheckDuplicateKeys (pointer;{longint}):longint
// $0:longint - count
// $1:pointer - field pointer
// $2:boolean - selection constant - true => current selection only
//Description: Finds all records where passed field has duplicate values
//C_BOOLEAN($QMC000101;$QMU050823)
//C_BOOLEAN($DEV_Floyd_334)  // 3/27/2003  3:21 PM - Return count
//C_BOOLEAN($DEV_Floyd_432)  //08/23/05  12:24 PM - Clear arrays normally
// instead of clear variable
C_LONGINT:C283($0)
C_POINTER:C301($1)
C_BOOLEAN:C305($2)

C_BOOLEAN:C305($CurrentSelection_b)
C_LONGINT:C283($ArraySize_i; $FieldType_i; $loop_i)
C_POINTER:C301($IDField_ptr; $PointerToFieldArray_ptr; $Table_ptr)

ARRAY DATE:C224($DateArray_ad; 0)
ARRAY LONGINT:C221($IntArray_ai; 0)
ARRAY LONGINT:C221($LongArray_ai; 0)
ARRAY LONGINT:C221($RecordNumber_ai; 0)
ARRAY TEXT:C222($TextArray_at; 0)

If (False:C215)
	C_LONGINT:C283(Syn_CheckDuplicateKeys; $0)
	C_POINTER:C301(Syn_CheckDuplicateKeys; $1)
	C_BOOLEAN:C305(Syn_CheckDuplicateKeys; $2)
End if 

If (Count parameters:C259>=1)
	$IDField_ptr:=$1
	$Table_ptr:=Table:C252(Table:C252($1))
Else 
	$Table_ptr:=Current form table:C627
	$IDField_ptr:=Field:C253(Table:C252($Table_ptr); 1)
End if 

If (Is nil pointer:C315($IDField_ptr))
	//Fnd_Dlg_Alert ("Sorry, unable to determine ID field.")
Else 
	$CurrentSelection_b:=False:C215
	If (Count parameters:C259>=2)
		$CurrentSelection_b:=$2
	End if 
	
	If (Not:C34($CurrentSelection_b))
		ALL RECORDS:C47($Table_ptr->)
	End if 
	CREATE EMPTY SET:C140($Table_ptr->; "Dupes")
	
	$FieldType_i:=Type:C295($IDField_ptr->)
	Case of 
		: ($FieldType_i=Is alpha field:K8:1) | ($FieldType_i=Is text:K8:3)
			$PointerToFieldArray_ptr:=->$TextArray_at
		: ($FieldType_i=Is longint:K8:6) | ($FieldType_i=Is time:K8:8)
			$PointerToFieldArray_ptr:=->$LongArray_ai
		: ($FieldType_i=Is integer:K8:5)
			$PointerToFieldArray_ptr:=->$IntArray_ai
		: ($FieldType_i=Is date:K8:7)
			$PointerToFieldArray_ptr:=->$DateArray_ad
		Else 
			TRACE:C157  // unsupported type
	End case 
	
	SELECTION TO ARRAY:C260($Table_ptr->; $RecordNumber_ai; $IDField_ptr->; $PointerToFieldArray_ptr->)
	SORT ARRAY:C229($PointerToFieldArray_ptr->; $RecordNumber_ai)
	$ArraySize_i:=Size of array:C274($RecordNumber_ai)
	ARRAY LONGINT:C221($Flag_ai; $ArraySize_i)
	
	For ($loop_i; 1; $ArraySize_i-1)
		If ($PointerToFieldArray_ptr->{$loop_i}=$PointerToFieldArray_ptr->{$loop_i+1})
			$Flag_ai{$loop_i}:=1
			$Flag_ai{$loop_i+1}:=1
		End if 
	End for 
	
	For ($loop_i; 1; $ArraySize_i)
		If ($Flag_ai{$loop_i}>0)
			GOTO RECORD:C242($Table_ptr->; $RecordNumber_ai{$loop_i})
			ADD TO SET:C119($Table_ptr->; "Dupes")
		End if 
	End for 
	
	$0:=Records in set:C195("Dupes")
	
	USE SET:C118("Dupes")
	CLEAR SET:C117("Dupes")
	ORDER BY:C49($Table_ptr->; $IDField_ptr->)
	
	If (Count parameters:C259=0)
		
	End if 
	
End if 