//%attributes = {"invisible":true}
// ----------------------------------------------------
// Project Method: Syn_UnloadRecordReduceSelection (Table pointer or number)

// Pass a table pointer or number to:
//  1.  Unload the current record and
//  2.  Reduce the selction to 0 for that table

// Access: Private

// Parameters: 
//   $1 : Pointer or number : Table pointer or number

// Created by Wayne Stewart (2021-04-21T14:00:00Z)
//     wayne@4dsupport.guru
// ----------------------------------------------------

C_VARIANT:C1683($1)

C_POINTER:C301($Table_ptr)

If (False:C215)
	C_VARIANT:C1683(Syn_UnloadRecordReduceSelection; $1)
End if 

If (Value type:C1509($1)=Is pointer:K8:14)
	$Table_ptr:=$1
Else 
	$Table_ptr:=Table:C252($1)  // It's a table number
End if 

If (Is nil pointer:C315($Table_ptr))
Else 
	UNLOAD RECORD:C212($Table_ptr->)
	REDUCE SELECTION:C351($Table_ptr->; 0)
End if 