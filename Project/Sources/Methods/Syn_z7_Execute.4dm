//%attributes = {"invisible":true}
C_BOOLEAN:C305($0)
C_TEXT:C284($1)
C_POINTER:C301($2)
C_BLOB:C604($3)

C_BLOB:C604($stdErr;$stdIn;$stdOut)
C_LONGINT:C283($NumberOfParameters_i)
C_POINTER:C301($Response_p)
C_TEXT:C284($argument_t;$command_t;$error_t;$executablePath_t;$output_t)

ARRAY LONGINT:C221($len;0)
ARRAY LONGINT:C221($pos;0)

If (False:C215)
	C_BOOLEAN:C305(Syn_z7_Execute;$0)
	C_TEXT:C284(Syn_z7_Execute;$1)
	C_POINTER:C301(Syn_z7_Execute;$2)
	C_BLOB:C604(Syn_z7_Execute;$3)
End if 

$NumberOfParameters_i:=Count parameters:C259


If ($NumberOfParameters_i>1)
	
	$argument_t:=$1
	$executablePath_t:=Syn_z7_Get_path
	$command_t:=$executablePath_t+$argument_t
	$Response_p:=$2
	
	If ($NumberOfParameters_i>2)
		$stdIn:=$3
	End if 
	
	LAUNCH EXTERNAL PROCESS:C811($command_t;$stdIn;$stdOut;$stdErr)
	
	If (BLOB size:C605($stdErr)=0)
		
		$output_t:=BLOB to text:C555($stdOut;Choose:C955(Folder separator:K24:12=":";UTF8 text without length:K22:17;Mac text without length:K22:10))
		
		If (Match regex:C1019("(Error:|Errors:|WARNING:|CRC Failed)\\s*(.*)";$output_t;1;$pos;$len))
			$error_t:=Substring:C12($output_t;$pos{2};$len{2})
			If (Not:C34(Is nil pointer:C315($2)))
				Case of 
					: (Type:C295($2->)=Is text:K8:3)
						$2->:=$error_t
					: (Type:C295($2->)=Is BLOB:K8:12)
						CONVERT FROM TEXT:C1011($error_t;"utf-8";$2->)
				End case 
			End if 
		Else 
			$0:=True:C214
		End if 
	Else 
		$0:=False:C215
		If (Not:C34(Is nil pointer:C315($2)))
			Case of 
				: (Type:C295($2->)=Is text:K8:3)
					
					$2->:=BLOB to text:C555($stdErr;Choose:C955(Folder separator:K24:12=":";UTF8 text without length:K22:17;Mac text without length:K22:10))
					
				: (Type:C295($2->)=Is BLOB:K8:12)
					$2->:=$stdErr
			End case 
		End if 
	End if 
	
End if 