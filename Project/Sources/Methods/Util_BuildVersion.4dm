//%attributes = {}
////%attributes = {"invisible":true}
//// ----------------------------------------------------
//// Project Method: Fnd_FCS_BuildComponent

//// Builds the Foundation component

//// Access: Private

//// Parameters: 
////   $1 : Text : Version Number

//// Created by Wayne Stewart (2021-08-10T14:00:00Z)

////     wayne@4dsupport.guru
//// ----------------------------------------------------

//var $1;$versionNumber_t : Text

//var $templatePath_t;$json_t;$date_t;$year_t;$enteredValue_t;$archive_t : Text
//var $buildSettingsPath_t;$buildFolderPath_t;$LF;$strings_t;$builtComponent_t;$newName_t : Text
//var $info_o : Object


//$templatePath_t:=Get 4D folder(Current resources folder)+"InfoPlist.json"
//If (Test path name($templatePath_t)=Is a document)
//$LF:=Char(Line feed)
//$json_t:=Document to text($templatePath_t)
//$info_o:=JSON Parse($json_t)
//$date_t:=Fnd_Date_DateAndTimeToISO(Current date)
//$year_t:=Substring($date_t;1;4)

//If (Count parameters=1)
//$versionNumber_t:=$1
//OK:=1
//Else 
//$enteredValue_t:=Request("Please enter the new version number:";\
$info_o.CFBundleShortVersionString;\
"OK";"Cancel")
//If (OK=1)
//$versionNumber_t:=$enteredValue_t
//End if 
//End if 

//Else 
//OK:=0


//End if 

//If (OK=1)
//// Update the json
//$info_o.CFBundleShortVersionString:=$versionNumber_t
//$info_o.CFBundleGetInfoString:=$info_o.CFBundleName+" "\
+$versionNumber_t+", "\
+$date_t
//$info_o.NSHumanReadableCopyright:="Copyright © 2009-"+$year_t+", Walt Nelson"

//$json_t:=JSON Stringify($info_o;*)
//DELETE DOCUMENT($templatePath_t)
//TEXT TO DOCUMENT($templatePath_t;$json_t;"UTF-8";Document with LF)

//// Update the .strings document (this is the one used by Get Info)
//$strings_t:="/* Localized versions of Info.plist keys */"+$LF+$LF
//$strings_t:=$strings_t+"CFBundleName = \""+$info_o.CFBundleName+"\";"+$LF
//$strings_t:=$strings_t+"CFBundleShortVersionString = \""+$info_o.CFBundleShortVersionString+"\";"+$LF
//$strings_t:=$strings_t+"CFBundleGetInfoString = \""+$info_o.CFBundleGetInfoString+"\";"+$LF
//$strings_t:=$strings_t+"NSHumanReadableCopyright = \""+$info_o.NSHumanReadableCopyright+"\";"

//$templatePath_t:=Get 4D folder(Current resources folder)+"InfoPlist.strings"

//DELETE DOCUMENT($templatePath_t)
//TEXT TO DOCUMENT($templatePath_t;$strings_t;"UTF-8";Document with LF)

//End if 

//If (OK=1)  // If any of the previous steps failed OK will be 0
//$buildSettingsPath_t:=Get 4D file(Build application settings file)
//$buildFolderPath_t:=Get 4D folder(Database folder)+"Builds"+Folder separator+"Components"+Folder separator

//Fnd_FCS_WriteDocumentation("";True;True)  // Rewrite the documentation, eliminating the private methods
//BUILD APPLICATION($buildSettingsPath_t)
//Fnd_FCS_WriteDocumentation("";True;False)  // Rewrite the documentation, now documenting the private methods

//If (OK=1)
//SHOW ON DISK($buildFolderPath_t)
//ALERT("Successful Build")
//Else 
//ALERT("Component Build Failed")
//End if 

//End if 