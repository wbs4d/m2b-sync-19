//%attributes = {"invisible":true}
C_TEXT:C284($0)
C_OBJECT:C1216($1)

C_LONGINT:C283($Table_i)
C_POINTER:C301($SyncField_ptr;$Table_ptr)
C_TEXT:C284($RecordID_t;$Result_t)
C_OBJECT:C1216($Packet_o)

If (False:C215)
	C_TEXT:C284(Syn_DeleteRecord;$0)
	C_OBJECT:C1216(Syn_DeleteRecord;$1)
End if 

C_BOOLEAN:C305($logging_b)
$logging_b:=LOG ENABLE
LOG ENABLE(True:C214)

$Packet_o:=$1

$Result_t:=""

$Table_i:=$Packet_o.TblNo

$RecordID_t:=sync_Sync_ID_ptr->

$Table_ptr:=Table:C252($Table_i)
$SyncField_ptr:=sync_CachedStructure_aptr{$Table_i}

QUERY:C277($Table_ptr->;$SyncField_ptr->=$RecordID_t)

If (Records in selection:C76($Table_ptr->)=0)
	//  This shouldn't happen but if it does we will log the fault…
	//   However this won't block the processing queue
	If (Storage:C1525.Local.logEverything=1) | (Storage:C1525.Local.logErrors=1)
		LOG ADD ENTRY(sync_PK_ptr->;Current method name:C684;"["+Table name:C256($Table_ptr)+"] "+Syn Err Record does not exist;$RecordID_t)
	End if 
	
	$Result_t:=Syn Record Does Not Exist
Else 
	
	If (Syn_RecordLoad($Table_ptr))  //  This will attempt to load the record for 18 minutes and then give up
		DELETE RECORD:C58($Table_ptr->)
		Syn_UnloadRecordReduceSelection($Table_ptr)
		$Result_t:=Syn OK
		
		If (Storage:C1525.Local.logEverything=1)
			LOG ADD ENTRY(sync_PK_ptr->;Current method name:C684;Syn OK;"["+Table name:C256($Table_i)+"]";$RecordID_t)
		End if 
		
	Else 
		$Result_t:=Syn Locked Record  //  This error will block the queue
		If (Storage:C1525.Local.logErrors=1) | (Storage:C1525.Local.logEverything=1)  // Now log the error
			LOG ADD ENTRY(sync_PK_ptr->;Current method name:C684;Syn Err Locked Record;Table name:C256($Table_ptr);$RecordID_t)
		End if 
	End if 
	
End if 

LOG ENABLE($logging_b)

$0:=$Result_t