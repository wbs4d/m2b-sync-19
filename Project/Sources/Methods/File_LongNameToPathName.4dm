//%attributes = {"invisible":true}
// Project Method: Util_LongNameToPathName (path to a document) --> Text
// From the 4D Documentation. Takes a fully qualified path name and strips
//   off the file name so we’re left with just the path name to the folder.
// from 4DToday tips
If (False:C215)
	C_TEXT:C284(File_LongNameToPathName;$0;$1)
End if 

C_TEXT:C284($0;$1;$originalPath)
C_LONGINT:C283($len;$pos;$char;$dirSymbolAscii)  //;$platform)

$originalPath:=$1

$dirSymbolAscii:=Character code:C91(Folder separator:K24:12)

$len:=Length:C16($originalPath)
$pos:=0

For ($char;$len;1;-1)
	If (Character code:C91($originalPath[[$char]])=$dirSymbolAscii)
		$pos:=$char
		$char:=0
	End if 
End for 

If ($pos>0)
	$0:=Substring:C12($originalPath;1;$pos)
Else 
	$0:=$originalPath
End if 
