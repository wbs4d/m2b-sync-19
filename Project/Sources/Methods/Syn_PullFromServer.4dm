//%attributes = {"invisible":true,"executedOnServer":true}
// Syn_PullFromServer
// Created by Wayne Stewart (Jul 5, 2013)
//  Method is an autostart type
//     waynestewart@mac.com
C_TEXT:C284($1)
C_LONGINT:C283($2)

C_BOOLEAN:C305($QuitNow_b)
C_LONGINT:C283($ProcessID_i;$RecordsRemaining_i;$Counter_i;$Max_i;$Start_i;$EmptyCallCount_i;$Delay_i)
C_TIME:C306($finishTime_h)
C_TEXT:C284($ProcessName_t;$ServerAddress_t;$ServerIdentifier_t)

If (False:C215)
	C_TEXT:C284(Syn_PullFromServer;$1)
	C_LONGINT:C283(Syn_PullFromServer;$2)
End if 
C_BOOLEAN:C305($logging_b)
$logging_b:=LOG ENABLE
LOG ENABLE(True:C214)
Syn_Init

If (Count parameters:C259>0)
	$ServerIdentifier_t:=$1
End if 

Case of 
	: (Count parameters:C259=0)  //  First called without any parameters
		
		If (Storage:C1525.Local.ServerIdentifier=Null:C1517)
			$ServerIdentifier_t:=""
		Else 
			$ServerIdentifier_t:=Storage:C1525.Local.ServerIdentifier
		End if 
		
		If (Length:C16($ServerIdentifier_t)>0)  //  If there is no server specified, end of method
			Syn_PullFromServer($ServerIdentifier_t)  //  Otherwise, call again with Server name specified
		End if 
		
	: (Count parameters:C259=1)  //  Now that we have the server specified, we can set up the new process
		If (Storage:C1525.Local.ServerIdentifier#Null:C1517)  // Don't go any further
			$ServerIdentifier_t:=Storage:C1525.Local.ServerIdentifier
			
			$ProcessName_t:="Syn-Pull from Server ("+$ServerIdentifier_t+"): "+Storage:C1525.Local.ServerAddress
			$ProcessID_i:=New process:C317(Current method name:C684;0;$ProcessName_t;$ServerIdentifier_t;0;*)
		End if 
		
	: (Count parameters:C259=2)  //  Now ready to run…
		$ServerIdentifier_t:=Storage:C1525.Local.ServerIdentifier
		
		//  We set up some timers so that the process dies after a period & relaunches
		$ProcessID_i:=Current process:C322
		//$finishTime_h:=Current time+?00:10:00?  // We will kill process in ten minutes time
		
		//  Set up the communications
		WEB SERVICE SET OPTION:C901(Web Service HTTP timeout:K48:9;30)  //  30 second time out (default is 180 seconds)
		//Log_Enabled (KVP_Longint (Syn Log Errors)=1)  //  Turn logging on or off
		ON ERR CALL:C155("Syn_ErrorSOAP")
		
		$EmptyCallCount_i:=0
		Repeat 
			//  Call this every time, in case the user has changed ip address
			$ServerAddress_t:=Syn_GetServerAddress
			$ServerIdentifier_t:=Storage:C1525.Local.ServerIdentifier
			
			//  Find out how many records are on the server
			$RecordsRemaining_i:=Syn_C_CheckQueueLength($ServerIdentifier_t;$ServerAddress_t)
			
			If ($RecordsRemaining_i=0)  // Delay loop for a bit before calling again
				$EmptyCallCount_i:=$EmptyCallCount_i+1
				$Delay_i:=$EmptyCallCount_i*Syn Timing Delay  // Syn Timing Delay = 5 seconds
				$Delay_i:=Syn_LesserOf($Delay_i;3600)  //  If nothing happening contact at least once per minute
				DELAY PROCESS:C323($ProcessID_i;$Delay_i)
				If (Storage:C1525.Local.logEverything=1) & ($EmptyCallCount_i>0)
					LOG ADD ENTRY("Syn_C_CheckQueueLength";"Delay: "+String:C10($Delay_i\60)+" seconds")
				End if 
				
			Else 
				
				// At most we want to retreive 10 records without checking for quitting
				$Max_i:=Syn_LesserOf($RecordsRemaining_i;10)
				$Start_i:=Tickcount:C458
				
				For ($Counter_i;1;$Max_i)  //  Run through 10 records
					syn_C_RequestUpdate(Storage:C1525.Local.SiteIdentifier;$ServerAddress_t)  //  quickly
					
					If (Tickcount:C458>($Start_i+3600))  //  maximum of one minute
						$Counter_i:=$Max_i+1
					End if 
				End for 
			End if 
			
			
			//  Check if Foundation wants us to quit
			If (Storage:C1525.Syn.foundationPresent)
				EXECUTE METHOD:C1007("Fnd_Gen_QuitNow";$QuitNow_b)
			Else 
				$QuitNow_b:=False:C215
			End if 
			
			// Exit or repeat loop
		Until ((syn_Quit>0) | ($QuitNow_b) | (Process aborted:C672))
		
		//syn_C_CloseConnection ($ServerAddress_t)  // Close connection as we have been using keep alive
		
		// If we exited from loop after one hour we actually want to restart again
		//If (Current time>$finishTime_h) & (Not((syn_Quit>0) | ($QuitNow_b) | (Process aborted)))
		//Syn_PullFromServer($ServerIdentifier_t)  //  relaunch in a new process
		//End if 
		
		ON ERR CALL:C155("")
		
		
End case 

LOG ENABLE($logging_b)