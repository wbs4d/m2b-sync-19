//%attributes = {"invisible":true}

// ----------------------------------------------------
// Project Method: syn_CacheStructure

// Method Type:    Private

// Parameters:

// Local Variables:

// Created by Wayne Stewart (Sep 28, 2013)
//     waynestewart@mac.com

//This method creates a cache of the structure
//so that [Sync] table and [...]JournalID fields
//can be accessed by pointers.

// ----------------------------------------------------

C_BOOLEAN:C305($FoundData_b;$FoundID_b)
C_LONGINT:C283($CurrentField_i;$CurrentTable_i;$NumberofFields_i;$NumberofTables_i)
C_POINTER:C301($IDField_ptr)

$NumberofTables_i:=Get last table number:C254

ARRAY POINTER:C280(sync_CachedStructure_aptr;$NumberofTables_i)

For ($CurrentTable_i;1;$NumberofTables_i)
	If (Is table number valid:C999($CurrentTable_i))
		$NumberofFields_i:=Get last field number:C255($CurrentTable_i)
		If (Table name:C256($CurrentTable_i)="Sync")
			sync_Table_ptr:=Table:C252($CurrentTable_i)
			sync_PK_ptr:=Field:C253($CurrentTable_i;1)  // This is the time stamp not guaranteed to be unique
			sync_Sync_ID_ptr:=Field:C253($CurrentTable_i;2)  //  This is the unique identifier for the target table, but not unique in [sync] table
			sync_ForIdentifier_ptr:=Field:C253($CurrentTable_i;3)  //  This is the target destination
			sync_SmallFieldData_ptr:=Field:C253($CurrentTable_i;4)  //  A json containing all the smaller fields and some update meta data
			sync_LargeFieldData_ptr:=Field:C253($CurrentTable_i;5)  //  A Blob containing larger fields
			sync_NumberOfRetries_ptr:=Field:C253($CurrentTable_i;6)  //  Number of unsuccessful integration attempts for this record 
			sync_DTS_Received_ptr:=Field:C253($CurrentTable_i;7)  //  When this record was received by the sync client
			sync_Update_Type_ptr:=Field:C253($CurrentTable_i;8)  //  INS, MOD or DEL
			sync_DTS_Created_ptr:=Field:C253($CurrentTable_i;9)  //  When this sync record was first created on original computer
			sync_JournalID_ptr:=Field:C253($CurrentTable_i;10)  //  Unique identifier for this table
			
		Else 
			$FoundData_b:=False:C215
			$FoundID_b:=False:C215
			For ($CurrentField_i;1;$NumberofFields_i)
				If (Is field number valid:C1000($CurrentTable_i;$CurrentField_i))
					If (Field name:C257($CurrentTable_i;$CurrentField_i)="_UUID_PK")
						$IDField_ptr:=Field:C253($CurrentTable_i;$CurrentField_i)
						$FoundID_b:=True:C214
					End if 
					If ($FoundID_b)
						$CurrentField_i:=$NumberofFields_i+1
						sync_CachedStructure_aptr{$CurrentTable_i}:=$IDField_ptr
					End if 
				End if 
			End for 
		End if 
	End if 
End for 

