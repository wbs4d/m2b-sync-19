//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Method: Syn_CheckForOnHold
// Description:
//
//
// Parameters:
C_TEXT:C284($1)

C_LONGINT:C283($Bracket_i)
C_TEXT:C284($BumpedIdentifier_t; $LocalIdentifier_t; $Prefix_t)

If (False:C215)
	C_TEXT:C284(Syn_CheckForOnHold; $1)
End if 

// ----------------------------------------------------
// Created by Wayne Stewart (2015-11-01T13:00:00Z)
//     waynestewart@mac.com
// ----------------------------------------------------

$LocalIdentifier_t:=$1

If ($LocalIdentifier_t="X33")
Else 
	$Bracket_i:=Num:C11($LocalIdentifier_t)
	$Prefix_t:=$LocalIdentifier_t[[1]]
	
	$Bracket_i:=$Bracket_i+Syn Bump From Queue
	
	$BumpedIdentifier_t:=$Prefix_t+String:C10($Bracket_i; "00")
	
	READ ONLY:C145(sync_Table_ptr->)
	QUERY:C277(sync_Table_ptr->; sync_ForIdentifier_ptr->=$BumpedIdentifier_t)
	
	SELECTION TO ARRAY:C260(sync_Sync_ID_ptr->; Syn_UpdatesOnHold_at)
	
	Syn_UnloadRecordReduceSelection(sync_Table_ptr)
	
End if 