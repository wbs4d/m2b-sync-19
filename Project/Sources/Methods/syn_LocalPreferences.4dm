//%attributes = {"invisible":true}
C_LONGINT:C283($SyncLocal_i)

// ----------------------------------------------------
// User name (OS): Wayne Stewart
// Date and time: Jan 5, 2013
If (False:C215)
	syn_LocalPreferences
End if 
// ----------------------------------------------------
// Method: sync_LocalPreferences
// Description
//
//
// Parameters
//
// ----------------------------------------------------

If (Dict_IsValid(Dict_Named("SyncLocal")))
	Dict_Release(Dict_Named("SyncLocal"))
End if 

If (Test path name:C476(KVP_Text("SyncDefaults.PrefsPath"))=Is a document:K24:1)
	<>Sync_LocalDictionary_i:=Dict_LoadFromFile(KVP_Text("SyncDefaults.PrefsPath"))
Else 
	<>Sync_LocalDictionary_i:=Dict_New("SyncLocal")  // This will set up SiteIdentifier only
	Dict_SetText(<>Sync_LocalDictionary_i;"SiteIdentifier";KVP_Text("SyncDefaults.SiteIdentifier"))
End if 