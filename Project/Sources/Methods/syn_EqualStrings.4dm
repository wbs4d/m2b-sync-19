//%attributes = {"invisible":true}
C_BOOLEAN:C305($0)
C_TEXT:C284($1)
C_TEXT:C284($2)

C_LONGINT:C283($LengthOfStringOne_i; $LengthOfStringTwo_i)

If (False:C215)
	C_BOOLEAN:C305(syn_EqualStrings; $0)
	C_TEXT:C284(syn_EqualStrings; $1)
	C_TEXT:C284(syn_EqualStrings; $2)
End if 

$LengthOfStringOne_i:=Length:C16($1)
$LengthOfStringTwo_i:=Length:C16($2)

Case of 
	: ($LengthOfStringOne_i=0) & ($LengthOfStringTwo_i=0)
		$0:=True:C214
	: ($LengthOfStringOne_i#$LengthOfStringTwo_i)
	: (Position:C15($1; $2; *)#1)
	Else 
		$0:=True:C214
End case 