//%attributes = {"invisible":true}
// ----------------------------------------------------
// Project Method: Sync_Info (label) --> Text

// Returns requested information.  See the Fnd_Gen_ComponentInfo 
//   method for more information.

// Method Type: Protected

// Parameters: 
//   $1 : Text : Info desired

// Returns: 
//   $0 : Text : Response

// Created by Wayne Stewart (Jul 22, 2012)
//     waynestewart@mac.com
// ----------------------------------------------------

C_TEXT:C284($0;$1;$request_t;$reply_t)

$request_t:=$1

Syn_Init

Case of 
	: ($request_t="version")
		$reply_t:="1.0"
		
	: ($request_t="name")
		$reply_t:="Syn"
		
	: ($request_t="logging")
		If (LOG ENABLE)
			$reply_t:="enabled"
		Else 
			$reply_t:="disabled"
		End if 
		
		
	Else 
		$reply_t:="Fnd_LabelNotRecognized"
End case 

$0:=$reply_t
