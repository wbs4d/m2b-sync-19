//%attributes = {"invisible":true}
// Project Method: Syn_ObjectLoadFromFile (File Path) --> Object

//Expects the passed in file to have an uncompressed JSON string as it's contents.

//Expects the JSON string to have an object ("{}") at the top level,
//not an array ("[]"). If it looks like the top level is an array
//(ie. the first character is a "["), the entire array will be automatically
//embedded in a new top level object named "OBJ".

//Use OBJ_IsValid after the call to know if this method was able
//to parse the JSON into an object.

// Access: Shared

// Parameters: 
//   $1 : Text : A text document containing a JSON string

// Returns: 
//   $0 : Object

// Created by Cannon Smith
// ----------------------------------------------------

C_TEXT:C284($1; $Filepath_t)
C_OBJECT:C1216($0; $Object_o)

$Filepath_t:=$1

C_TEXT:C284($JSONString_t)

If (Test path name:C476($Filepath_t)=Is a document:K24:1)
	$JSONString_t:=Document to text:C1236($Filepath_t)
	$Object_o:=JSON Parse:C1218($JSONString_t)
End if 

$0:=$Object_o
