//%attributes = {"invisible":true}
// ----------------------------------------------------
// Project Method: syn_UpdateQueueDisplay

// Description

// Access: Private

// Parameters:

// Created by Wayne Stewart (2016-06-22T14:00:00Z)
//     waynestewart@mac.com
// ----------------------------------------------------



C_LONGINT:C283($Bracket_i; $Bumped_i; $CurrentSite_i; $NumberInQueue_i; $NumberOfSites_i)
C_POINTER:C301($FixedQueue_ptr; $NumberInQueue_ptr; $Object_ptr; $QueueLength_ptr; $SiteArray_ptr)
C_TEXT:C284($Bumped_t; $Prefix_t)


$SiteArray_ptr:=OBJECT Get pointer:C1124(Object named:K67:5; "Site")
$QueueLength_ptr:=OBJECT Get pointer:C1124(Object named:K67:5; "Queue Length")
$FixedQueue_ptr:=OBJECT Get pointer:C1124(Object named:K67:5; "Fixed Queue")

ARRAY TEXT:C222($SiteArray_ptr->; 0)

Syn_SendUpdatesTo($SiteArray_ptr; Current method name:C684)
If (Find in array:C230($SiteArray_ptr->; Storage:C1525.Local.SiteIdentifier)=-1)
	APPEND TO ARRAY:C911($SiteArray_ptr->; Storage:C1525.Local.SiteIdentifier)  //  Add the local queue as well
End if 
SORT ARRAY:C229($SiteArray_ptr->)

$NumberOfSites_i:=Size of array:C274($SiteArray_ptr->)
ARRAY LONGINT:C221($QueueLength_ptr->; $NumberOfSites_i)

$NumberInQueue_ptr:=->$NumberInQueue_i
SET QUERY DESTINATION:C396(Into variable:K19:4; ""; $NumberInQueue_ptr)

For ($CurrentSite_i; 1; $NumberOfSites_i)
	QUERY:C277(sync_Table_ptr->; sync_ForIdentifier_ptr->=$SiteArray_ptr->{$CurrentSite_i})
	$QueueLength_ptr->{$CurrentSite_i}:=$NumberInQueue_i
	
End for 

$Object_ptr:=OBJECT Get pointer:C1124(Object named:K67:5; "SmallFieldData")
$Object_ptr->:=""

$Object_ptr:=OBJECT Get pointer:C1124(Object named:K67:5; "Globbo Size")
$Object_ptr->:=0

$Object_ptr:=OBJECT Get pointer:C1124(Object named:K67:5; "Globbo Size1")
$Object_ptr->:=0

$Object_ptr:=OBJECT Get pointer:C1124(Object named:K67:5; "On Hold")
$Object_ptr->:=Size of array:C274(Syn_UpdatesOnHold_at)

$Object_ptr:=OBJECT Get pointer:C1124(Object named:K67:5; "Bumped")

$Bracket_i:=Num:C11(Storage:C1525.Local.SiteIdentifier)
$Prefix_t:=Storage:C1525.Local.SiteIdentifier[[1]]
$Bracket_i:=$Bracket_i+Syn Bump From Queue

$Bumped_t:=$Prefix_t+String:C10($Bracket_i; "00")

SET QUERY DESTINATION:C396(Into variable:K19:4; $Bumped_i)
QUERY:C277(sync_Table_ptr->; sync_ForIdentifier_ptr->=$Bumped_t)

$Object_ptr->:=$Bumped_i

LISTBOX SELECT ROW:C912(*; "Sites List"; 0; lk remove from selection:K53:3)
LISTBOX SELECT ROW:C912(*; "Sync_Queue"; 0; lk remove from selection:K53:3)

SET QUERY DESTINATION:C396(Into current selection:K19:1)
$CurrentSite_i:=$FixedQueue_ptr->
If ($CurrentSite_i>1)
	QUERY:C277(sync_Table_ptr->; sync_ForIdentifier_ptr->=$FixedQueue_ptr->{$CurrentSite_i})
Else 
	ALL RECORDS:C47(sync_Table_ptr->)
End if 
ORDER BY:C49(sync_Table_ptr->; sync_PK_ptr->; >)

