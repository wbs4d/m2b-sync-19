//%attributes = {"invisible":true,"shared":true,"executedOnServer":true}
// syn_ApplyUpdates
// Created by Wayne Stewart (Jul 6, 2013)
//  Method is an autostart type
//     waynestewart@mac.com
C_LONGINT:C283($1)

C_BOOLEAN:C305($ExitNow_b;$HideRecord_b;$QuitNow_b;$logging_b)
C_LONGINT:C283($Bracket_i;$Prefix_i;$ProcessID_i)
C_TIME:C306($finishTime_h)
C_TEXT:C284($Action_t;$NewFor_t;$OldFor_t;$SyncPKInstruction_t;$Prefix_t;$Result_t;$SyncIDOfCurrentInstruction_t;$TableName_t;$SyncRecordJournalID_t)
C_OBJECT:C1216($Packet_o)

If (False:C215)
	C_LONGINT:C283(Syn_ApplyUpdates;$1)
End if 

//Launches the Apply Update process on the server.
//
//The basic idea is a looping method that looks for a
//sync record to process, then it calls DELAY PROCESS on
//itself and then wakes and looks again.
//
//The method will run for at most one hour and then
//relaunch a new copy of itself and then terminate.
//
//Once it finds a record in the [Sync] table it applies
//whatever instructions are contained within the sync
//record.  If there is more than one record the earliest
//record will be chosen.
//
//This may be:
//Create a New Record
//Modify an existing record
//Delete an existing record
//
//The sync record itself is then deleted.



Syn_Init



If (Count parameters:C259=1)
	
	$logging_b:=Log Enable
	Log Enable(True:C214)
	
	If (Syn.SOE=Null:C1517)
		Syn.SOE:=New object:C1471(\
			"Count";0;\
			"ReinsertInQueue";"")
	End if 
	
	
	$finishTime_h:=Current time:C178+?01:00:00?  // We will kill process in one hours time
	Syn.ApplyUpdatesProcessID:=Current process:C322
	
	READ ONLY:C145(*)  //  Everything should be read only
	
	Repeat 
		
		Syn_CheckForOnHold(Storage:C1525.Local.SiteIdentifier)  // Check for on hold
		
		$HideRecord_b:=False:C215
		Case of 
			: (Syn.ApplyUpdatesProcessID#Current process:C322)  // Should not have any other processes running with the same name
				$ExitNow_b:=True:C214  // This will kill this loop
				
			: (syn_RecordsInQueue(Storage:C1525.Local.SiteIdentifier)>0)  //  Is there a record queued up for this computer?
				// If there is a record we need to load it now
				START TRANSACTION:C239
				
				$SyncRecordJournalID_t:=syn_LoadFirstRecordInQueue(Storage:C1525.Local.SiteIdentifier)
				$SyncIDOfCurrentInstruction_t:=sync_Sync_ID_ptr->
				$SyncPKInstruction_t:=sync_PK_ptr->  //  So we know which one to delete later
				$Action_t:=sync_Update_Type_ptr->
				
				$Packet_o:=Syn_ExtractDictionary  //  Assumes record is loaded, this is the JSON identifier
				
				Case of 
					: ($Packet_o=Null:C1517)  //  This will happen if the dictionary was unable to be extracted correctly
						Case of 
							: (Storage:C1525.Local.logEverything=1)
								LOG ADD ENTRY(sync_PK_ptr->;Current method name:C684;Syn Err Unable to extract;sync_SmallFieldData_ptr->;sync_Sync_ID_ptr->)
							: (Storage:C1525.Local.logErrors=1)
								LOG ADD ENTRY(sync_PK_ptr->;Current method name:C684;Syn Err Unable to extract;sync_SmallFieldData_ptr->;sync_Sync_ID_ptr->)
						End case 
						Syn_DeleteSpecifiedQueueItem($SyncRecordJournalID_t;Current method name:C684;Syn Err Unable to extract)  //  If we get invalid JSON we should delete it
						
						VALIDATE TRANSACTION:C240  //  This will validate the deletion of the incorrect update
						
						
					: (OB Is empty:C1297($Packet_o))  //  This will happen if the dictionary was unable to be extracted
						Case of 
							: (Storage:C1525.Local.logEverything=1)
								LOG ADD ENTRY(sync_PK_ptr->;Current method name:C684;Syn Err Unable to extract;sync_SmallFieldData_ptr->;sync_Sync_ID_ptr->)
							: (Storage:C1525.Local.logErrors=1)
								LOG ADD ENTRY(sync_PK_ptr->;Current method name:C684;Syn Err Unable to extract;sync_SmallFieldData_ptr->;sync_Sync_ID_ptr->)
						End case 
						Syn_DeleteSpecifiedQueueItem($SyncRecordJournalID_t;Current method name:C684;Syn Err Unable to extract)  //  If we get invalid JSON we should delete it
						
						VALIDATE TRANSACTION:C240  //  This will validate the deletion of the incorrect update
						
					Else 
						
						//  What Table are we updating?
						$TableName_t:=Table name:C256($Packet_o.TblNo)
						
						If (Length:C16($Action_t)=0)  // If it isn't set read it from the packet
							$Action_t:=$Packet_o.Act
						End if 
						
						Case of 
							: ($Action_t="INS")
								$Result_t:=Syn_CreateRecord($Packet_o)
								
							: ($Action_t="MOD")
								$Result_t:=Syn_UpdateRecord($Packet_o)
								
							: ($Action_t="DEL")
								$Result_t:=Syn_DeleteRecord($Packet_o)
								
						End case 
						
						Syn_UnloadRecordReduceSelection($Packet_o.TblNo)
						
						Case of 
							: ($Result_t=Syn OK)
								Syn_DeleteSpecifiedQueueItem($SyncRecordJournalID_t;Current method name:C684;$Result_t)
								
							: (($Action_t="DEL") & ($Result_t=Syn Record Does Not Exist))
								$HideRecord_b:=True:C214  //  We can't delete it just yet!
								// Syn_DeleteSpecifiedQueueItem ($SyncPKInstruction_t;Current method name;$Result_t) 
								//  If we get a "record does not exist" error
								//   and we were attempting to delete it
								//   anyway, basically we don't care!
								
							: ($Result_t=Syn Record Does Not Exist)
								// We will be stuck in an infinite loop here if not careful.
								//  Two options:
								//    1) Put it to the end of the queue (maybe the record will show up later?)
								//    2) "Hide" the record in an imaginary queue
								$HideRecord_b:=True:C214
								
							: (($Action_t="INS") & ($Result_t=Syn Push Duplicate Record))
								// This should be impossible and it has already been logged.
								// $HideRecord_b:=True
								Syn_DeleteSpecifiedQueueItem($SyncRecordJournalID_t;Current method name:C684;$Result_t)  // No longer hide these records, just delete them
								
						End case 
						
						//  Do we need to "hide" sync record?
						If ($HideRecord_b)
							Syn_HideUpdate($TableName_t;$SyncRecordJournalID_t;$SyncIDOfCurrentInstruction_t)
						End if 
						
						// Check to see if there are any hidden updates that need to placed back in queue
						If (Length:C16(Syn.SOE.ReinsertInQueue)>0)
							Syn_ReinsertHiddenUpdates
						End if 
						
						VALIDATE TRANSACTION:C240  //  Validate the record add/update/delete & the deletion of the instruction
						
						
				End case 
				
				
				
			Else 
				DELAY PROCESS:C323(Current process:C322;Syn Timing Delay)  // Delay for 5 seconds before next check if nothing in queue
		End case 
		
		If (Storage:C1525.Syn.foundationPresent)
			EXECUTE METHOD:C1007("Fnd_Gen_QuitNow";$QuitNow_b)
		End if 
		
	Until ((syn_Quit>0) | ($QuitNow_b) | ($ExitNow_b) | (Process aborted:C672))  // | (Current time>$finishTime_h))
	
	
	Log Enable($logging_b)
	
	//If ($ExitNow_b)
	////  Don't launch another one!!
	//Else 
	
	//// Reconsider adding back to queue here?
	
	
	////If (Current time>$finishTime_h) & (Not((syn_Quit>0) | ($QuitNow_b) | (Process aborted)))
	////Syn_ApplyUpdates  //  relaunch in a new process
	////End if 
	//End if 
	
Else 
	// This version allows for any number of processes
	Syn.ApplyUpdatesProcessID:=New process:C317(Current method name:C684;0;Current method name:C684;0;*)
	// This version allows for one unique process
	//$ProcessID_i:=New process(Current method name;0;Current method name;0;*)
	RESUME PROCESS:C320($ProcessID_i)
	//SHOW PROCESS($ProcessID_i)
	//BRING TO FRONT($ProcessID_i)
End if 
