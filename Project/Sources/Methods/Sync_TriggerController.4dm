//%attributes = {}
// ----------------------------------------------------
// User name (OS): Wayne Stewart
// ----------------------------------------------------
// Method: Sync_TriggerController
// Description
// 
//
// Parameters
// 
// ----------------------------------------------------

//  This method is called in all triggers
//  it allows you to determine if you want the triggers to run or not

$0:=""

Case of 
	: (True:C214)  //   Normal Operation
		Case of 
			: ($1="InSyncProcess")
				If (Syn_PerformingUpdate)
					$0:="True"
				Else 
					$0:="False"
				End if 
				
			: ($1="Trigger")
				Syn_Trigger  //  Run the Sync code to record the activity
				
		End case 
		
	: (False:C215)  //  Table Trigger will not run and Table will not sync
		$0:="True"
		
	: (False:C215)  //  Table Trigger will run but Table will not sync
		$0:="False"
		
	: (False:C215)  //  Table will sync but Table Trigger will not run
		$0:="true"
		If ($1="Trigger")
			Syn_Trigger
		End if 
		
End case 