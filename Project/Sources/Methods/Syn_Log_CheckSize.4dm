//%attributes = {"invisible":true}
// ----------------------------------------------------
// Method: Syn_CheckLogSize
// Description:
//    Limits the maximum size of the log to 20 MB
//
// Parameters:
// ----------------------------------------------------
// Created by Wayne Stewart (2015-08-11T14:00:00Z)
//     waynestewart@mac.com
// ----------------------------------------------------
C_LONGINT:C283($MaxSize_i;$Size_i)
C_TEXT:C284($NewName_t;$SyncFolder_t)


//ARRAY TEXT($Documents_at;0)

$SyncFolder_t:=KVP_Text("SyncDefaults.Sync Folder")

If (Test path name:C476(<>Syn_Log_Path_t)=Is a document:K24:1)  //  Only do this if the document exists
	$Size_i:=Get document size:C479(<>Syn_Log_Path_t)
	$MaxSize_i:=20*1024*1024
	
	If ($Size_i>$MaxSize_i)
		$NewName_t:="Sync Error Log ("+Syn_GetTimeStamp(Syn TS DateTime Only)+").txt"
		MOVE DOCUMENT:C540(<>Syn_Log_Path_t;$SyncFolder_t+$NewName_t)  //  Get it out of the way
		
	End if 
End if 