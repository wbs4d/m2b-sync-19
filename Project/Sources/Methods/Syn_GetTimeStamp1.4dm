//%attributes = {"invisible":true,"shared":true,"executedOnServer":true}
// ----------------------------------------------------
// Project Method: Syn_GetTimeStamp (Base Only; Date; Time ) --> Text

// Description
//  Creates a time stamp based on the following:
//  YYYYMMDDHHMMSS
//  00000 additional time stamps within a second
//  00 bracket based on ServerIdentifier

// Access: Shared

// Parameters: 
//   $1 : BOOLEAN : Pass true to show only the base date and time only
//   $2 : Date : Use this date (Default current date)
//   $3 : Time : Use this time (Default current time)

// Returns: 
//   $0 : TEXT : Time Stamp

// Created by Wayne Stewart (2016-05-15T14:00:00Z)
//     waynestewart@mac.com
// ----------------------------------------------------


//  Creates a time stamp consisting of
//  Current Date
//  Current Time
//  A counter within last second
//  The bracket based on SiteIdentifier


C_TEXT:C284($0)
C_LONGINT:C283($1;$TimeStampMode_i)
C_DATE:C307($2)
C_TIME:C306($3)

C_DATE:C307($Now_d)
C_LONGINT:C283($Day_i;$Month_i;$Years_i)
C_TIME:C306($Now_h)
C_TEXT:C284($Time_t;$TimeStamp_t)

If (False:C215)
	C_TEXT:C284(Syn_GetTimeStamp;$0)
	C_DATE:C307(Syn_GetTimeStamp;$2)
	C_TIME:C306(Syn_GetTimeStamp;$3)
	C_LONGINT:C283(Syn_GetTimeStamp;$1)
End if 

Case of 
	: (Count parameters:C259=0)
		$Now_d:=Current date:C33
		$Now_h:=Current time:C178
		$TimeStampMode_i:=Syn TS DateTime Bracketed
		
	: (Count parameters:C259=1)
		$Now_d:=Current date:C33
		$Now_h:=Current time:C178
		$TimeStampMode_i:=$1
		
	: (Count parameters:C259=2)
		$Now_d:=$2
		$Now_h:=Current time:C178
		$TimeStampMode_i:=$1
		
	: (Count parameters:C259=3)
		$Now_d:=$2
		$Now_h:=$3
		$TimeStampMode_i:=$1
		
End case 

//$Years_i:=Year of($Now_d)
//$Month_i:=Month of($Now_d)
//$Day_i:=Day of($Now_d)
//$TimeStamp_t:=String($Years_i;"0000")+String($Month_i;"00")+String($Day_i;"00")


//$Time_t:=String($Now_h;HH MM SS)
//$Time_t:=Replace string($Time_t;":";"";*)
//$TimeStamp_t:=$TimeStamp_t+$Time_t

$TimeStamp_t:=Timestamp:C1445

$TimeStamp_t:=Replace string:C233($TimeStamp_t;":";"")
$TimeStamp_t:=Replace string:C233($TimeStamp_t;"Z";"")
$TimeStamp_t:=Replace string:C233($TimeStamp_t;".";"")
$TimeStamp_t:=Replace string:C233($TimeStamp_t;"-";"")
$TimeStamp_t:=Replace string:C233($TimeStamp_t;"T";"")




Case of 
	: ($TimeStampMode_i=Syn TS DateTime Bracketed)  //  This is the default mode
		
		If ($TimeStamp_t=<>Syn_CurrentTimeStamp_t)  //  We have already created a Timestamp for this second
			//  Don't do anything
		Else   //  Not yet
			<>Syn_TimeStampCounter_i:=0
			<>Syn_CurrentTimeStamp_t:=$TimeStamp_t
		End if 
		
		$TimeStamp_t:=$TimeStamp_t+String:C10(<>Syn_TimeStampCounter_i;"00")
		
		// Increment it for next usage
		<>Syn_TimeStampCounter_i:=<>Syn_TimeStampCounter_i+1
		
		$TimeStamp_t:=$TimeStamp_t+String:C10(Num:C11(<>sync_SiteIdentifier_c3);"00")
		
	: ($TimeStampMode_i=Syn TS DateTime Only)
		// Do nothing
		
	: ($TimeStampMode_i=Syn TS DateTime Padded)
		$TimeStamp_t:=$TimeStamp_t+"00000"+"00"
		
End case 



$0:=$TimeStamp_t