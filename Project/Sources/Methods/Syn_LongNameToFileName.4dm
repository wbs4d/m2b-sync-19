//%attributes = {"invisible":true}
// Long name to file name Project Method
// Long name to file name ( String ) -> String
// Long name to file name ( Long file name ) -> file name

_O_C_STRING:C293(255; $1; $0)
_O_C_INTEGER:C282($viLen; $viPos; $viChar; $viDirSymbol)

$viDirSymbol:=Character code:C91(Folder separator:K24:12)
$viLen:=Length:C16($1)
$viPos:=0
For ($viChar; $viLen; 1; -1)
	If (Character code:C91($1[[$viChar]])=$viDirSymbol)
		$viPos:=$viChar
		$viChar:=0
	End if 
End for 
If ($viPos>0)
	$0:=Substring:C12($1; $viPos+1)
Else 
	$0:=$1
End if 

//If (<>vbDebugOn)  // Set this variable to True or False in the On Startup database method
//If ($0="")
//TRACE
//End if 
//End if 