//%attributes = {"invisible":true}
// ----------------------------------------------------
// Project Method: Syn_ObjectSaveToFile (Object; File Path {; Pretty Print?})

// Takes an object and writes it to disk 
//   as a JSON text document
// NB. will overwrite existing doc, without checking

// Access: Shared

// Parameters: 
//   $1 : Object : Description
//   $2 : Text : File path
//   $3 : Boolean {Optional} : True (default) = Use Pretty Print 

// Created by Cannon Smith
// ----------------------------------------------------

C_OBJECT:C1216($1)
C_TEXT:C284($2; $Filepath_t)  //If it already exists, it will be overwritten
C_BOOLEAN:C305($3; $UsePrettyPrint_b)  //Optional, default is true.
C_TEXT:C284($JSONString_t)


$Filepath_t:=$2

If (Count parameters:C259>2)
	
	$UsePrettyPrint_b:=$3
Else 
	$UsePrettyPrint_b:=True:C214
End if 

If ($UsePrettyPrint_b)
	$JSONString_t:=JSON Stringify:C1217($1; *)
Else 
	$JSONString_t:=JSON Stringify:C1217($1)
End if 

TEXT TO DOCUMENT:C1237($Filepath_t; $JSONString_t; UTF8 text without length:K22:17)
