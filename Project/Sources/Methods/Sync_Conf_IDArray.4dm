//%attributes = {}
C_LONGINT:C283($0)

C_LONGINT:C283($arrayrow_i;$processnum_i)
C_POINTER:C301($RegisteredClientsList_ptr;$source_ptr;$ThisListBox_ptr)
C_TEXT:C284($IdentifierDropped_t)

$ThisListBox_ptr:=OBJECT Get pointer:C1124(Object named:K67:5;"sync_ListboxObject_ab")
$RegisteredClientsList_ptr:=OBJECT Get pointer:C1124(Object named:K67:5;"registeredClientsList")

_O_DRAG AND DROP PROPERTIES:C607($source_ptr;$arrayrow_i;$processnum_i)

Case of 
	: (Form event code:C388=On Drag Over:K2:13)
		If ($source_ptr=OBJECT Get pointer:C1124(Object named:K67:5;"availableIdentifiersList Box"))  // If the drop does come from the scrollable area
			$0:=0
		Else 
			$0:=-1  //The drop is refused
		End if 
		
	: (Form event code:C388=On Drop:K2:12)
		
		If ($source_ptr=$RegisteredClientsList_ptr) | ($source_ptr=$ThisListBox_ptr)
			// Not allowed to bounce back onto itself!!!!!
		Else 
			
			$source_ptr:=OBJECT Get pointer:C1124(Object named:K67:5;"availableIdentifiersList")
			$IdentifierDropped_t:=$source_ptr->{$arrayrow_i}
			
			If (Find in array:C230($RegisteredClientsList_ptr->;$IdentifierDropped_t)<1)
				// add it to the array
				APPEND TO ARRAY:C911($RegisteredClientsList_ptr->;$IdentifierDropped_t)
				SORT ARRAY:C229($RegisteredClientsList_ptr->)
				
			End if 
			
			//  If it is present in the other possible locations remove it
			Syn_Pref_RemoveIdentifier($IdentifierDropped_t)
			
		End if 
End case 