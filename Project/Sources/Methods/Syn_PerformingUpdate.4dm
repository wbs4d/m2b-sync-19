//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Method: Syn_PerformingUpdate -> Boolean
If (False:C215)
	C_BOOLEAN:C305(Syn_PerformingUpdate; $0)
End if 

// Description
//   This method is run in a trigger to determine if the 
//    changes are being made in a "sync" process

// ----------------------------------------------------
// User name (OS): Wayne Stewart
// Date and time: Jan 4, 2013

// Parameters
C_BOOLEAN:C305($0)

// Local Vars
C_LONGINT:C283($procState; $procTime)
C_TEXT:C284($procName)

// ----------------------------------------------------

PROCESS PROPERTIES:C336(Current process:C322; $procName; $procState; $procTime)
$0:=($procName="Syn_ApplyUpdates")
