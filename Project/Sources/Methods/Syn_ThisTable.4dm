//%attributes = {"invisible":true}
// ----------------------------------------------------
// Project Method: Syn_ThisTable

// Method Type:    Private
If (False:C215)
	C_BOOLEAN:C305(Syn_ThisTable; $0)
	C_LONGINT:C283(Syn_ThisTable; $1)
	C_POINTER:C301(Syn_ThisTable; $2)
End if 

// Parameters:
C_BOOLEAN:C305($0)
C_LONGINT:C283($1)
C_POINTER:C301($2)

// Local Variables:

// Created by Wayne Stewart (Jul 6, 2013)
//     waynestewart@mac.com
//   This method will check if the table has the required fields
//     and if it has will return true and the Sync_ID field
//   The sync_CachedStructure_aptr is filled in the Sync_Init method
// ----------------------------------------------------

Syn_Init  // So we know the array is declared

If ($1>Size of array:C274(sync_CachedStructure_aptr))  //  Does this table exist in array?
	$0:=False:C215
Else 
	If (Is nil pointer:C315(sync_CachedStructure_aptr{$1}))  //  Is there a field registered for this table?
		$0:=False:C215
	Else 
		$0:=True:C214  //  Return true
		$2->:=sync_CachedStructure_aptr{$1}  //  Return the pointer
	End if 
End if 