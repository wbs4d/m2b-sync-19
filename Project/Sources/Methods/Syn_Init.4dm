//%attributes = {"invisible":true}
// ----------------------------------------------------
// Project Method: Syn_Init

// Initializes both the process and interprocess variables used by the Sync routines.

// Method Type:    Protected

// Created by Wayne Stewart (Jul 22, 2012)
//     waynestewart@mac.com
// ----------------------------------------------------




C_LONGINT:C283($ErrorHandler_i)

ARRAY TEXT:C222($InstalledComponents_at;0)

var $syncFolder_t;$bracket_t;$procName_t;$errorLog_t;$transmissionLogPath_t;$LineFeed_t : Text
var $procState_i;$procTime_i;$ErrorHandler_i;$EncryptionKey_i : Integer
var $runningOnRemote_b : Boolean
var $temp_o : Object

ARRAY TEXT:C222($InstalledComponents_at;0)

$runningOnRemote_b:=(Application type:C494=4D Remote mode:K5:5)





If (Storage:C1525.Syn=Null:C1517)  // So we only do this once
	
	If (True:C214)  //  Set up the default values
		
		If ($runningOnRemote_b)  //  Don't create the folder on 4D Client
			$syncFolder_t:=""
		Else 
			$syncFolder_t:=Get 4D folder:C485(Data folder:K5:33;*)+"Sync"+Folder separator:K24:12
			If (Test path name:C476($syncFolder_t)=Is a folder:K24:2)
			Else 
				CREATE FOLDER:C475($syncFolder_t;*)
			End if 
		End if 
		
		Use (Storage:C1525)
			Storage:C1525.Defaults:=New shared object:C1526(\
				"Port";1701;\
				"Password";"fred1234";\
				"TimeOut";10;\
				"SmallBlobSizeOffset";20;\
				"LargeBlobSizeOffset";24;\
				"DataOffset";28;\
				"BlobPadding";80;\
				"SiteIdentifier";"x33";\
				"ServerIdentifier";"";\
				"SyncFolder";$syncFolder_t;\
				"PrefsPath";$syncFolder_t+"Config.json")
		End use 
		
	End if 
	
	If (True:C214)  // -- Local Prefs
		
		If ($runningOnRemote_b)
			Use (Storage:C1525)
				Storage:C1525.Local:=OB Copy:C1225(syn_GetPrefsFromServer;ck shared:K85:29)
			End use 
		Else 
			Use (Storage:C1525)
				If (Test path name:C476(Storage:C1525.Defaults.PrefsPath)=Is a document:K24:1)
					$temp_o:=Syn_ObjectLoadFromFile(Storage:C1525.Defaults.PrefsPath)
					Storage:C1525.Local:=OB Copy:C1225($temp_o;ck shared:K85:29)
					
				Else 
					Storage:C1525.Local:=New shared object:C1526
					
					Use (Storage:C1525.Local)
						Storage:C1525.Local.SiteIdentifier:=Storage:C1525.Defaults.SiteIdentifier
						Storage:C1525.Local.Port:=Storage:C1525.Defaults.Port
						Storage:C1525.Local.Password:=Storage:C1525.Defaults.Password
						Storage:C1525.Local.logErrors:=0
						Storage:C1525.Local.logConnectionErrors:=0
						Storage:C1525.Local.logTransmissions:=0
						Storage:C1525.Local.logEverything:=0
						Storage:C1525.Local.ServerIdentifier:=Storage:C1525.Defaults.ServerIdentifier
						Storage:C1525.Local.ServerAddress:=""
						Storage:C1525.Local.Clients:=New shared collection:C1527
					End use 
					
					
				End if 
				
			End use 
			
			
			
			
		End if 
		
	End if 
	
	
	
	If (True:C214)  // Miscellaneous Constants
		
		COMPONENT LIST:C1001($InstalledComponents_at)
		
		$EncryptionKey_i:=Syn_ConvertPasswordIntoKey(Storage:C1525.Local.Password)
		If ($EncryptionKey_i=0)
			$EncryptionKey_i:=1701
		End if 
		
		Use (Storage:C1525)
			Storage:C1525.Syn:=New shared object:C1526(\
				"foundationPresent";(Find in array:C230($InstalledComponents_at;"Foundation@")>0);\
				"encryptionKey";$EncryptionKey_i;\
				"closeDialog";False:C215)
		End use 
		
		
		
	End if 
	
	// Set up Logging, now store this with all other logs
	LOG DECLARE LOG("TransmissionLog";"Sync Transmission Log.txt";Get 4D folder:C485(Logs folder:K5:19))
	LOG DECLARE LOG("ErrorLog";"Sync Log.txt";Get 4D folder:C485(Logs folder:K5:19))
	
End if 



If (Syn=Null:C1517)  // So we only do this once.
	
	
	Compiler_Sync
	
	PROCESS PROPERTIES:C336(Current process:C322;$procName_t;$procState_i;$procTime_i)
	$bracket_t:=Storage:C1525.Local.SiteIdentifier
	$bracket_t:=String:C10(Num:C11($bracket_t))+"/32"
	
	Syn:=New object:C1471(\
		"processNumber";Current process:C322;\
		"processName";$procName_t;\
		"createSyncRecords";($procName_t#"Syn_ApplyUpdates");\
		"bracket";$bracket_t)
	
	syn_CacheStructure  // This now contains process variables
	
	//  Enable logging in every process it is controlled by Storage
	//LOG ENABLE(True)
	
	
End if 