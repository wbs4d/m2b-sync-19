//%attributes = {"invisible":true}
C_TEXT:C284($1)
C_TEXT:C284($2)

C_BOOLEAN:C305($logging_b)

If (False:C215)
	C_TEXT:C284(Syn_BugAlert;$1)
	C_TEXT:C284(Syn_BugAlert;$2)
End if 

If (False:C215)
	
	Syn_BugAlert
	
End if 

$logging_b:=LOG ENABLE

LOG ENABLE(True:C214)

If (Count parameters:C259=2)
	ALERT:C41("An error occurred in the method: "+$1+Char:C90(13)+"Error Description:"+Char:C90(13)+$2)
	If (Storage:C1525.Local.logEverything=1) | (Storage:C1525.Local.logErrors=1)
		LOG ADD ENTRY(Error Method;"Line: "+String:C10(Error Line);"Err: "+String:C10(Error);$1;$2)
		
	End if 
	
Else 
	ALERT:C41("An error occurred in the method: "+$1)
	If (Storage:C1525.Local.logEverything=1) | (Storage:C1525.Local.logErrors=1)
		LOG ADD ENTRY(Error Method;"Line: "+String:C10(Error Line);"Err: "+String:C10(Error);$1)
	End if 
	
End if 

Syn_TRACE

LOG ENABLE($logging_b)