//%attributes = {"invisible":true,"shared":true,"publishedSoap":true,"publishedWsdl":true}
C_TEXT:C284($1;$JournalID_t)
C_TEXT:C284($2;$Origin_t)
C_TEXT:C284($3;$Target_t;$Temp_t)

C_LONGINT:C283($RecordCount_i;$Char_i)
C_BOOLEAN:C305($logging_b)
$logging_b:=LOG ENABLE
LOG ENABLE(True:C214)
If (False:C215)
	C_TEXT:C284(syn_S_DeleteSyncRecord;$1;$2;$3)
End if 

Syn_Init

Syn_TRACE

SOAP DECLARATION:C782($1;Is text:K8:3;SOAP input:K46:1;"Journal_ID")
SOAP DECLARATION:C782($2;Is text:K8:3;SOAP input:K46:1;"origin")
SOAP DECLARATION:C782($3;Is text:K8:3;SOAP input:K46:1;"target")

$JournalID_t:=$1
$Origin_t:=$2
$Target_t:=$3

READ WRITE:C146(sync_Table_ptr->)
QUERY:C277(sync_Table_ptr->;sync_JournalID_ptr->=$JournalID_t)  //  This is unique in [sync] table

If (syn_EqualStrings($Target_t;Storage:C1525.Local.SiteIdentifier))
Else 
	$Temp_t:=$Target_t
	$Target_t:=""
	For ($Char_i;1;Length:C16($Temp_t))
		$Target_t:=$Target_t+$Temp_t[[$Char_i]]+Char:C90(0x0332)
	End for 
End if 


$RecordCount_i:=Records in selection:C76(sync_Table_ptr->)
If (Storage:C1525.Local.logTransmissions=1)
	LOG USE LOG("TransmissionLog")
	LOG ADD ENTRY(\
		"SyncDel";\
		"T "+$Target_t+\
		" O "+$Origin_t+\
		" L "+Storage:C1525.Local.SiteIdentifier;\
		$JournalID_t;\
		"Rec: "+String:C10($RecordCount_i))
	LOG USE LOG("ErrorLog")
End if 

Case of 
	: ($RecordCount_i=0)  // How does this happen?
		If (Storage:C1525.Local.logErrors=1) | (Storage:C1525.Local.logEverything=1)
			LOG ADD ENTRY(Current method name:C684;\
				"T "+$Target_t+\
				" O "+$Origin_t+\
				" L "+Storage:C1525.Local.SiteIdentifier;"No Record found with JournalID: "+$JournalID_t)
		End if 
		
	: ($RecordCount_i=1)
		If (Syn_RecordLoad(sync_Table_ptr))
			DELETE RECORD:C58(sync_Table_ptr->)
			If (Storage:C1525.Local.logEverything=1)  // If they have log everything on
				LOG ADD ENTRY(sync_PK_ptr->;Current method name:C684;\
					"T "+$Target_t+\
					" O "+$Origin_t+\
					" L "+Storage:C1525.Local.SiteIdentifier;Syn Delete Record+" "+Syn OK+" with JournalID: "+$JournalID_t)
			End if 
		Else 
			If (Storage:C1525.Local.logErrors=1) | (Storage:C1525.Local.logEverything=1)
				LOG ADD ENTRY(sync_PK_ptr->;Current method name:C684;\
					"T "+$Target_t+\
					" O "+$Origin_t+\
					" L "+Storage:C1525.Local.SiteIdentifier;Syn Err Locked Record+" with JournalID: "+$JournalID_t)
			End if 
		End if 
		
	Else   // There should only be one
		LOG ADD ENTRY(sync_PK_ptr->;Current method name:C684;\
			"T "+$Target_t+\
			" O "+$Origin_t+\
			" L "+Storage:C1525.Local.SiteIdentifier;String:C10($RecordCount_i)+" records found with JournalID: "+$JournalID_t)
		DELETE SELECTION:C66(sync_Table_ptr->)
		
		
End case 

LOG ENABLE($logging_b)