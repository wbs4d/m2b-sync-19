//%attributes = {"shared":true}
// ----------------------------------------------------
// Project Method: Syn_reSyncRecord

// Description

// Access: Shared

// Parameters: 
//   $1 : Type : Description
//   $x : Type : Description (optional)

// Created by Wayne Stewart (2021-07-16T14:00:00Z)

//     wayne@4dsupport.guru
// ----------------------------------------------------

If (False:C215)
	C_POINTER:C301(Syn_reSyncRecord; $1)
End if 

C_LONGINT:C283($DocType_i)
C_LONGINT:C283($Loop_i; $Start_i)
C_LONGINT:C283($NumRecords_i)
C_LONGINT:C283($ProgressBar_i)
C_REAL:C285($Progress_r)
C_TEXT:C284($Message_t)
C_TEXT:C284($NumRecords_t)
C_TEXT:C284($PathName_t; $TableName_t)
C_TEXT:C284($Progress_t)
C_POINTER:C301($1; $Table_ptr)


//IE_ImportAllRecordsInTable()
//IE_ExportAllRecordsInTable()

$Start_i:=Milliseconds:C459

If (Count parameters:C259=1)
	$Table_ptr:=$1
Else 
	$Table_ptr:=Current form table:C627  //Fnd_Gen_CurrentTable
End if 

C_TEXT:C284($PathName_t)

$TableName_t:=Table name:C256($Table_ptr)
$PathName_t:=Temporary folder:C486+"CAAMSExport"+Folder separator:K24:12
CREATE FOLDER:C475($PathName_t; *)

$PathName_t:=$PathName_t+$TableName_t+".4DRecords"

If (Test path name:C476($PathName_t)=Is a document:K24:1)  //  If the document already exists
	DELETE DOCUMENT:C159($PathName_t)  //     Delete it first
End if 

$ProgressBar_i:=Progress New

Progress SET BUTTON ENABLED($ProgressBar_i; False:C215)

// Step one
SET CHANNEL:C77(10; $PathName_t)  //   Open the document in write mode
$NumRecords_i:=Records in selection:C76($Table_ptr->)
$Message_t:="Exporting "+String:C10($NumRecords_i)+" records from the "+$TableName_t+" table."
$Progress_t:="    "
Progress SET TITLE($ProgressBar_i; $Message_t; 0; $Progress_t; True:C214)

FIRST RECORD:C50($Table_ptr->)
$NumRecords_t:=" of "+String:C10($NumRecords_i)+" records."
For ($Loop_i; 1; $NumRecords_i)
	$Progress_t:=String:C10($Loop_i)+$NumRecords_t
	SEND RECORD:C78($Table_ptr->)
	NEXT RECORD:C51($Table_ptr->)
	$Progress_r:=$Loop_i/$NumRecords_i
	Progress SET PROGRESS($ProgressBar_i; $Progress_r; $Progress_t; True:C214)
End for 
UNLOAD RECORD:C212($Table_ptr->)

SET CHANNEL:C77(11)  // Close the document

$Message_t:="Deleting "+String:C10($NumRecords_i)+" records from the "+$TableName_t+" table."
Progress SET TITLE($ProgressBar_i; $Message_t; -1; ""; True:C214)
Progress SET PROGRESS($ProgressBar_i; -1; ""; True:C214)
// Step Two
READ WRITE:C146($Table_ptr->)
DELETE SELECTION:C66($Table_ptr->)


// Step Three
$Message_t:="Importing "+String:C10($NumRecords_i)+" records into the "+$TableName_t+" table."
$Progress_t:="    "
Progress SET TITLE($ProgressBar_i; $Message_t; 0; $Progress_t; True:C214)


If (Test path name:C476($PathName_t)=Is a document:K24:1)  //  If the document already exists
	
	SET CHANNEL:C77(10; $PathName_t)
	CREATE EMPTY SET:C140($Table_ptr->; "$import")
	For ($Loop_i; 1; $NumRecords_i)
		$Progress_t:=String:C10($Loop_i)+$NumRecords_t
		RECEIVE RECORD:C79($Table_ptr->)
		SAVE RECORD:C53($Table_ptr->)
		ADD TO SET:C119($Table_ptr->; "$import")
		$Progress_r:=$Loop_i/$NumRecords_i
		Progress SET PROGRESS($ProgressBar_i; $Progress_r; $Progress_t; True:C214)
	End for 
	
	SET CHANNEL:C77(11)  // Close the document
End if 

Progress QUIT($ProgressBar_i)

DELETE DOCUMENT:C159($PathName_t)
//Fnd_Dlg_CustomIcon

USE SET:C118("$import")
CLEAR SET:C117("$import")

//Fnd_Gen_SelectionChanged

Alert2(String:C10($NumRecords_i)+" "+$TableName_t+"s processed, duration: "+String:C10(Milliseconds:C459-$Start_i)+" ms."; 4; "OK"; "Resynchonise Complete")