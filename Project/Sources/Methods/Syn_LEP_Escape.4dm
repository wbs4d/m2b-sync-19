//%attributes = {"invisible":true}
C_TEXT:C284($0)
C_TEXT:C284($1)

C_BOOLEAN:C305($shouldQuote_b)
C_LONGINT:C283($i;$Length_i;$platform_i)
C_TEXT:C284($MetaCharacterString_t;$Parameters_t;$ThisMetaCharacter_t)

If (False:C215)
	C_TEXT:C284(Syn_LEP_Escape;$0)
	C_TEXT:C284(Syn_LEP_Escape;$1)
End if 

If (Count parameters:C259#0)
	
	$Parameters_t:=$1
	
	_O_PLATFORM PROPERTIES:C365($platform_i)
	
	If ($platform_i=Windows:K25:3)
		
		//argument escape for cmd.exe; other commands (curl, ruby, etc.) may be incompatible
		
		$shouldQuote_b:=False:C215
		
		$MetaCharacterString_t:="&|<>()%^\" "
		
		$Length_i:=Length:C16($MetaCharacterString_t)
		
		For ($i;1;$Length_i)
			$ThisMetaCharacter_t:=Substring:C12($MetaCharacterString_t;$i;1)
			$shouldQuote_b:=$shouldQuote_b | (Position:C15($ThisMetaCharacter_t;$Parameters_t;*)#0)
			If ($shouldQuote_b)
				$i:=$Length_i
			End if 
		End for 
		
		If ($shouldQuote_b)
			If (Substring:C12($Parameters_t;Length:C16($Parameters_t))="\\")
				$Parameters_t:="\""+$Parameters_t+"\\\""
			Else 
				$Parameters_t:="\""+$Parameters_t+"\""
			End if 
		End if 
		
	Else 
		
		$MetaCharacterString_t:="\\!\"#$%&'()=~|<>?;*`[] "
		
		For ($i;1;Length:C16($MetaCharacterString_t))
			$ThisMetaCharacter_t:=Substring:C12($MetaCharacterString_t;$i;1)
			$Parameters_t:=Replace string:C233($Parameters_t;$ThisMetaCharacter_t;"\\"+$ThisMetaCharacter_t;*)
		End for 
		
	End if 
	
End if 

$0:=$Parameters_t