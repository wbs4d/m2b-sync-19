//%attributes = {"invisible":true}
// ----------------------------------------------------
// Project Method: Syn_CreateRecord
// Method Type:    Private
// Parameters:
If (False:C215)
	C_TEXT:C284(Syn_CreateRecord;$0)
	C_OBJECT:C1216(Syn_CreateRecord;$1)
End if 

C_TEXT:C284($0)
C_OBJECT:C1216($1;$packet_o)

// Local Variables:
C_LONGINT:C283($CurrentField_i;$FieldType_i;$LargeFieldCount_i;$Offset_i;$SmallFieldCount_i;$Table_i)
C_POINTER:C301($CurrentField_ptr;$SyncField_ptr;$Table_ptr)
C_TEXT:C284($RecordID_t;$Result_t;$TableName_t;$AlertText_t;$TemporaryText_t)
C_OBJECT:C1216($Field_o)

// Created by Wayne Stewart (Jul 6, 2013)
//     waynestewart@mac.com

//   Assumes - Sync Record is loaded
//  See also

If (False:C215)
	Syn_UpdateRecord
	Syn_DeleteRecord
End if 

// ----------------------------------------------------

C_BOOLEAN:C305($logging_b)
$logging_b:=LOG ENABLE
LOG ENABLE(True:C214)

$packet_o:=$1

$Result_t:=""

$Table_i:=$packet_o.TblNo  // Get Table number

$TableName_t:=Table name:C256($Table_i)  // Get Name (for log)

$Table_ptr:=Table:C252($Table_i)  // Get Table pointer

$SyncField_ptr:=sync_CachedStructure_aptr{$Table_i}

$SmallFieldCount_i:=Choose:C955($Packet_o.SF#Null:C1517;$Packet_o.SF.length;0)
$LargeFieldCount_i:=Choose:C955($Packet_o.LF#Null:C1517;$Packet_o.LF.length;0)
$RecordID_t:=$packet_o.RecID

QUERY:C277($Table_ptr->;$SyncField_ptr->=$RecordID_t)

If (Records in selection:C76($Table_ptr->)=0)
	
	//  Create record in appropriate table
	CREATE RECORD:C68($Table_ptr->)
	
	//  Assign the sync field ID
	$SyncField_ptr->:=$RecordID_t
	
	//  Double check that there are no updates waiting for this ID
	If (Find in array:C230(Syn_UpdatesOnHold_at;$RecordID_t)#-1)
		//  There are some updates that have been queued!
		// These will be "MOD" entries that came before the "INS" entry, it shouldn't be possible
		//  We need them to be put back in the queue
		Syn.SOE.ReinsertInQueue:=$RecordID_t
	End if 
	
	// Loop through "small" fields in record to add them
	If ($Packet_o.SF#Null:C1517)
		For each ($Field_o;$Packet_o.SF)
			
			
			$CurrentField_ptr:=Field:C253($Table_i;$Field_o.No)  // This is the field number
			$FieldType_i:=Type:C295($CurrentField_ptr->)
			
			Case of   //  Extract small fields from obbject
				: ($FieldType_i=Is alpha field:K8:1) | ($FieldType_i=Is text:K8:3)
					
					If ($Field_o.Val#Null:C1517)
						// When creating a sync record you may get
						// with text that is "" or NULL
						$TemporaryText_t:=$Field_o.Val
						If (syn_EqualStrings(Syn Empty Value;$TemporaryText_t))
							$CurrentField_ptr->:=""  // Set it to blank
						Else 
							$CurrentField_ptr->:=$TemporaryText_t
						End if 
						$TemporaryText_t:=""
						
					End if 
					
				: ($FieldType_i=Is integer:K8:5)\
					 | ($FieldType_i=Is longint:K8:6)\
					 | ($FieldType_i=Is real:K8:4)
					$CurrentField_ptr->:=$Field_o.Val
					
				: ($FieldType_i=Is boolean:K8:9)
					$CurrentField_ptr->:=($Field_o.Val=1)
					
				: ($FieldType_i=Is date:K8:7)
					$CurrentField_ptr->:=$Field_o.Val  //Syn_DateFromText($Field_o.Val)
					
				: ($FieldType_i=Is time:K8:8)
					$CurrentField_ptr->:=$Field_o.Val  //Syn_TimeFromText($Field_o.Val)
					
			End case 
			
			
		End for each 
	End if 
	$Offset_i:=0
	
	
	// Loop through "large" fields in record to add them
	If ($Packet_o.LF#Null:C1517)
		For each ($Field_o;$Packet_o.LF)
			$CurrentField_ptr:=Field:C253($Table_i;$Field_o.No)  // This is the field number
			BLOB TO VARIABLE:C533(sync_LargeFieldData_ptr->;$CurrentField_ptr->;$Offset_i)
		End for each 
	End if 
	SAVE RECORD:C53($Table_ptr->)
	Syn_UnloadRecordReduceSelection($Table_ptr)
	
	$Result_t:=Syn OK
	
	If (Storage:C1525.Local.logEverything=1)
		LOG ADD ENTRY(sync_PK_ptr->;Current method name:C684;Syn OK;$TableName_t;$RecordID_t)
	End if 
	
Else 
	If (Storage:C1525.Local.logEverything=1) | (Storage:C1525.Local.logErrors=1)
		LOG ADD ENTRY(sync_PK_ptr->;Current method name:C684;Syn Err Duplicate;$TableName_t;$RecordID_t)
	End if 
	
	If (Is compiled mode:C492)
	Else 
		$AlertText_t:=Current method name:C684+Char:C90(13)
		$AlertText_t:=$AlertText_t+Syn Err Duplicate+Char:C90(13)
		$AlertText_t:=$AlertText_t+$TableName_t+": "+Field name:C257($SyncField_ptr)+" - "+$RecordID_t+Char:C90(13)
		$AlertText_t:=$AlertText_t+"1° Key: "+sync_PK_ptr->
		CALL WORKER:C1389(1;"Syn_ApplAlert";$AlertText_t)
		//ALERT($AlertText_t)
		Syn_TRACE
	End if 
	
	$Result_t:=Syn Push Duplicate Record
End if 

LOG ENABLE($logging_b)

$0:=$Result_t