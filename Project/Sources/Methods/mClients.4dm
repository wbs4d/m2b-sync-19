//%attributes = {"invisible":true}
// mClients
// Created by Wayne Stewart (Sep 4, 2013)
//  Method is an autostart type
//     waynestewart@mac.com

C_LONGINT:C283($1;$ProcessID_i;$win_i)

If (False:C215)  //  Copy this to your Compiler Method!
	C_LONGINT:C283(mClients;$1)
End if 

If (Count parameters:C259=1)
	$win_i:=Open form window:C675([Client:1];"Input";Plain form window:K39:10;Horizontally centered:K39:1;Vertically centered:K39:4)
	READ ONLY:C145(*)
	READ WRITE:C146([Client:1])
	ALL RECORDS:C47([Client:1])
	MODIFY SELECTION:C204([Client:1];Multiple selection:K50:3;False:C215;*)
	CLOSE WINDOW:C154($win_i)
Else 
	// This version allows for any number of processes
	// $ProcessID_i:=New Process(Current method name;512*1024;Current method name;0)
	// This version allows for one unique process
	$ProcessID_i:=New process:C317(Current method name:C684;512*1024;Current method name:C684;0;*)
	RESUME PROCESS:C320($ProcessID_i)
	SHOW PROCESS:C325($ProcessID_i)
	BRING TO FRONT:C326($ProcessID_i)
End if 

