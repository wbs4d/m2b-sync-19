//%attributes = {"shared":true}
// syn_Monitor
// Created by Wayne Stewart (Aug 10, 2013)
//  Method is an autostart type
//     waynestewart@mac.com

C_LONGINT:C283($1; $ProcessID_i)

If (False:C215)  //  Copy this to your Compiler Method!
	C_LONGINT:C283(syn_Monitor; $1)
End if 

Syn_Init

If (Count parameters:C259=1)
	
	Syn.OnClient:=(Application type:C494=4D Remote mode:K5:5)
	
	syn_CloseDialog(False:C215)
	
	$ProcessID_i:=Open form window:C675("Sync_Queue"; Plain form window:K39:10; Horizontally centered:K39:1; Vertically centered:K39:4)
	
	If (Application type:C494=4D Remote mode:K5:5)
		SET WINDOW TITLE:C213("Sync Server Monitor "+Storage:C1525.Local.SiteIdentifier)
	Else 
		SET WINDOW TITLE:C213("Sync Local Monitor "+Storage:C1525.Local.SiteIdentifier)
	End if 
	
	If (Storage:C1525.Syn.foundationPresent)
		EXECUTE METHOD:C1007("Fnd_Menu_Window_Add")
	End if 
	
	DIALOG:C40("Sync_Queue")
	
	If (Storage:C1525.Syn.foundationPresent)
		EXECUTE METHOD:C1007("Fnd_Menu_Window_Remove")
	End if 
	
	
	CLOSE WINDOW:C154
	
Else 
	// This version allows for any number of processes
	// $ProcessID_i:=New Process(Current method name;0;Current method name;0)
	// This version allows for one unique process
	$ProcessID_i:=New process:C317(Current method name:C684; 0; Current method name:C684; 0; *)
	RESUME PROCESS:C320($ProcessID_i)
	SHOW PROCESS:C325($ProcessID_i)
	BRING TO FRONT:C326($ProcessID_i)
End if 

