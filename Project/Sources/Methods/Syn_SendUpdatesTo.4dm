//%attributes = {"invisible":true}
// ----------------------------------------------------
// Project Method: Syn_SendUpdatesTo

// Method Type:    Private
If (False:C215)
	C_POINTER:C301(Syn_SendUpdatesTo; $1)
	C_TEXT:C284(Syn_SendUpdatesTo; $2)
End if 
// Parameters:
C_POINTER:C301($1)
// Local Variables:
C_TEXT:C284($server_t; $CallingMethod_t)
// Created by Wayne Stewart (Jul 6, 2013)
//     waynestewart@mac.com

//

// ----------------------------------------------------

$CallingMethod_t:=$2

Case of 
	: ($CallingMethod_t="syn_C_RequestUpdate")
		//  All clients
		COLLECTION TO ARRAY:C1562(Storage:C1525.Local.Clients; $1->)
		
		//  Local site
		APPEND TO ARRAY:C911($1->; Storage:C1525.Local.SiteIdentifier)
		
		//  Do NOT add the server
		
		
	: ($CallingMethod_t="syn_S_ReceiveUpdate")
		//  All clients, this will create a 'bounce back' which will be filtered in calling method
		COLLECTION TO ARRAY:C1562(Storage:C1525.Local.Clients; $1->)
		
		//  Local site
		APPEND TO ARRAY:C911($1->; Storage:C1525.Local.SiteIdentifier)
		
		//  And the server, if there is one
		$server_t:=Storage:C1525.Local.ServerIdentifier
		If (Length:C16($server_t)>0)
			//  If there is add it to the array of sites to send update to
			APPEND TO ARRAY:C911($1->; $server_t)
		End if 
		
		
	: ($CallingMethod_t="Syn_Trigger")
		//  All clients
		COLLECTION TO ARRAY:C1562(Storage:C1525.Local.Clients; $1->)
		//Dict_GetArray(Dict_Named("SyncLocal"); "Sync Clients"; $1)
		
		//  Do NOT add Local site
		
		//  And the server, if there is one
		$server_t:=Storage:C1525.Local.ServerIdentifier
		If (Length:C16($server_t)>0)
			//  If there is add it to the array of sites to send update to
			APPEND TO ARRAY:C911($1->; $server_t)
		End if 
		
	: ($CallingMethod_t="syn_UpdateQueueDisplay") | ($CallingMethod_t="Sync Queue") | ($CallingMethod_t="Sync_Queue@")
		If (Storage:C1525.Local.Clients#Null:C1517)
			COLLECTION TO ARRAY:C1562(Storage:C1525.Local.Clients; $1->)
			//Dict_GetArray(Dict_Named("SyncLocal"); "Sync Clients"; $1)
		End if 
		//  Local site
		APPEND TO ARRAY:C911($1->; Storage:C1525.Local.SiteIdentifier)
		
		//  And the server, if there is one
		$server_t:=Storage:C1525.Local.ServerIdentifier
		If (Length:C16($server_t)>0)
			//  If there is add it to the array of sites to send update to
			APPEND TO ARRAY:C911($1->; $server_t)
		End if 
		
		
	Else 
		Syn_BugAlert(Current method name:C684; "Unknown calling method: "+$CallingMethod_t)
		
End case 





