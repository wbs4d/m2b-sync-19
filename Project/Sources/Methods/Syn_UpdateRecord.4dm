//%attributes = {"invisible":true}
C_TEXT:C284($0)
C_OBJECT:C1216($1)

C_LONGINT:C283($CurrentField_i;$DictionaryID_i;$FieldType_i;$LargeFieldCount_i;$Offset_i;$SmallFieldCount_i;$Table_i)
C_POINTER:C301($CurrentField_ptr;$SyncField_ptr;$Table_ptr)
C_TEXT:C284($RecordID_t;$Result_t;$TemporaryText_t)
C_OBJECT:C1216($Packet_o;$Field_o)

If (False:C215)
	C_TEXT:C284(Syn_UpdateRecord;$0)
	C_OBJECT:C1216(Syn_UpdateRecord;$1)
	// See Also
	Syn_CreateRecord
	Syn_DeleteRecord
	
End if 
C_BOOLEAN:C305($logging_b)
$logging_b:=LOG ENABLE
LOG ENABLE(True:C214)
$Packet_o:=$1
//$DictionaryID_i:=$1

$Table_i:=$Packet_o.TblNo
//$Table_i:=JSON Get Number($DictionaryID_i; Syn Table Number)

$Table_ptr:=Table:C252($Table_i)
$SyncField_ptr:=sync_CachedStructure_aptr{$Table_i}

$SmallFieldCount_i:=Choose:C955($Packet_o.SF#Null:C1517;$Packet_o.SF.length;0)
$LargeFieldCount_i:=Choose:C955($Packet_o.LF#Null:C1517;$Packet_o.LF.length;0)
$RecordID_t:=$packet_o.RecID

//$SmallFieldCount_i:=JSON Get Number($DictionaryID_i; Syn Small Field Count)
//$LargeFieldCount_i:=JSON Get Number($DictionaryID_i; Syn Large Field Count)
//$RecordID_t:=JSON Get Text($DictionaryID_i; Syn Record ID)

READ ONLY:C145(*)
READ WRITE:C146($Table_ptr->)
QUERY:C277($Table_ptr->;$SyncField_ptr->=$RecordID_t)


If (Records in selection:C76($Table_ptr->)=0)  //  Need to handle the case we are Updating" a record that does not exist
	If (Storage:C1525.Local.logEverything=1) | (Storage:C1525.Local.logErrors=1)
		LOG ADD ENTRY(sync_PK_ptr->;Current method name:C684;"["+Table name:C256($Table_ptr)+"] record does not exist for update this PK and Sync ID: "+$RecordID_t)
	End if 
	$Result_t:=Syn Record Does Not Exist
	
	If (Is compiled mode:C492)
		
	Else 
		ALERT:C41(Current method name:C684+Char:C90(13)+"["+Table name:C256($Table_ptr)+"] record does not exist for update"+Char:C90(13)+$RecordID_t)
		Syn_TRACE
	End if 
	
	
	
Else   //  The record does exist (thank goodness)
	
	If (Syn_RecordLoad($Table_ptr))  //  This will attempt to load the record for 18 minutes and then give up
		
		//  Assign the sync field ID
		//$SyncField_ptr->:=$RecordID_t  //  No need to do this with modify record
		
		// Loop through "small" fields in record to add them
		
		If ($Packet_o.SF#Null:C1517)
			For each ($Field_o;$Packet_o.SF)
				
				$CurrentField_ptr:=Field:C253($Table_i;$Field_o.No)  // This is the field number
				$FieldType_i:=Type:C295($CurrentField_ptr->)
				
				Case of   //  Extract small fields from obbject
					: ($FieldType_i=Is alpha field:K8:1) | ($FieldType_i=Is text:K8:3)
						
						If ($Field_o.Val#Null:C1517)
							// When creating a sync record you may get
							// with text that is "" or NULL
							$TemporaryText_t:=$Field_o.Val
							If (syn_EqualStrings(Syn Empty Value;$TemporaryText_t))
								$CurrentField_ptr->:=""  // Set it to blank
							Else 
								$CurrentField_ptr->:=$TemporaryText_t
							End if 
							$TemporaryText_t:=""
							
						End if 
						
					: ($FieldType_i=Is integer:K8:5)\
						 | ($FieldType_i=Is longint:K8:6)\
						 | ($FieldType_i=Is real:K8:4)
						$CurrentField_ptr->:=$Field_o.Val
						
					: ($FieldType_i=Is boolean:K8:9)
						$CurrentField_ptr->:=($Field_o.Val=1)
						
					: ($FieldType_i=Is date:K8:7)
						$CurrentField_ptr->:=$Field_o.Val  //Syn_DateFromText($Field_o.Val)
						
					: ($FieldType_i=Is time:K8:8)
						$CurrentField_ptr->:=$Field_o.Val  //Syn_TimeFromText($Field_o.Val)
						
				End case 
				
				
			End for each 
		End if 
		
		$Offset_i:=0
		
		
		// Loop through "large" fields in record to add them
		If ($Packet_o.LF#Null:C1517)
			For each ($Field_o;$Packet_o.LF)
				$CurrentField_ptr:=Field:C253($Table_i;$Field_o.No)  // This is the field number
				BLOB TO VARIABLE:C533(sync_LargeFieldData_ptr->;$CurrentField_ptr->;$Offset_i)
			End for each 
		End if 
		
		SAVE RECORD:C53($Table_ptr->)
		Syn_UnloadRecordReduceSelection($Table_ptr)
		
		$Result_t:=Syn OK
		
		If (Storage:C1525.Local.logEverything=1)
			LOG ADD ENTRY(sync_PK_ptr->;Current method name:C684;Syn OK;Table name:C256($Table_i);"Sync ID: "+$RecordID_t)
		End if 
		
	Else 
		If (Storage:C1525.Local.logEverything=1) | (Storage:C1525.Local.logErrors=1)
			LOG ADD ENTRY(sync_PK_ptr->;Current method name:C684;Syn Err Locked Record;Table name:C256($Table_ptr);"Sync ID: "+$RecordID_t)
			$Result_t:=Syn Locked Record
		End if 
		
		If (Is compiled mode:C492)
		Else 
			ALERT:C41(Current method name:C684+Char:C90(13)+Syn Err Locked Record+Char:C90(13)+Table name:C256($Table_ptr)+Char:C90(13)+$RecordID_t)
			Syn_TRACE
		End if 
		
	End if 
	
End if 

LOG ENABLE($logging_b)

$0:=$Result_t