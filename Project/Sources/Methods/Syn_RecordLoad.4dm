//%attributes = {"invisible":true}
C_BOOLEAN:C305($0)
C_POINTER:C301($1)

C_LONGINT:C283($PauseTime_i; $process_i)
C_TIME:C306($TimeOut_h)
C_POINTER:C301($Table_ptr)
C_TEXT:C284($4Duser_t; $sessionUser_t; $processName_t)

If (False:C215)
	C_BOOLEAN:C305(Syn_RecordLoad; $0)
	C_POINTER:C301(Syn_RecordLoad; $1)
End if 

$Table_ptr:=$1

READ WRITE:C146($Table_ptr->)
LOAD RECORD:C52($Table_ptr->)

$TimeOut_h:=Current time:C178+600  //  Maximum 10 minute wait in loop

$PauseTime_i:=8  // Ticks

While ((Locked:C147($Table_ptr->)) & (Current time:C178<$TimeOut_h) & (Not:C34(Process aborted:C672)))
	DELAY PROCESS:C323(Current process:C322; $PauseTime_i)  // We're in a hurry here, so don't wait long.
	$PauseTime_i:=$PauseTime_i*2  //  But we will delay progressively longer
	IDLE:C311
	LOAD RECORD:C52($Table_ptr->)
	LOCKED BY:C353($Table_ptr->; $process_i; $4Duser_t; $sessionUser_t; $processName_t)
	
End while 

$0:=(Not:C34(Locked:C147($Table_ptr->)))


//   1
//   2
//   4
//   8  --  starting here so actually 3 requests in first second, otherwise it's a bit silly!
//  16
//  32  --  This is one second, 6 load requests in first second
//  64
//  128
//  256
//  512  -- Approx 17 seconds
// 1024
// 2048
// 4096
// 8192 -- 4 min 33 seconds
//16384
//32768 -- Time out: 18 minutes