//%attributes = {"invisible":true}
// ----------------------------------------------------
// Method: Syn_HideUpdate
// Description:
//    Hides an update
//
// Parameters:
C_TEXT:C284($1)
C_TEXT:C284($2)
C_TEXT:C284($3)
// ----------------------------------------------------
// Created by Wayne Stewart (2015-11-01T13:00:00Z)
//     waynestewart@mac.com
// ----------------------------------------------------

C_LONGINT:C283($Bracket_i;$NumberOfAttempts_i;$Position_i)
C_TEXT:C284($SyncRecordJournalID_t;$Prefix_t;$SyncID_t;$SyncIDOfCurrentInstruction_t;$TableName_t)

If (False:C215)
	C_TEXT:C284(Syn_HideUpdate;$1)
	C_TEXT:C284(Syn_HideUpdate;$2)
	C_TEXT:C284(Syn_HideUpdate;$3)
End if 
C_BOOLEAN:C305($logging_b)
$logging_b:=LOG ENABLE
LOG ENABLE(True:C214)
$TableName_t:=$1
$SyncRecordJournalID_t:=$2
$SyncIDOfCurrentInstruction_t:=$3

//  This search should be unnecessary
QUERY:C277(sync_Table_ptr->;sync_JournalID_ptr->=$SyncRecordJournalID_t)

If (Syn_RecordLoad(sync_Table_ptr))  //  This will attempt to load the record for 18 minutes and then give up
	$Bracket_i:=Num:C11(sync_ForIdentifier_ptr->)
	$Prefix_t:=sync_ForIdentifier_ptr->[[1]]
	
	$NumberOfAttempts_i:=sync_NumberOfRetries_ptr->
	$NumberOfAttempts_i:=$NumberOfAttempts_i+1  //  Increment the counter
	
	If ($NumberOfAttempts_i>100)  //  If we've reinserted it 100 times, double bump it
		$Bracket_i:=$Bracket_i+(2*Syn Bump From Queue)  //
	Else 
		$Bracket_i:=$Bracket_i+Syn Bump From Queue
	End if 
	
	sync_ForIdentifier_ptr->:=$Prefix_t+String:C10($Bracket_i;"00")
	
	$Position_i:=Find in array:C230(Syn_UpdatesOnHold_at;$SyncIDOfCurrentInstruction_t)  //  There really should only be one?
	If ($Position_i=-1)
		APPEND TO ARRAY:C911(Syn_UpdatesOnHold_at;$SyncIDOfCurrentInstruction_t)
	End if 
	
	Case of 
		: (Storage:C1525.Local.logEverything=1) | (Storage:C1525.Local.logErrors=1)
			LOG ADD ENTRY($SyncRecordJournalID_t;Current method name:C684;"["+$TableName_t+"]";$SyncIDOfCurrentInstruction_t)
	End case 
	
	SAVE RECORD:C53(sync_Table_ptr->)
	UNLOAD RECORD:C212(sync_Table_ptr->)
End if 

LOG ENABLE($logging_b)