//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Log_Enable({enable†})-->Boolean
C_BOOLEAN:C305($0)
C_BOOLEAN:C305($1)

If (False:C215)
	C_BOOLEAN:C305(Syn_Log_Enable;$0)
	C_BOOLEAN:C305(Syn_Log_Enable;$1)
End if 

// Allows the developer to turn the logger on or off.  Also returns True
// if logging is currently enabled.

// Access: Shared

// Parameters:
// $1 : Boolean : Turn on logging† (optional)

// Returns:
// $0 : Boolean : True if logging i

// Created by Dave Batton on Dec 27, 2004
// ----------------------------------------------------

Syn_Init

If (Count parameters:C259>=1)
	<>Syn_Log_Enabled_b:=$1
End if 

$0:=<>Syn_Log_Enabled_b