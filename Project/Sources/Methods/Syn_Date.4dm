//%attributes = {"shared":true}
// ----------------------------------------------------
// Project Method: Syn_Date (Date or String) --> Date or String

// Automagically converts a  date or a string
//  !25/4/1915! becomes "1915-04-25"
//  "1915-04-25" becomes !25/4/1915!

// Access: Shared

// Parameters: 
//   $1 : Variant : A date or a string

// Returns: 
//   $0 : Variant : The converted date or a string

// Created by Wayne Stewart (2021-04-21T14:00:00Z)
//     wayne@4dsupport.guru
// ----------------------------------------------------


C_VARIANT:C1683($1; $0)
C_TEXT:C284($DTS_t)

If (Value type:C1509($1)=Is date:K8:7)
	$0:=Substring:C12(String:C10($1; ISO date:K1:8); 1; 10)
Else 
	$DTS_t:=$1+"T"+String:C10(Current time:C178; HH MM SS:K7:1)
	$0:=Date:C102($DTS_t)
End if 